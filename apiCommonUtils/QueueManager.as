package {

	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	/**
	 * Queue pattern implementation, goals:
	 * 	-	Timer driven queue pulling
	 * 	-	Process objects batches
	 *
	 * Usage:	1) new QueueManager(Pulling interval in ms, Batch length if > 1);
	 * 			2) populate queue using queueObject or queueObjects methods
	 * 			3) specify completeFunction and (processingFunction or batchProcessingFunction)
	 * 			4) Invoke startProcessing(); Enjoy!
	 */
	public class QueueManager extends EventDispatcher {

		public static const BATCH_LENGTH:int = 1;
		public static const QUEUE_PULL_INTERVAL:int = 100;

		public var processingFunction:Function;
		public var batchProcessingFunction:Function;
		public var completeFunction:Function;

		protected var batchLength:int;
		protected var queuePullTimer:Timer;
		protected var queue:Array = [];

		function QueueManager(pullInterval:int = QUEUE_PULL_INTERVAL, batchLength:int = BATCH_LENGTH) {
			this.batchLength = batchLength;
			queuePullTimer = new Timer(pullInterval);
			queuePullTimer.addEventListener(TimerEvent.TIMER, onQueuePull);
		}

		public function get length():int {
			return queue.length;
		}

		public function startProcessing():void {
			queuePullTimer.start();
		}

		public function stopProcessing():void {
			queuePullTimer.stop();
		}

		public function clear():void {
			stopProcessing();
			queue = [];
		}

		public function queueObject(obj:Object):void {
			queue.push(obj);
		}

		protected function onQueuePull(e:TimerEvent = null):void {
			if (queue.length == 0) {
				stopProcessing();
				if (completeFunction != null)
					completeFunction();
				return;
			}


			var objects:*;

			if (batchLength <= 1) {
				objects = queue.shift();

				if (processingFunction != null)
					processingFunction(objects);

			} else {
				objects = [];

				while (objects.length < batchLength && queue.length > 0)
					objects.push(queue.shift());

				if (batchProcessingFunction != null)
					batchProcessingFunction(objects);
			}

		}

	}
}