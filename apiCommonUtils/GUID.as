package {
	
	import flash.system.Capabilities;
	
	/**
	 * Creates various types of Genuine Unique Identifier
	 */
	public class GUID {
		
		private static var counter:uint = 0;
		
		/**
		 * postId can contain 0..9, a..z 
		 * @return new vkontakte.ru api compatible postId for wall posting 
		 */
		public static function createPostId():String {
			var guid:String = newhash();
			
			// format as vkontakte.ru postId
			guid = guid.substr(0,  30);
			
			return guid.toLowerCase();
		}

		public static function createNew():String {
			return createTiny();
		}
		
		/**
		 * Generate canonically formatted GUID
		 * Example: {B67FEF4B-D398-4970-8706-F6E17B1124DA}
		 */
		private static function createCanonical():String {
			var guid:String = newhash();
			// format as canonical GUID
			guid = '{' + guid.substr(0,  8) + '-' + guid.substr( 8,  4) + '-' 
				+ guid.substr(12, 4) + '-' + guid.substr( 16,  4) + '-'
				+ guid.substr(20, 12) + '}'; 
			return guid.toUpperCase();
		}
		
		/**
		 * Generate tiny GUID
		 * Example: {F6E17B1124DA}
		 */
		private static function createTiny():String {
			var guid:String = newhash();
			
			// format as tiny GUID
			guid = '{' + guid.substr(8, 12) + '}'; 
			return guid.toUpperCase();
		}
		
		/**
		 * Generate short GUID
		 * Example: {B67FEF4B-F6E17B1124DA}
		 */
		private static function createShort():String {
			var guid:String = newhash();
			
			// format as short GUID
			guid = '{' + guid.substr(0, 8) + '-' + guid.substr(8, 12) + '}'; 
			return guid.toUpperCase();
		}
		
		private static function newhash():String {
			var id1:Number = new Date().getTime();
			var id2:Number = Math.random() * Number.MAX_VALUE;
			var id3:String = Capabilities.serverString;
			return new SHA1().calculate(id1 + id3 + id2 + counter++);
		}		
	}
}