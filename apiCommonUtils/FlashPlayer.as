package {

	import flash.system.Capabilities;

	public class FlashPlayer {

		public static const PLATFORM_WINDOWS:String = 'WIN';
		public static const PLATFORM_LINUX:String = 'LNX';
		public static const PLATFORM_MACINTOSH:String = 'MAC';

		protected var _platform:String;
		protected var _majorVersion:Number;
		protected var _minorVersion:Number;
		protected var _buildNumber:Number;
		protected var _internalBuildNumber:Number;

		function FlashPlayer() {
			init();
		}

		public function get internalBuildNumber():Number {
			return _internalBuildNumber;
		}

		public function set internalBuildNumber(value:Number):void {
			_internalBuildNumber = value;
		}

		public function get buildNumber():Number {
			return _buildNumber;
		}

		public function set buildNumber(value:Number):void {
			_buildNumber = value;
		}

		public function get minorVersion():Number {
			return _minorVersion;
		}

		public function set minorVersion(value:Number):void {
			_minorVersion = value;
		}

		public function get majorVersion():Number {
			return _majorVersion;
		}

		public function set majorVersion(value:Number):void {
			_majorVersion = value;
		}

		public function get platform():String {
			return _platform;
		}

		public function set platform(value:String):void {
			_platform = value;
		}

		public function get isOpenScreenCompatible():Boolean {
			return majorVersion >= 10 && minorVersion >= 1;
		}

		public function toString():String {
			return Capabilities.version;
		}

		protected function init():void {
			var parts:Array = toString().split(',');

			// The main version contains the OS type too so we split it in two
			// and we'll have the OS type and the major version number separately.
			var platformAndVersion:Array = parts[0].split(' ');
			_platform = platformAndVersion[0];
			_majorVersion = parseInt(platformAndVersion[1]);
			_minorVersion = parseInt(parts[1]);
			_buildNumber = parseInt(parts[2]);
			_internalBuildNumber = parseInt(parts[3]);
		}

	}
}