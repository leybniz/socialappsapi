apivkontakte, apiMailRu, apiOdnoklassniki - Main libraries
apiCommonUtils - common classes, shared over main libraries
helloWorld - sample applicaions utilizing main libraries


Libraries setup steps:

Import libraries into your Flash Builder workspace

"How to import libraries into Flash Builder workspace?"
a) File -> Import -> Other... -> General -> Existing Projects into Workspace
b) Important: Make sure "Copy projects into workspace" - is Unchecked!!!
c) Select root directory: Browse... select library root folder

Import order:
  vyana
  apiCommonUtils
  apivkontakte (and/or) apiMailRu (and/or) apiOdnoklassniki
  HelloVkontakte (and/or) HelloMoiMir (and/or) HelloOdnoklassniki

Open appropriate HelloWorld app digg into & Enjoy!

That's it