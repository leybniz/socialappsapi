package {
	
	import api.mailru.commands.audio.*;
	import api.mailru.commands.general.*;
	import api.mailru.commands.photo.*;
	import api.mailru.commands.stream.*;
	import api.mailru.commands.user.*;
	
	import org.vyana.control.commands.*;

	/**
	 * Commands registry class, each command represented as separate constant
	 */
	public final class APIMailRu extends VyanaCommandRegistry {
		
		// General
		public static const OPEN_PAYMENT_DIALOG:* = OpenPaymentDialogCommand;
		
		// Users		
		public static const IS_APP_USER:* = IsAppUserCommand;
		public static const HAS_USER_PERMISSION:* = HasUserPermissionCommand;
		public static const GET_FRIENDS:* = GetFriendsCommand;
		public static const GET_APP_FRIENDS:* = GetAppFriendsCommand;
		public static const GET_USER_INFO:* = GetUserInfoCommand;
		
		// Photo
		public static const GET_PHOTO_ALBUMS:* = GetAlbumsCommand;
		public static const GET_PHOTOS:* = GetPhotosCommand;
		
		// Audio
		public static const GET_AUDIOS:* = GetAudiosCommand;
		public static const LINK_AUDIO:* = LinkAudioCommand;
		
		// Stream		
		public static const STREAM_PUBLISH:* = StreamPublishCommand;

	}

	new APIMailRu();

}