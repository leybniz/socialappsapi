package api.mailru {

	import flash.utils.Dictionary;

	[Bindable]
	public class MoyMirGlobals {
		
		/**
		 * Cached UserInfo's instances from all user info fetching apis, to minimize waste requests
		 */
		public static var userInfoItems:Dictionary = new Dictionary();
		
	}
}