package api.mailru.commands {

	import api.mailru.business.AbstractMoyMirServiceDelegate;
	import api.mailru.constant.ResponseFormat;
	import api.mailru.model.MoyMirApplicationModel;
	
	import com.adobe.serialization.json.JSON;
	
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import mx.rpc.Fault;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.Base64Decoder;
	import mx.utils.Base64Encoder;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import org.vyana.control.commands.AsynchronousCommand;

	public class MoyMirCommand extends AsynchronousCommand {

		protected var resultMessages:Dictionary = new Dictionary();

		protected function get applicationModel():MoyMirApplicationModel {
			return context.model as MoyMirApplicationModel;
		}

		protected function get isXML():Boolean {
			return applicationModel.appDescriptor.responseFormat == ResponseFormat.XML;
		}

		protected function get isJSON():Boolean {
			return applicationModel.appDescriptor.responseFormat == ResponseFormat.JSON;
		}

		protected function compressString(s:String):String {
			if (s.length == 0)
				return "";
			var ba:ByteArray = new ByteArray();
			var Base64:Base64Encoder = new Base64Encoder();
			ba.writeMultiByte(s, 'utf-8');
			ba.compress();
			ba.position = 0;
			Base64.encodeBytes(ba);
			return Base64.flush();
		}

		protected function decompressString(s:String):String {
			var Base64:Base64Decoder = new Base64Decoder();
			var ba:ByteArray;

			try {
				Base64.decode(StringUtil.trim(s));
				ba = Base64.flush();
				ba.uncompress();
				ba.position = 0;
				return ba.readMultiByte(ba.length, 'utf-8');
			}
			catch (e:Error) {
			}

			return "";
		}

		protected function detectResultFault(e:ResultEvent):Fault {
			var f:Fault;
			var r:* = e.result;

			if (!r)
				return f;

			// JSON
			if (r.hasOwnProperty('error'))
				f = new Fault(r.error.error_code, r.error.error_msg, ObjectUtil.toString(r));

			// XML
			if (r is XML && XML(r).localName() == 'error')
				f = new Fault(r.error_code, r.error_msg, ObjectUtil.toString(r));

			return f;
		}

		override public function result(e:Object):void {
			var re:ResultEvent = e as ResultEvent;
			
			// Duplicated result event passed, ignore
			if (re && resultMessages[re.messageId])
				return;
			
			resultMessages[re.messageId] = re.message;				

			if (isJSON) {								
				var resObject:* = e.result;
				
				try {
					if (resObject is String) {
						var strResult:String = e.result as String;
						resObject = (strResult.length > 0) ? JSON.decode(strResult) : {};
					}					
					
					re = ResultEvent.createEvent(resObject, e.token, e.message);
				} catch (err:Error) {
					
				}
			}	

			// Is this an Error?
			var f:Fault = detectResultFault(re);
			if (f) {
				// If so, redirect it to proper destination ;)
				fault(new FaultEvent(FaultEvent.FAULT, false, true, f));

				return;
			}

			super.result(re);
		}

		override protected function onResult(e:ResultEvent):void {			
			super.onResult(e);

			AbstractMoyMirServiceDelegate.requestsQueue.startProcessing();
		}

		override protected function onFault(e:FaultEvent):void {
			super.onFault(e);

			context.dispatchEvent(e);

			AbstractMoyMirServiceDelegate.requestsQueue.startProcessing();
		}

	}
}