package api.mailru.commands.stream {

	import api.mailru.business.delegates.StreamMoyMirServiceDelegate;
	import api.mailru.commands.MoyMirCommand;
	
	import mx.rpc.IResponder;

	public class AbstractStreamCommand extends MoyMirCommand {
		
		protected var delegate:StreamMoyMirServiceDelegate = new StreamMoyMirServiceDelegate(this as IResponder);

	}
}