package api.mailru.commands.photo {
	
	import org.vyana.control.VyanaEvent;

	public class GetPhotosCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getPhotos(data.aid, data.uid, data.pids);
		}

	}
}