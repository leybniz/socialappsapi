package api.mailru.commands.photo {

	import api.mailru.business.delegates.PhotoMoyMirServiceDelegate;
	import api.mailru.commands.MoyMirCommand;
	
	import mx.rpc.IResponder;

	public class AbstractPhotoCommand extends MoyMirCommand {
		
		protected var delegate:PhotoMoyMirServiceDelegate = new PhotoMoyMirServiceDelegate(this as IResponder);

	}
}