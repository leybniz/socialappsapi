package api.mailru.commands.photo {
	
	import org.vyana.control.VyanaEvent;

	public class GetAlbumsCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getAlbums(data.uid, data.aids);
		}

	}
}