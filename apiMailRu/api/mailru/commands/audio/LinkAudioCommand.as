package api.mailru.commands.audio {
	
	import org.vyana.control.VyanaEvent;

	public class LinkAudioCommand extends AbstractAudioCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.linkAudio(data);
		}

	}
}