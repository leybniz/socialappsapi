package api.mailru.commands.audio {

	import api.mailru.business.delegates.AudioMoyMirServiceDelegate;
	import api.mailru.commands.MoyMirCommand;
	
	import mx.rpc.IResponder;

	public class AbstractAudioCommand extends MoyMirCommand {
		
		protected var delegate:AudioMoyMirServiceDelegate = new AudioMoyMirServiceDelegate(this as IResponder);

	}
}