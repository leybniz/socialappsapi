package api.mailru.commands.audio {
	
	import org.vyana.control.VyanaEvent;

	public class GetAudiosCommand extends AbstractAudioCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getAudios(data.uid, data.mids);
		}

	}
}