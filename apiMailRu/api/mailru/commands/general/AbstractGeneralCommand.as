package api.mailru.commands.general {

	import api.mailru.business.delegates.GeneralMoyMirServiceDelegate;
	import api.mailru.commands.MoyMirCommand;
	
	import mx.rpc.IResponder;

	public class AbstractGeneralCommand extends MoyMirCommand {
		
		protected var delegate:GeneralMoyMirServiceDelegate = new GeneralMoyMirServiceDelegate(this as IResponder);

	}
}