package api.mailru.commands.general {
	
	import org.vyana.control.VyanaEvent;

	public class OpenPaymentDialogCommand extends AbstractGeneralCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.openPaymentsDialog(data);
		}

	}
}