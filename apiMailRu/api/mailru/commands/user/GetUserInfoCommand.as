package api.mailru.commands.user {

	import api.mailru.MoyMirGlobals;
	
	import org.vyana.control.VyanaEvent;

	public class GetUserInfoCommand extends AbstractUserCommand {
		
		protected var res:MightyArray = new MightyArray();
		protected var uids:MightyArray;
		protected var skipRequest:Boolean;
		
		override protected function set Result(value:*):void {			
			
			if (isJSON && value is Array) {
				
				if (!skipRequest)
					res.append(cacheUserInfoObjects(value));
				
				super.Result = res;
			}			
			
		}		

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			uids = new MightyArray(data);
			
			// Trim cached uids
			for each (var uid:String in uids)
				if (MoyMirGlobals.userInfoItems[uid]) {
					uids.removeItem(uid);
					res.addItem(MoyMirGlobals.userInfoItems[uid]);
				}	
				
			// Nothing to load? everything cached :)
			if (uids.length == 0) {
				skipRequest = true;
				Result = res;				
				return;
			} else
				delegate.getInfo(uids);
		}

	}
}