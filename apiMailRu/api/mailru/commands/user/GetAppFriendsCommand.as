package api.mailru.commands.user {

	
	import org.vyana.control.VyanaEvent;

	public class GetAppFriendsCommand extends AbstractUserCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getAppFriends(data.includeInfo);
		}

	}
}