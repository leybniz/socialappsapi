package api.mailru.commands.user {

	
	import org.vyana.control.VyanaEvent;

	public class IsAppUserCommand extends api.mailru.commands.user.AbstractUserCommand {
		
		override protected function set Result(value:*):void {			
			if (value)
				super.Result = value['isAppUser']
			else			
				super.Result = value;
		}		

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.isAppUser();
		}

	}
}