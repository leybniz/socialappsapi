package api.mailru.commands.user {

	
	import org.vyana.control.VyanaEvent;

	public class GetFriendsCommand extends AbstractUserCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getFriends(data.includeInfo);
		}

	}
}