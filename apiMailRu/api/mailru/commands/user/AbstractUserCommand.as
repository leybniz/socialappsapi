package api.mailru.commands.user {

	import api.mailru.MoyMirGlobals;
	import api.mailru.business.delegates.UserMoyMirServiceDelegate;
	import api.mailru.model.vo.UserInfo;
	
	import mx.rpc.IResponder;
	
	import org.vyana.control.VyanaEvent;
	import api.mailru.commands.MoyMirCommand;

	public class AbstractUserCommand extends MoyMirCommand {
		
		protected var delegate:UserMoyMirServiceDelegate = new UserMoyMirServiceDelegate(this as IResponder);

		override protected function set Result(value:*):void {			
			
			if (isJSON && value is Array) {
				super.Result = cacheUserInfoObjects(value);				
				return;
			}			
			
			super.Result = value;
		}
		
		protected function cacheUserInfoObjects(objects:Array):MightyArray {			
			var result:MightyArray = new MightyArray();
			
			for each (var o:* in objects) {
				var u:UserInfo = new UserInfo(o);
				MoyMirGlobals.userInfoItems[u.id] = u;
				result.addItem(u);
			}
			
			return result;
		}

	}
}