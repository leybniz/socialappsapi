package api.mailru.commands.user {

	import api.mailru.constant.StringBoolean;
	
	import org.vyana.control.VyanaEvent;

	public class HasUserPermissionCommand extends AbstractUserCommand {

		override protected function set Result(value:*):void {			
			if (value)
				super.Result = value[data.permission]
			else			
				super.Result = value;
		}		
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.hasUserPermission(data.permission);
		}

	}
}