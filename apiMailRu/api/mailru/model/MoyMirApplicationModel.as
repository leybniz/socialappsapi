package api.mailru.model {

	import api.mailru.model.vo.MoyMirApplicationDescriptor;
	import api.mailru.model.vo.Viewer;
	
	import mx.core.FlexGlobals;
	
	import org.vyana.model.VyanaModel;

	public class MoyMirApplicationModel extends VyanaModel {
		
		public var appDescriptor:MoyMirApplicationDescriptor;		
		public var viewer:Viewer;
		
		function MoyMirApplicationModel(app_id:String, app_private_key:String, author_id:String) {			
			
			appDescriptor = new MoyMirApplicationDescriptor(app_id, app_private_key, author_id, flashvars);
			
			// Does this app stolen?
			if (appDescriptor.isStolen)
				do {
					// Nirvana ;)
				} while (true); 
			
		}
		
		protected function get app():* {
			return FlexGlobals.topLevelApplication;
		}

		public function get flashvars():Object {
			return app.parameters;
		}
		
		/**
		 * Must be called on Application AddedToStage 
		 */
		override public function init():void {
			super.init();
			
			viewer = new Viewer(appDescriptor, flashvars);
			appDescriptor.flashvars = flashvars;
		}

	}

}