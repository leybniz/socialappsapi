package api.mailru.model.vo {
	
	import api.mailru.constant.ResponseFormat;
	
	import flash.net.LocalConnection;
	
	import org.vyana.model.vo.VyanaValueObject;

	public class MoyMirApplicationDescriptor extends VyanaValueObject {
		
		/**
		 * Defines API response format
		 * default: format is JSON, because it's much faster than XML
		 */
		public var responseFormat:String = ResponseFormat.JSON;

		protected var lc:LocalConnection = new LocalConnection();
		protected var _id:String;
		protected var _privateKey:String;
		protected var _authorId:String;
		protected var _flashvars:Object;
		
		function MoyMirApplicationDescriptor(id:String, privateKey:String, author:String, fVars:Object) {
			
			this._id = id;
			this._privateKey = privateKey;
			this._authorId = author;
			this.flashvars = fVars ? fVars: {};
			
		}

		public function get flashvars():Object {
			return _flashvars;
		}

		public function set flashvars(value:Object):void {
			_flashvars = value;
		}

		public function get isProduction():Boolean {
			return flashvars.hasOwnProperty('app_id');
		}
		
		public function get state():String {
			return flashvars.state;
		}
		
		public function get windowId():String {
			return flashvars.window_id;
		}
		
		public function get isStolen():Boolean {
			return isProduction && id.toUpperCase() != String(flashvars.app_id).toUpperCase();
		}
		
		public function get isSecondInstance():Boolean {
			try {				
				lc.connect(id);
			}
			catch (e:Error) {
				return true;
			}
			
			return false;
		}
		
		public function get id():String {
			return _id;
		}
		
		public function get secret():String {
			return _privateKey;
		}

		public function get authorId():String {
			return _authorId;
		}

	}
}