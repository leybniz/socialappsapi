package api.mailru.model.vo {

	import org.vyana.model.vo.VyanaValueObject;

	public class PayableService extends VyanaValueObject {

		private var _name:String;
		private var _id:String;
		private var _price:String;
		private var _smsPrice:String; // 1,3,5 USD

		function PayableService(id:String, name:String, smsPrice:String, price:String = null) {
			this.id = id;
			this.name = name;
			this.smsPrice = smsPrice;
			this.price = price;
		}

		public function get smsPrice():String {
			return _smsPrice;
		}

		public function set smsPrice(value:String):void {
			_smsPrice = value;
		}

		public function get price():String {
			return _price;
		}

		public function set price(value:String):void {
			_price = value;
		}

		public function get id():String {
			return _id;
		}

		public function set id(value:String):void {
			_id = value;
		}

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}

	}
}