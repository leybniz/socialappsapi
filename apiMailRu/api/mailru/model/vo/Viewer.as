package api.mailru.model.vo {

	import api.mailru.constant.StringBoolean;
	import api.mailru.constant.UserPermissions;
	
	import org.vyana.model.vo.VyanaValueObject;

	public class Viewer extends VyanaValueObject {

		protected var flashvars:*;
		protected var applicationDescriptor:MoyMirApplicationDescriptor;
		protected var userPermissions:UserPermissions;

		function Viewer(appDescriptor:MoyMirApplicationDescriptor, parameters:*) {

			applicationDescriptor = appDescriptor;
			this.flashvars = parameters ? parameters : {};

			userPermissions = new UserPermissions(flashvars.ext_perm);
		}

		public function get permissions():UserPermissions {
			return userPermissions;
		}

		public function get id():String {
			return applicationDescriptor.isProduction ? flashvars.vid : applicationDescriptor.authorId;
		}

		public function get userId():String {
			return flashvars.oid;
		}
		
		public function get sessionKey():String {
			return flashvars.session_key;
		}		

		public function get isAuthor():Boolean {
			return id == applicationDescriptor.authorId;
		}

		public function get isAppUser():Boolean {
			return flashvars.is_app_user == StringBoolean.TRUE;
		}

	}
}