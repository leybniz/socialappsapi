package api.mailru.model.vo {
	
	import api.mailru.constant.Gender;
	import api.mailru.constant.UserProfileField;
	
	import org.vyana.model.vo.VyanaValueObject;

	public dynamic class UserInfo extends VyanaValueObject {
		
		function UserInfo(initObj:Object = null) {
			
			// Copy props from initial object
			if (initObj)
				for (var p:String in initObj)
					this[p] = initObj[p];
			
		}
		
		public function get id():String {
			return this[UserProfileField.UID];
		}

		public function get isMale():Boolean {
			return this[UserProfileField.SEX] == Gender.MALE;
		}
		
		public function get fullName():String {
			var result:Array = [];
			
			result.push(this[UserProfileField.FIRST_NAME]);
			result.push(this[UserProfileField.LAST_NAME]);
			
			return result.join(' ');
		}
		
	}
}