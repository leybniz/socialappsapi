package api.mailru.business {

	import api.mailru.constant.*;
	import api.mailru.model.MoyMirApplicationModel;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	import org.vyana.model.business.HTTPServiceDelegate;
	import org.vyana.model.business.VyanaHTTPService;

	public class AbstractMoyMirServiceDelegate extends HTTPServiceDelegate {
		
		public static var requestsQueue:QueueManager;		
		
		function AbstractMoyMirServiceDelegate(responder:IResponder = null) {
			super(responder, ServiceName.MOYMIR);

			if (applicationModel.appDescriptor.responseFormat == ResponseFormat.XML)
				mmService.resultFormat = 'e4x';
			
			if (applicationModel.appDescriptor.responseFormat == ResponseFormat.JSON)
				mmService.resultFormat = 'text';

			if (!requestsQueue) {	
				requestsQueue = new QueueManager(1000 / APIRestriction.MAX_REQUESTS_PER_SECOND);
				requestsQueue.processingFunction = 
					function (request:*):void {
						var call:AsyncToken;
						call = mmService.send(request.params);
						call.addResponder(request.responder);
						requestsQueue.stopProcessing();
					};
				requestsQueue.startProcessing();
			}	
		}

		protected function get applicationModel():MoyMirApplicationModel {
			return context.model as MoyMirApplicationModel;
		}

		protected function get mmService():VyanaHTTPService {
			return service as VyanaHTTPService;
		}

		protected function getParamsSignature(params:Object):String {
			var keys:Array = [];
			for (var k:String in params)
				keys.push(k);
			keys.sort();

			var sig:String = applicationModel.viewer.id;
			for (var i:int = 0; i < keys.length; i++)
				sig += keys[i] + "=" + params[keys[i]];
			sig += applicationModel.appDescriptor.secret;

			return MD5.encrypt(sig);
		}

		protected function invokeMethod(methodName:String, params:* = null):void {
			if (!params)
				params = {};

			if (applicationModel.appDescriptor.responseFormat != ResponseFormat.JSON)
				params.format = applicationModel.appDescriptor.responseFormat;

			params.api_id = applicationModel.appDescriptor.id;
			params.method = methodName;

			params.sig = getParamsSignature(params);

			requestsQueue.queueObject(
				{
					params : params,
					responder : responder
				}
			);
			
			if (requestsQueue.length > 0)
				requestsQueue.startProcessing();						
		}

	}
}