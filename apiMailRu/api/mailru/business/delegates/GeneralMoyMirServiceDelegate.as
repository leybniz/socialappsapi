package api.mailru.business.delegates {

	import api.mailru.business.AbstractMoyMirServiceDelegate;
	import api.mailru.constant.methods.GeneralAPIName;
	import api.mailru.model.vo.PayableService;
	
	import mx.rpc.IResponder;

	public class GeneralMoyMirServiceDelegate extends AbstractMoyMirServiceDelegate {
		
		function GeneralMoyMirServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function openPaymentsDialog(service:PayableService):void {
			
			var params:Object = 
				{ 
					session_key : applicationModel.viewer.sessionKey,
					window_id : applicationModel.appDescriptor.windowId,
					service_id : service.id,
					service_name : service.name
				};
			
			if (service.smsPrice)
				params.sms_price = service.smsPrice;
			
			if (service.price)
				params.other_price = service.price; 
			
			invokeMethod(GeneralAPIName.OPEN_PAYMENT_DIALOG, params);
		}
		
	}
}