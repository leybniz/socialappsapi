package api.mailru.business.delegates {

	import api.mailru.business.AbstractMoyMirServiceDelegate;
	import api.mailru.constant.methods.PhotoAPIName;
	
	import mx.rpc.IResponder;

	public class PhotoMoyMirServiceDelegate extends AbstractMoyMirServiceDelegate {
		
		function PhotoMoyMirServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getPhotos(aid:String, uid:String = null, pids:Array = null):void {
			if (!uid)
				uid = applicationModel.viewer.id;
			
			var params:Object = 
				{ 
					uid : uid,
					aid : aid
				};
			
			if (pids)
				params.pids = pids.join(',');
			
			invokeMethod(PhotoAPIName.GET, params);
		}
		
		public function getAlbums(uid:String = null, aids:Array = null):void {
			if (!uid)
				uid = applicationModel.viewer.id; 
			
			var params:Object = 
				{
					uid : uid 
				}; 
			
			if (aids)
				params.aids = aids.join(',');			
			
			invokeMethod(PhotoAPIName.GET_ALBUMS, params);
		}		
		
	}
}