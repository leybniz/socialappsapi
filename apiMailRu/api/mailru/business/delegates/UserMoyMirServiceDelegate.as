package api.mailru.business.delegates {

	import api.mailru.business.AbstractMoyMirServiceDelegate;
	import api.mailru.constant.StringBoolean;
	import api.mailru.constant.methods.UserAPIName;
	
	import mx.rpc.IResponder;

	public class UserMoyMirServiceDelegate extends AbstractMoyMirServiceDelegate {
		
		function UserMoyMirServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function isAppUser():void {
			invokeMethod(UserAPIName.IS_APP_USER, 
				{ uid : applicationModel.viewer.id }			
			);
		}
		
		public function getInfo(uids:Array):void {
			var params:Object = 
				{
					uid : applicationModel.viewer.id,
					uids : uids.join(',') 
				}; 
			
			invokeMethod(UserAPIName.GET_INFO, params);
		}		
		
		public function hasUserPermission(permission:String):void {
			invokeMethod(UserAPIName.HAS_USER_PERMISSION,
				{ 
					uid : applicationModel.viewer.id,
					ext_perm : permission 
				}
			);
		}

		public function getAppFriends(includeInfo:Boolean = false):void {
			invokeMethod(UserAPIName.GET_APP_FRIENDS,
				{ 
					uid : applicationModel.viewer.id,
					ext : StringBoolean.toStringBoolean(includeInfo)
				}
			);
		}
		
		public function getFriends(includeInfo:Boolean = false):void {
			invokeMethod(UserAPIName.GET_FRIENDS,
				{ 
					uid : applicationModel.viewer.id,
					ext : StringBoolean.toStringBoolean(includeInfo)
				}
			);
		}
		
	}
}