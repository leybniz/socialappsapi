package api.mailru.business.delegates {

	import api.mailru.business.AbstractMoyMirServiceDelegate;
	import api.mailru.constant.methods.StreamAPIName;
	
	import mx.rpc.IResponder;

	public class StreamMoyMirServiceDelegate extends AbstractMoyMirServiceDelegate {
		
		function StreamMoyMirServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function publish(text:String, post:String = null, img:String = null):void {
			
			var params:Object = 
				{ 
					session_key : applicationModel.viewer.sessionKey,
					text : text
				};
			
			if (post)
				params.post = post;
			
			if (img)
				params.img = img;			
			
			invokeMethod(StreamAPIName.PUBLISH, params);
		}
		
	}
}