package api.mailru.business.delegates {

	import api.mailru.business.AbstractMoyMirServiceDelegate;
	import api.mailru.constant.methods.AudioAPIName;
	
	import mx.rpc.IResponder;

	public class AudioMoyMirServiceDelegate extends AbstractMoyMirServiceDelegate {
		
		function AudioMoyMirServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function linkAudio(mid:String):void {
			
			var params:Object = 
				{ 
					session_key : applicationModel.viewer.sessionKey,
					mid : mid
				};
			
			invokeMethod(AudioAPIName.LINK, params);
		}
		
		public function getAudios(uid:String = null, mids:Array = null):void {
			if (!uid)
				uid = applicationModel.viewer.id; 
			
			var params:Object = 
				{
					uid : uid 
				}; 
			
			if (mids)
				params.mids = mids.join(',');			
			
			invokeMethod(AudioAPIName.GET, params);
		}		
		
	}
}