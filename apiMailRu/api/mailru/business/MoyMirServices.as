package api.mailru.business {

	import api.mailru.constant.ServiceName;
	
	import org.vyana.model.VyanaModel;
	import org.vyana.model.business.VyanaHTTPService;
	import org.vyana.model.business.VyanaServices;

	public class MoyMirServices extends VyanaServices {

		function MoyMirServices() {
		}
		
		override public function init(model:VyanaModel):void {
			
			registerHTTPService(new VyanaHTTPService(ServiceName.MOYMIR));
			
		}

	}
}