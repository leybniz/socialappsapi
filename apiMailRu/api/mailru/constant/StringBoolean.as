package api.mailru.constant {

	public class StringBoolean {

		public static const TRUE:String = '1';
		public static const FALSE:String = '0';
		
		public static function toStringBoolean(value:Boolean):String {
			return value ? TRUE : FALSE;
		}
		
		public static function toBoolean(value:String):Boolean {
			return value == TRUE ? true : false;
		}		
		
	}
}