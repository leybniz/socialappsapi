package api.mailru.constant {
	
	public final class APIRestriction {
		
		public static const MAX_REQUESTS_PER_SECOND:int  = 10;
		
		public static const MAX_APPLICATION_HEIGHT:int  = 730;
		public static const MAX_APPLICATION_WIDTH:int  = 760;
		
	}
}