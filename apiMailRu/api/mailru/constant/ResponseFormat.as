package api.mailru.constant {
	
	public final class ResponseFormat {
		
		public static const XML:String  = 'xml';
		public static const JSON:String  = 'json';
		
	}
}