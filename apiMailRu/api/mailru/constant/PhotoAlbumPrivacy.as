package api.mailru.constant {

	public class PhotoAlbumPrivacy {
		
		public static const ALL:String = '2';
		public static const PWD_PROTECTED:String = '3';
		public static const FRIENDS_ONLY:String = '4';
		public static const BEST_FRIENDS_ONLY:String = '5';
		public static const BLOG_FRIENDS_ONLY:String = '6';
		
	}
}