package api.mailru.constant {

	public final class UserPermissions {
		
		public static const NOTIFICATIONS:String = 'notifications';
		public static const STREAM:String = 'stream';
		public static const WIDGET:String = 'widget';
		
		protected var permissions:MightyArray = new MightyArray();
		
		function UserPermissions(permissions:String) {
			if (!permissions)				
				return;
			
			this.permissions = new MightyArray(permissions.split(',')); 
		}
		
		public function isSet(permission:String):Boolean {
			return permissions.contains(permission); 
		}
		
		public function setPermission(permission:String):void {
			permissions.addItem(permission); 
		}		
		
	}
}