package api.mailru.constant.methods {
	
	public final class UserAPIName {
		
		public static const IS_APP_USER:String   = 'users.isAppUser';
		public static const GET_INFO:String  = 'users.getInfo';
		public static const HAS_USER_PERMISSION:String   = 'users.hasAppPermission';
		public static const GET_FRIENDS:String   = 'friends.get';		
		public static const GET_APP_FRIENDS:String   = 'friends.getAppUsers';
		
	}
}