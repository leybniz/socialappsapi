package api.mailru.constant.methods {
	
	public final class PhotoAPIName {
		
		public static const GET:String  = 'photos.get';
		public static const GET_ALBUMS:String   = 'photos.getAlbums';		
		
	}
}