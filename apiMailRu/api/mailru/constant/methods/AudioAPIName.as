package api.mailru.constant.methods {
	
	public final class AudioAPIName {
		
		public static const GET:String  = 'audios.get';
		public static const LINK:String   = 'audios.linkAudio';		
		
	}
}