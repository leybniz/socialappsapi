package api.mailru.constant.methods {
	
	public final class StreamAPIName {
		
		public static const PUBLISH:String  = 'stream.publish';
		
	}
}