package api.mailru.constant.methods {
	
	public final class GeneralAPIName {
		
		public static const OPEN_PAYMENT_DIALOG:String  = 'payments.openDialog';
		
	}
}