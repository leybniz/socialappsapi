package api.mailru.constant {

	public class Gender {
		
		public static const FEMALE:String = '1';
		public static const MALE:String = '0';
		
	}
}