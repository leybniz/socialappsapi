package api.mailru.constant {
	
	public final class UserProfileField {
		
		public static const UID:String  = 'uid';
		public static const FIRST_NAME:String  = 'first_name';
		public static const LAST_NAME:String  = 'last_name';
		public static const NICKNAME:String  = 'nick';
		public static const SEX:String  = 'sex';
		public static const BIRTH_DATE:String  = 'birthday';
		public static const LINK:String  = 'link';
		public static const PHOTO:String  = 'pic';
		public static const PHOTO_SMALL:String  = 'pic_small';
		public static const PHOTO_BIG:String  = 'pic_big';
	}
}