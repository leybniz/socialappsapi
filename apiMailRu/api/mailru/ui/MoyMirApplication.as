package api.mailru.ui {

	import api.mailru.constant.APIRestriction;
	import api.mailru.model.MoyMirApplicationModel;
	
	import flash.events.Event;
	
	import spark.components.Application;
	import spark.layouts.VerticalLayout;

	public class MoyMirApplication extends Application {

		function MoyMirApplication() {
			frameRate = 30;
			layout = new VerticalLayout();

			maxWidth = APIRestriction.MAX_APPLICATION_WIDTH;
			maxHeight = APIRestriction.MAX_APPLICATION_HEIGHT;

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}		
		
		protected function get appModel():MoyMirApplicationModel {
			return VyanaContext.getInstance().model as MoyMirApplicationModel;
		}

		override protected function initializationComplete():void {
			super.initializationComplete();			
				
			VyanaContext.getInstance();
		}
		
		protected function onAddedToStage(e:Event):void {			
			
			if (appModel)
				appModel.init();
			
		}		

	}
}