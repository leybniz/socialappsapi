package api.facebook.model.vo {
	
	import api.facebook.constant.ResponseFormat;
	
	import flash.net.LocalConnection;
	
	import org.vyana.model.vo.VyanaValueObject;

	public class FacebookApplicationDescriptor extends VyanaValueObject {
		
		/**
		 * Defines API response format
		 * default: format is JSON, because it's much faster than XML
		 */
		public var responseFormat:String = ResponseFormat.JSON;
		

		protected var lc:LocalConnection = new LocalConnection();
		protected var _id:String;
		protected var _secret:String;
		protected var _authorId:String;
		protected var _testMode:Boolean = false;
		protected var _flashvars:Object;
		
		function FacebookApplicationDescriptor(id:String, secret:String, author:String, fVars:Object) {
			
			this._id = id;
			this._secret = secret;
			this._authorId = author;
			this.flashvars = fVars ? fVars: {};
			
			testMode = !isProduction;
		}

		public function get flashvars():Object {
			return _flashvars;
		}

		public function set flashvars(value:Object):void {
			_flashvars = value;
		}

		public function get isProduction():Boolean {
			return flashvars.hasOwnProperty('swf_url');
		}		
		
		public function get isStolen():Boolean {
			return isProduction && id.toUpperCase() != String(flashvars.api_id).toUpperCase();
		}
		
		public function get isSecondInstance():Boolean {
			try {				
				lc.connect(id);
			}
			catch (e:Error) {
				return true;
			}
			
			return false;
		}
		
		public function get id():String {
			return _id;
		}
		
		public function get secret():String {
			return _secret;
		}

		public function get authorId():String {
			return _authorId;
		}
		
		public function get posterId():String {
			return flashvars.poster_id;
		}
		
		public function get postId():String {
			return flashvars.post_id;
		}		
		
		public function get testMode():Boolean {
			return _testMode;
		}
		
		public function set testMode(value:Boolean):void {
			_testMode = value;
		}
		
		public function get referrer():String {
			return flashvars.referrer;
		}

		public function get language():int {
			return parseInt(flashvars.language);
		}

	}
}