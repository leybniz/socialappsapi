package api.facebook.model {

	import api.facebook.model.vo.FacebookApplicationDescriptor;
	
	import mx.core.FlexGlobals;
	
	import org.vyana.model.VyanaModel;

	public class FacebookApplicationModel extends VyanaModel {
		
		public var appDescriptor:FacebookApplicationDescriptor;		
		//public var viewer:Viewer;
		
		function FacebookApplicationModel(app_id:String, app_secret:String, author_id:String) {			
			
			appDescriptor = new FacebookApplicationDescriptor(app_id, app_secret, author_id, flashvars);
			
			// Does this app stolen?
			if (appDescriptor.isStolen) {
				// Neverending loop ;)
				do {
					// Nirvana :)
				} while (true); 
			}
			
		}
		
		protected function get app():* {
			return FlexGlobals.topLevelApplication;
		}

		public function get flashvars():Object {
			return app.parameters;
		}
		
		override public function init():void {
			super.init();
			
			//viewer = new Viewer(appDescriptor, flashvars);
			//appDescriptor.flashvars = flashvars;
		}

	}

}