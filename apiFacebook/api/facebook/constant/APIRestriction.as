package api.facebook.constant {
	
	public final class APIRestriction {
		
		public static const MAX_REQUESTS_PER_SECOND:int  = 3;
		public static const MAX_VARIABLE_SIZE:int  = 255;
		public static const MAX_VARIABLES_PER_READ_BATCH:int  = 32;
		public static const MAX_USERS_PER_GET_PROFILE:int  = 1000;
		public static const MAX_ADVERTISEMENTS_PER_CALL:int  = 20;
		
		public static const MAX_APPLICATION_HEIGHT:int  = 730;
		public static const MAX_APPLICATION_WIDTH:int  = 827;
		
		public static const MAX_WRAPPED_APPLICATION_HEIGHT:int  = 4050;
		public static const MAX_WRAPPED_APPLICATION_WIDTH:int  = MAX_APPLICATION_WIDTH;
		
		public static const MAX_MENU_LABEL_LENGTH:int  = 17;
		public static const MAX_AUDIO_SEARCH_RESULTS:int  = 30;
		
	}
}