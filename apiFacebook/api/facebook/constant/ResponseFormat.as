package api.facebook.constant {
	
	public final class ResponseFormat {
		
		public static const XML:String  = 'XML';
		public static const JSON:String  = 'JSON';
		
	}
}