package api.facebook.ui {

	import api.facebook.constant.APIRestriction;
	import api.facebook.model.FacebookApplicationModel;
	
	import flash.events.Event;
	
	import spark.components.Application;


	public class FacebookApplication extends Application {

		function FacebookApplication() {
			maxWidth = APIRestriction.MAX_APPLICATION_WIDTH;
			maxHeight = APIRestriction.MAX_APPLICATION_HEIGHT;

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}		
		
		private function get appModel():FacebookApplicationModel {
			return VyanaContext.getInstance().model as FacebookApplicationModel;
		}
		
		override protected function initializationComplete():void {
			super.initializationComplete();			
				
			VyanaContext.getInstance();
		}
		
		protected function onAddedToStage(e:Event):void {			
			if (appModel)
				appModel.init();
		}		

	}
}