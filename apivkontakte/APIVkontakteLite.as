package {

	import api.vkontakte.commands.friends.*;
	import api.vkontakte.commands.general.*;
	import api.vkontakte.commands.user.*;
	import api.vkontakte.commands.wrapper.*;
	
	import org.vyana.control.commands.*;

	/**
	 * Commands registry class, each command represented as separate constant
	 */
	public final class APIVkontakteLite extends VyanaCommandRegistry {

		// Wrapper dependent commands
		public static const INSTALL_APPLICATION:* = InstallApplicationCommand;
		public static const REFRESH_PAGE:* = RefreshPageCommand;
		public static const SET_APPLICATION_VIEW_SIZE:* = SetApplicationViewSizeCommand;
		public static const SCROLL_WINDOW:* = ScrollWindowCommand;
		public static const ASK_PERMISSION:* = AskUserPermissionCommand;
		public static const ASK_PERMISSIONS_SET:* = AskUserPermissionsSetCommand;
		public static const ASK_VOTES:* = AskForVotesCommand;
		public static const INVITE_FRIENDS:* = InviteFriendsCommand;
		
		// Friends
		public static const GET_FRIENDS:* = GetFriendsCommand;
		public static const GET_APP_FRIENDS:* = GetAppFriendsCommand;
		public static const GET_FRIENDS_LISTS:* = GetFriendsListsCommand;		

		// Users		
		public static const IS_APP_USER:* = IsAppUserCommand;
		public static const OPEN_USER_PROFILE:* = OpenUserProfileCommand;
		public static const GET_PROFILE_FIELDS:* = GetProfileFieldsCommand;
		public static const GET_COUNTERS:* = GetCountersCommand;
		public static const GET_USER_BALANCE:* = GetUserBalanceCommand;
		public static const GET_USER_SETTINGS:* = GetUserSettingsCommand;
		public static const GET_USER_GROUPS:* = GetUserGroupsCommand;
		public static const GET_USER_GROUPS_FULL:* = GetUserGroupsFullCommand;
		
		// General
		public static const EXECUTE_VKSCRIPT:* = ExecuteVKScriptCommand;
		public static const GET_LOCALIZATION_VALUES:* = GetLocalizationValuesCommand;		
		public static const GET_ADVERTISEMENTS:* = GetAdvertisementsCommand;
		public static const GET_SERVER_TIME:* = GetServerTimeCommand;
		public static const GET_VARIABLE:* = GetVariableCommand;
		public static const GET_VARIABLES:* = GetVariablesCommand;
		public static const PUT_VARIABLE:* = PutVariableCommand;
		public static const SET_MENU_LABEL:* = SetMenuLabelCommand;
		public static const SET_MENU_COUNTER:* = SetMenuCounterCommand;
		public static const OPEN_APPLICATION_PAGE:* = OpenApplicationPageCommand;
		public static const OPEN_AUTHOR_PROFILE:* = OpenAuthorProfileCommand;
		
		// Extra
		public static const PUT_LONG_VARIABLE:* = PutLongVariableCommand;
		public static const GET_LONG_VARIABLE:* = GetLongVariableCommand;
		public static const CLEAR_LONG_VARIABLE:* = ClearLongVariableCommand;

	}

	new APIVkontakteLite();

}