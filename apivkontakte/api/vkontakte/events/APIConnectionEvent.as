package api.vkontakte.events {

	import flash.events.Event;

	/**
	 * @author Andrew Rogozov
	 */
	public class APIConnectionEvent extends Event {
		
		public static const CONN_INIT:String = "onConnectionInit";
		public static const WINDOW_BLUR:String = "onWindowBlur";
		public static const WINDOW_FOCUS:String = "onWindowFocus";
		public static const APP_ADDED:String = "onApplicationAdded";
		public static const WALL_SAVE:String = "onWallPostSave";
		public static const WALL_CANCEL:String = "onWallPostCancel";
		public static const PHOTO_SAVE:String = "onProfilePhotoSave";
		public static const PHOTO_CANCEL:String = "onProfilePhotoCancel";
		public static const BALANCE_CHANGED:String = 'onBalanceChanged';		
		public static const SETTINGS_CHANGED:String = 'onSettingsChanged';

		public var data:Object = {};
		public var params:Array = [];

		public function APIConnectionEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			
		}

	}
}