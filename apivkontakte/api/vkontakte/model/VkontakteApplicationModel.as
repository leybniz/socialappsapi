package api.vkontakte.model {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.general.GetLocalizationValuesCommand;
	import api.vkontakte.constant.InitialAPIResultField;
	import api.vkontakte.constant.Locale;
	import api.vkontakte.constant.ResponseFormat;
	import api.vkontakte.model.vo.*;
	
	import com.adobe.serialization.json.JSON;
	
	import mx.core.FlexGlobals;
	import mx.resources.ResourceManager;
	
	import org.vyana.control.VyanaEvent;
	import org.vyana.model.VyanaModel;

	public class VkontakteApplicationModel extends VyanaModel {
		
		public var appDescriptor:VkontakteApplicationDescriptor;		
		public var viewer:Viewer;
		
		function VkontakteApplicationModel(app_id:String, app_secret:String, author_id:String) {			
			
			appDescriptor = new VkontakteApplicationDescriptor(app_id, app_secret, author_id, flashvars);
			
			// Does this app stolen?
			if (appDescriptor.isStolen) {
				// Neverending loop ;)
				do {
					// Nirvana :)
				} while (true); 
			}
			
		}
		
		protected function get app():* {
			return FlexGlobals.topLevelApplication;
		}

		public function get flashvars():Object {
			return VkontakteGlobals.wrapper ? VkontakteGlobals.wrapper.application.parameters : app.parameters;
		}
		
		override public function init():void {
			super.init();
			
			with (FlexGlobals.topLevelApplication) {
				if (parent.parent && parent.parent.hasOwnProperty('scaleX'))
					scaleX = scaleY = 1 / parent.parent.scaleX;
			}			
			
			viewer = new Viewer(appDescriptor, flashvars);
			appDescriptor.flashvars = flashvars;
			
			if (flashvars.hasOwnProperty('api_result')) {
				
				if (appDescriptor.responseFormat == ResponseFormat.JSON)
					VkontakteGlobals.initialResult = Object(com.adobe.serialization.json.JSON.decode(flashvars.api_result)).response;
				
				if (appDescriptor.responseFormat == ResponseFormat.XML)
					VkontakteGlobals.initialResult = new XML(flashvars.api_result);				
				
			}
			
			// In case if you are using vkontakte.ru provided localization solution
			if (appDescriptor.useVkontakteLocalization) {
				
				// and you have your application configured to be given api_result
				// First API request: method=execute&code=return {"lng": API.language.getValues({all:1})};&format=json
				if (appDescriptor.localizationAsAPIResult && appDescriptor.isProduction && VkontakteGlobals.initialResult && VkontakteGlobals.initialResult.hasOwnProperty(InitialAPIResultField.LANGUAGE)) {
					if (appDescriptor.responseFormat == ResponseFormat.JSON)					
						GetLocalizationValuesCommand.fillLocalizationValuesDictionary(appDescriptor.localizationValues, VkontakteGlobals.initialResult[InitialAPIResultField.LANGUAGE], ResponseFormat.JSON);
				} else {
					// Request localization values to be filled into appDescriptor.localizationValues
					new VyanaEvent(APIVkontakte.GET_LOCALIZATION_VALUES, appDescriptor.localizationValues).dispatch();					
				}
				
			} else {
				// Set application locale, in case you are using native Flex framework provided localization solution
				var localeChain:Array = [Locale.ru_RU];
				localeChain.unshift(appDescriptor.locale);
				ResourceManager.getInstance().localeChain = localeChain;				
			}
			
			
			var u:UserInfo;
			if (VkontakteGlobals.initialResult) {
				
				if (VkontakteGlobals.initialResult.hasOwnProperty(InitialAPIResultField.VIEWER_INFO)) {
					u = new UserInfo(VkontakteGlobals.initialResult[InitialAPIResultField.VIEWER_INFO][0]);
					VkontakteGlobals.userInfoCache[u.id] = u; 
				}
				
				if (VkontakteGlobals.initialResult.hasOwnProperty(InitialAPIResultField.USER_INFO)) {
					if (VkontakteGlobals.initialResult[InitialAPIResultField.USER_INFO] is Array) {
						u = new UserInfo(VkontakteGlobals.initialResult[InitialAPIResultField.USER_INFO][0]);
						VkontakteGlobals.userInfoCache[u.id] = u;
					}
				}
			}			
			
		}

	}

}