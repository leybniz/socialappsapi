package api.vkontakte.model.vo {
	
	import api.vkontakte.constant.APIVersion;
	import api.vkontakte.constant.Language;
	import api.vkontakte.constant.ResponseFormat;
	
	import flash.net.LocalConnection;
	import flash.utils.Dictionary;
	
	import org.vyana.model.vo.VyanaValueObject;

	public class VkontakteApplicationDescriptor extends VyanaValueObject {
		
		/**
		 * Default Session values for TestMode in development phase 
		 */
		public static var TEST_SESSION_ID:String = '';
		public static var TEST_SESSION_SECRET:String = '';
		
		
		/**
		 * Defines API response format
		 * default: format is JSON, because it's much faster than XML
		 */
		public var responseFormat:String = ResponseFormat.JSON;
		
		
		/**
		 * Use vkontakte.ru provided localization solution 
		 */
		public var useVkontakteLocalization:Boolean = true;
		
		/**
		 * Treat flashvars.api_result as localization values list 
		 */
		public var localizationAsAPIResult:Boolean = true;
		
		public var localizationValues:Dictionary = new Dictionary();

		protected var lc:LocalConnection = new LocalConnection();
		protected var _id:String;
		protected var _secret:String;
		protected var _authorId:String;
		protected var _testMode:Boolean = false;
		protected var _flashvars:Object;
		
		function VkontakteApplicationDescriptor(id:String, secret:String, author:String, fVars:Object) {
			
			this._id = id;
			this._secret = secret;
			this._authorId = author;
			this.flashvars = fVars ? fVars: {};
			
			testMode = !isProduction;
			
			if (APIVersion.VERSION == APIVersion.V30)
				this._secret = testMode ? TEST_SESSION_SECRET : fVars.secret;
		}

		public function get flashvars():Object {
			return _flashvars;
		}

		public function set flashvars(value:Object):void {
			_flashvars = value;
		}

		public function get isProduction():Boolean {
			return flashvars.hasOwnProperty('secret');
		}		
		
		public function get isStolen():Boolean {
			return isProduction && id.toUpperCase() != String(flashvars.api_id).toUpperCase();
		}
		
		public function get isSecondInstance():Boolean {
			try {				
				lc.connect(id);
			}
			catch (e:Error) {
				return true;
			}
			
			return false;
		}
		
		public function get id():String {
			return _id;
		}
		
		public function get secret():String {
			return _secret;
		}

		public function get authorId():String {
			return _authorId;
		}
		
		public function get posterId():String {
			return flashvars.poster_id;
		}
		
		public function get postId():String {
			return flashvars.post_id;
		}		
		
		public function get testMode():Boolean {
			return _testMode;
		}
		
		public function set testMode(value:Boolean):void {
			_testMode = value;
		}
		
		public function get referrer():String {
			return flashvars.referrer;
		}

		public function get language():int {
			return parseInt(flashvars.language);
		}

		public function get locale():String {
			return Language.toLocale(language);
		}

	}
}