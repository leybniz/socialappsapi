package api.vkontakte.model.vo {

	import api.vkontakte.constant.UserSettings;
	import api.vkontakte.constant.ViewerType;
	
	import org.vyana.model.vo.VyanaValueObject;

	public class Viewer extends VyanaValueObject {

		protected var flashvars:*;
		protected var applicationDescriptor:VkontakteApplicationDescriptor;
		protected var userSettings:UserSettings;
		protected var userPreferences:UserPreferences;
		
		private var _isAppUser:Boolean;

		function Viewer(appDescriptor:VkontakteApplicationDescriptor, parameters:*) {

			applicationDescriptor = appDescriptor;
			this.flashvars = parameters ? parameters : {};
			_isAppUser = flashvars.is_app_user == '1'; 

			userSettings = new UserSettings(parseInt(flashvars.api_settings));
		}

		public function get preferences():UserPreferences {
			return userPreferences;
		}

		public function set preferences(value:UserPreferences):void {
			userPreferences = value;
		}

		public function get settings():UserSettings {
			return userSettings;
		}

		public function get sessionId():String {
			return flashvars.sid;
		}
		
		public function get type():String {
			return applicationDescriptor.testMode ? ViewerType.GUEST : flashvars.viewer_type;
		}

		public function get id():String {
			return applicationDescriptor.testMode ? applicationDescriptor.authorId : flashvars.viewer_id;
		}

		public function get userId():String {
			return applicationDescriptor.testMode ? applicationDescriptor.authorId : flashvars.user_id;
		}

		public function get groupId():String {
			return applicationDescriptor.testMode ? '0' : flashvars.group_id;
		}
		
		public function get isSelfViewer():Boolean {
			return (userId == id || userId == '0') && isAppUser;
		}

		public function get isPublicGuest():Boolean {
			return userId == '0' && !isAppUser;
		}
		
		public function get isGuest():Boolean {
			return userId != '0' && !isAppUser;
		}

		public function get isAuthor():Boolean {
			return applicationDescriptor.testMode ? true : flashvars.viewer_id == applicationDescriptor.authorId;
		}

		public function set isAppUser(value:Boolean):void {
			_isAppUser = value;
		}

		public function get isAppUser():Boolean {
			return applicationDescriptor.testMode ? true : _isAppUser;
		}

		public function get authKey():String {
			var result:String = flashvars.auth_key;
			
			// Skip leading meaningless zero, if any
			while (result && result.charAt(0) == '0')
				result = result.substr(1);
			
			return result;
		}
		
		/**
		 * Referrer, Group or User 
		 */
		public function get referrer():String {
			return userId == '0' ? (groupId == '0' ? id : '-' + groupId) : userId;
		}

	}
}