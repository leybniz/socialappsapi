package api.vkontakte.model.vo {
	
	import api.vkontakte.constant.Gender;
	import api.vkontakte.constant.NameCase;
	import api.vkontakte.constant.UserProfileField;
	
	import flash.utils.Dictionary;
	
	import mx.formatters.DateFormatter;
	
	import org.vyana.model.vo.VyanaValueObject;

	public dynamic class UserInfo extends VyanaValueObject {
		
		public static const millisecondsPerMinute:Number = 1000 * 60;
		public static const millisecondsPerHour:Number = millisecondsPerMinute * 60;
		public static const millisecondsPerDay:Number = millisecondsPerHour * 24;		
		public static const millisecondsPerYear:Number = millisecondsPerDay * 365;
		
		public var nameCases:Dictionary;
		public var nameCase:String = NameCase.NOM;
		
		function UserInfo(initObj:Object = null) {
			
			// Copy props from initial object
			if (initObj)
				for (var p:String in initObj)
					this[p] = initObj[p];
			
		}
		
		public function get id():String {
			return this[UserProfileField.UID];
		}

		public function get isMale():Boolean {
			return this[UserProfileField.SEX] == Gender.MALE;
		}
		
		public function get age():Number {
			var result:Number = NaN;
			var bd:String = this[UserProfileField.BIRTH_DATE]; 
			
			if (!bd || bd.length < 10)
				return result;
			
			var parts:Array = bd.split('.');
			var bDate:Date = new Date(parseInt(parts[2]), parseInt(parts[1]), parseInt(parts[0]), 0, 0, 0, 0);
			result = Math.floor((new Date().time - bDate.time) / millisecondsPerYear);
			
			return result;
		}
		
		public function get hasMobile():Boolean {
			return this[UserProfileField.HAS_MOBILE] == '1';
		}
		
		public function get fullName():String {
			var result:Array = [];
			
			result.push(this[UserProfileField.FIRST_NAME]);
			result.push(this[UserProfileField.LAST_NAME]);
			
			return result.join(' ');
		}
		
		public function registerNameCase(obj:Object, nameCase:String):void {
			var c:Object = {};
			c[UserProfileField.FIRST_NAME] = obj[UserProfileField.FIRST_NAME];
			c[UserProfileField.LAST_NAME] = obj[UserProfileField.LAST_NAME];
			
			if (!nameCases)
				nameCases = new Dictionary();
			
			nameCases[nameCase] = c;
		}
		
	}
}