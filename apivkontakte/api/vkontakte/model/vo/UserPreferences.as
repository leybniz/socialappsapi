package api.vkontakte.model.vo {

	import api.vkontakte.constant.StringBoolean;
	
	import flash.utils.Dictionary;
	
	import org.vyana.control.VyanaEvent;
	import org.vyana.model.vo.VyanaValueObject;

	public class UserPreferences extends VyanaValueObject {

		public static const FLAGS:uint = 0;
		public static const EQUALS:String = '=';
		public static const SEPARATOR:String = ',';
		
		public static var DEFAULT_STORAGE_VAR:int = 1279;		

		protected var preferences:MightyArray = new MightyArray();
		protected var prefValues:Dictionary = new Dictionary(true);

		function UserPreferences(prefs:String = '') {
			parsePreferences(prefs);
		}
		
		public function get flags():Number {
			return parseFloat(prefValues[FLAGS]);
		}
		
		public function registerPreference(p:UserPreference):void {			
			if (p)
				preferences.addItem(p);
		}
		
		public function getPreferenceById(prefId:uint):UserPreference {
			return preferences.toDictionary('id')[prefId];
		}

		public function setValue(prefId:uint, value:String = ''):void {
			var pref:UserPreference = getPreferenceById(prefId);
			
			if (!pref)
				return;
			
			// Same value no point to save
			if (getValue(prefId) == value)
				return;

			if (pref.isBoolean) {
				prefValues[FLAGS] = String((value == StringBoolean.TRUE) ? flags | pref.id : flags & ~pref.id);
			} else {
				prefValues[pref.id] = nvl(value, pref.defaultValue);
			}
			
			save();
		}

		public function getValue(prefId:uint):String {
			var pref:UserPreference = getPreferenceById(prefId);
			
			if (!pref)
				return null;
			
			var result:String = prefValues[pref.id];

			if (!result && pref.isBoolean) {
				var flags:uint = parseInt(prefValues[FLAGS]);
				result = (flags & pref.id) > 0 ? StringBoolean.TRUE : StringBoolean.FALSE;
			}

			if (!result)
				result = pref.defaultValue;

			return result;
		}
		
		public function toString():String {
			var result:MightyArray = new MightyArray();

			for (var key:String in prefValues)
				if (prefValues[key])
					result.addItem(key + EQUALS + prefValues[key]);
				else
					result.addItem(key + EQUALS + getPreferenceById(parseInt(key)).defaultValue);

			return result.join(SEPARATOR);
		}
		
		public function save():void {
			new VyanaEvent(APIVkontakte.PUT_VARIABLE, 
				{
					key: DEFAULT_STORAGE_VAR, 
					value: toString()
				}
			).dispatch();
		}
		
		public function load(visitorFunction:Function = null):void {			
			new VyanaEvent(APIVkontakte.GET_VARIABLE, 
				{
					key: DEFAULT_STORAGE_VAR
				}
			).dispatch(
				function(r:*):void {					
					parsePreferences(r.response);
					
					if (visitorFunction != null)
						visitorFunction();
				}
			);			
		}
		
		protected function nvl(value:*, def:*):* {
			var result:* = value;
			
			if (value is String && isEmptyString(value))
				result = def;
			
			if (!result)
				result = def;
			
			return result;
		}
		
		protected function isEmptyString(value:String):Boolean {
			return (value == '' || value == null || value == 'null' || value == 'undefined');
		}
		
		protected function parsePreferences(prefs:String):void {
			
			if (!isEmptyString(prefs))
				for each (var token:String in prefs.split(SEPARATOR))
					prefValues[token.split(EQUALS)[0]] = token.split(EQUALS)[1];
			
		}

	}
}