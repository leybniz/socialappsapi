package api.vkontakte.model.vo.audio {
	
	import org.vyana.model.vo.VyanaValueObject;

	public class AudioItemKey extends VyanaValueObject {
		
		public static const SEPARATOR:String = '_'; 
		
		public var audioId:String;
		public var ownerId:String;
		
		function AudioItemKey(aKey:String = '') {
			parseKey(aKey);
		}
		
		public function set key(value:String):void {
			parseKey(value);
		}

		public function get key():String {
			return toString();
		}
		
		public function toString():String {
			var result:Array = [];
			
			result.push(ownerId);
			result.push(audioId);
			
			return result.join(SEPARATOR); 
		}
		
		protected function parseKey(aKey:String):void {
			var keyParts:Array = aKey.split(SEPARATOR);
			
			if (keyParts.length < 2)
				return;
			
			ownerId = keyParts[0];
			audioId = keyParts[1];
				
		}
	}
}