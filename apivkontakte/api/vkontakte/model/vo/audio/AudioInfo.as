package api.vkontakte.model.vo.audio {
	
	import flash.events.IOErrorEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.system.System;
	import flash.text.TextField;
	
	import mx.formatters.DateFormatter;
	
	import org.vyana.model.vo.*;
	
	public dynamic class AudioInfo extends VyanaValueObject {
		
		public var key:AudioItemKey;
		public var soundChannel:SoundChannel;
		
		function AudioInfo(initObj:Object = null) {
			
			// Copy props from initial object
			if (initObj)
				for (var p:String in initObj)
					this[p] = initObj[p];
			
			key = new AudioItemKey(this.owner_id + '_' + this.aid);  
		}
		
		public function get displayTitle():String {
			return unescapeHTML(this.title);
		}
		
		public function get displayArtist():String {
			return unescapeHTML(this.artist);
		}
		
		public function get displayDuration():String {
			return formatDuration(this.duration);
		}
		
		public function get displayName():String {
			return displayArtist + ' » ' + displayTitle + ' » ' + displayDuration;
		}
		
		public function get displayHTMLName():String {
			return '<font size="9"><font color="#2B587A"><b>' + displayArtist + '</b></font> - ' + displayTitle + ' ' + displayDuration + '</font>';
		}

		public function get label():String {
			var result:Array = [];
			
			if (this.hasOwnProperty('artist'))
				result.push(displayArtist); 
					
			if (this.hasOwnProperty('title'))
				result.push(displayTitle);
			
			return result.join(' ');
		}		
		
		public function copyToClipboard():void {
			System.setClipboard(label);
		}
		
		public function stop():void {
			if (soundChannel)
				soundChannel.stop();
		}

		public function play():SoundChannel {
			var s:Sound = new Sound();
			s.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			s.load(new URLRequest(this['url']));
			
			if (soundChannel)
				soundChannel.stop();
			
			if (s) {
				soundChannel = s.play();
			}
			
			return soundChannel;
		}
		
		protected function unescapeHTML(s:String):String {
			var tf:TextField = new TextField();
			tf.htmlText = s;
			var result:String = tf.text;			
			return result; 
		}
		
		protected function formatDuration(seconds:int):String {
			var d:DateFormatter = new DateFormatter();
			d.formatString = 'N:SS';			
			return d.format(new Date(seconds * 1000)); 
		}
		
		protected function onIOError(e:IOErrorEvent):void {
			
		}
		
	}
}