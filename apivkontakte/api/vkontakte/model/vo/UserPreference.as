package api.vkontakte.model.vo {
	
	import api.vkontakte.constant.StringBoolean;
	
	import org.vyana.model.vo.VyanaValueObject;

	public class UserPreference extends VyanaValueObject {
		
		public var id:uint;
		public var isBoolean:Boolean;
		
		function UserPreference(id:uint, isBoolean:Boolean = false) {
			this.id = id;
			this.isBoolean = isBoolean;
		}
		
		public function get defaultValue():String {
			return isBoolean ? StringBoolean.FALSE : null;
		}
		
	}
}