package api.vkontakte.commands.general {
	
	import api.vkontakte.constant.ResponseFormat;
	
	import flash.utils.Dictionary;
	
	import mx.resources.ResourceManager;
	
	import org.vyana.control.VyanaEvent;

	public class GetLocalizationValuesCommand extends AbstractGeneralCommand {
		
		public static function fillLocalizationValuesDictionary(lvd:Dictionary, obj:*, objFormat:String):void {
			switch (objFormat) {
				
				case ResponseFormat.JSON : {
					
					for (var key:String in obj)
						lvd[key] = obj[key];
					
				} break;
				
			}
			
			ResourceManager.getInstance().update();
		}
		
		override protected function set Result(value:*):void {			
			
			if (data is Dictionary) {
				
				fillLocalizationValuesDictionary(data, value.response, applicationModel.appDescriptor.responseFormat);
				
				return;
			}
			
			super.Result = value;			
		}		
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (data is Dictionary) {
				delegate.getLocalizationValues();
				return;
			}
			
			delegate.getLocalizationValues(data.language, data.keys, data.all);
		}
		
	}
}