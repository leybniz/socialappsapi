package api.vkontakte.commands.general {
	
	import api.vkontakte.business.delegates.AbstractVkontakteServiceDelegate;
	
	import flash.events.ProgressEvent;
	
	import mx.rpc.events.ResultEvent;
	
	import org.vyana.control.VyanaEvent;

	public class ClearLongVariableCommand extends AbstractGeneralCommand {
		
		protected var resultsCount:int;
		protected var queriesCount:int;
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);			

			var key:int = int(data.key);
			queriesCount = data.numVars;
			
			// Clear vars
			for (var i:int = key; i < key + queriesCount; i++)
				delegate.putVariable(i, '', data.uid);
			
		}
		
		override protected function onResult(e:ResultEvent):void {
			AbstractVkontakteServiceDelegate.requestsQueue.startProcessing();
			
			resultsCount++;
			
			if (resultsCount == queriesCount)
				super.onResult(e);
			
			context.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, resultsCount, queriesCount));
		}
		
	}
}