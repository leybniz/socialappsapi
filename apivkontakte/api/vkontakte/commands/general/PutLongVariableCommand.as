package api.vkontakte.commands.general {

	import api.vkontakte.business.delegates.AbstractVkontakteServiceDelegate;
	import api.vkontakte.constant.APIRestriction;
	
	import flash.events.ProgressEvent;
	
	import mx.rpc.events.ResultEvent;
	
	import org.vyana.control.VyanaEvent;

	public class PutLongVariableCommand extends AbstractGeneralCommand {

		protected var resultsCount:int;
		protected var queriesCount:int = 1;
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!data.uid)
				data.uid = applicationModel.viewer.id;			
			
			var value:String = new String(compressString(data.value));
			var varsRequired:int = Math.ceil(value.length / APIRestriction.MAX_VARIABLE_SIZE);
			var i:int = int(data.key);
			
			// Write vars count occupied by the Long data passed
			delegate.putVariable(i, varsRequired.toString(), data.uid);
			
			// Write long data var by MAX_VARIABLE_SIZE chunks
			queriesCount += varsRequired; 
			while (value.length > 0) {				
				delegate.putVariable(++i, value.substr(0, APIRestriction.MAX_VARIABLE_SIZE), data.uid);
				value = value.slice(APIRestriction.MAX_VARIABLE_SIZE);
			}	
		}
		
		override protected function onResult(e:ResultEvent):void {
			AbstractVkontakteServiceDelegate.requestsQueue.startProcessing();
			
			resultsCount++;
			
			if (resultsCount == queriesCount)
				super.onResult(e);
			
			context.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, resultsCount, queriesCount));
		}

	}
}