package api.vkontakte.commands.general {

	import api.vkontakte.business.delegates.GeneralVkontakteSeviceDelegate;
	
	import mx.rpc.IResponder;
	
	import org.vyana.control.VyanaEvent;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractGeneralCommand extends VkontakteCommand {

		protected var delegate:GeneralVkontakteSeviceDelegate = new GeneralVkontakteSeviceDelegate(this as IResponder);
	
		override public function execute(e:VyanaEvent):void {
			super.execute(e);			
			
		}		
		
	}
}