package api.vkontakte.commands.general {
	
	import api.vkontakte.constant.VKontakteURL;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import org.vyana.control.VyanaEvent;

	public class OpenGetVotesPageCommand extends AbstractGeneralCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			navigateToURL(new URLRequest(VKontakteURL.payments), '_blank');
		}		
		
	}
}