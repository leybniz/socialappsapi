package api.vkontakte.commands.general {
	
	import api.vkontakte.constant.VKontakteURL;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import org.vyana.control.VyanaEvent;
	import org.vyana.control.commands.Command;

	public class OpenAuthorProfileCommand extends AbstractGeneralCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			navigateToURL(new URLRequest(VKontakteURL.profilePage + applicationModel.appDescriptor.authorId), '_blank');
		}		
		
	}
}