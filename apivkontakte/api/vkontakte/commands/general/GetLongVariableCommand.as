package api.vkontakte.commands.general {

	import api.vkontakte.constant.APIRestriction;
	
	import flash.events.ProgressEvent;
	
	import mx.rpc.events.ResultEvent;
	
	import org.vyana.control.VyanaEvent;

	public class GetLongVariableCommand extends AbstractGeneralCommand {

		protected var valChunksArray:MightyArray = new MightyArray();
		protected var chunksToGet:int;
		protected var chunksTotal:int;
		
		override protected function set Result(value:*):void {			
			if (isJSON)
				value.data = data;			
			super.Result = value;			
		}		
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!data.uid)
				data.uid = applicationModel.viewer.id;			
			
			new VyanaEvent(APIVkontakte.GET_VARIABLE, 
				{ 
					key : data.key, 
					uid : data.uid, 
					session : data.session
				}
			).dispatch(
				function (result:*):void {
					
					var varsCount:int;
					
					if (isJSON)
						varsCount = chunksTotal = chunksToGet = parseInt(result.response);
					
					if (result is XML)
						varsCount = chunksTotal = chunksToGet = parseInt(XML(result).text());
					
					// Var is empty
					if (varsCount == 0) {
						
						// Pass empty result to handler
						if (isJSON)
							onResult(ResultEvent.createEvent({response:''}));
						
						if (isXML)
							onResult(ResultEvent.createEvent(<response />));						
						
						return;
					}
					
					context.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, 0, chunksTotal));

					var key:int = parseInt(data.key) + 1;
					var n:int = (varsCount <= APIRestriction.MAX_VARIABLES_PER_READ_BATCH) ? varsCount : APIRestriction.MAX_VARIABLES_PER_READ_BATCH;
					
					while (varsCount > 0) {
						
						new VyanaEvent(APIVkontakte.GET_VARIABLES, 
							{ 
								key : key,
								count : n,  
								uid : data.uid, 
								session : data.session
							}
						).dispatch(
							function (result:*):void {								
								
								// Put readed chunks to dictionary
								if (isJSON) {									
									
									chunksToGet -= (result.response as Array).length;									
									valChunksArray.append(result.response);									
									context.dispatchEvent(new ProgressEvent(ProgressEvent.PROGRESS, false, false, chunksTotal - chunksToGet, chunksTotal));
									
									if (chunksToGet == 0) {
										// Super! we have all chunks ;)
										var value:String = '';										
										valChunksArray.sortByField('key', false, false, true);
										for each (var chunk:* in valChunksArray)
											value += chunk.value;
											
										// Pass result to handler
										onResult(ResultEvent.createEvent({response:decompressString(value)}));
									}											
								}
								
								// TODO: Implement XML results handling as well as JSON
								if (isXML) {
									
								}
																
							}
						);
						
						varsCount -= n;
						key += n;
						n = (varsCount <= APIRestriction.MAX_VARIABLES_PER_READ_BATCH) ? varsCount : APIRestriction.MAX_VARIABLES_PER_READ_BATCH;						
					}
					
				}
			);
			
		}		

	}
}