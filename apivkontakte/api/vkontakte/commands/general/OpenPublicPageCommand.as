package api.vkontakte.commands.general {
	
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.VKontakteURL;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import org.vyana.control.VyanaEvent;

	public class OpenPublicPageCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			var url:String = VKontakteURL.publicPage + data;
			
			navigateToURL(new URLRequest(url), '_blank');
		}		
		
	}
}