package api.vkontakte.commands.general {
	
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.VKontakteURL;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import org.vyana.control.VyanaEvent;

	public class OpenApplicationPageCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			var url:String = VKontakteURL.appPage + applicationModel.appDescriptor.id;
			
			if (data is String)
				url += '_' + data;
			
			navigateToURL(new URLRequest(url), '_blank');
		}		
		
	}
}