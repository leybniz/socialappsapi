package api.vkontakte.commands.general {
	
	import org.vyana.control.VyanaEvent;
	
	public class SetMenuCounterCommand extends AbstractGeneralCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.setMenuCounter(data);
		}
		
	}
}