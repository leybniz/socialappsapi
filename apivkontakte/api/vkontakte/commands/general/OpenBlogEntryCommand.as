package api.vkontakte.commands.general {
	
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.VKontakteURL;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import org.vyana.control.VyanaEvent;

	public class OpenBlogEntryCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			navigateToURL(new URLRequest(VKontakteURL.blogPage + data), '_blank');
		}		
		
	}
}