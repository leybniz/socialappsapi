package api.vkontakte.commands.general {
	
	import org.vyana.control.VyanaEvent;

	public class PutVariableCommand extends AbstractGeneralCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!data.uid)
				data.uid = applicationModel.viewer.id;			
			
			delegate.putVariable(data.key, data.value, data.uid);
		}		

	}
}