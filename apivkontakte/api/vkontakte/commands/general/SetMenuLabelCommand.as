package api.vkontakte.commands.general {
	
	import org.vyana.control.VyanaEvent;
	
	public class SetMenuLabelCommand extends AbstractGeneralCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.setMenuLabel(data);
		}
		
	}
}