package api.vkontakte.commands.general {
	
	import org.vyana.control.VyanaEvent;

	public class GetVariableCommand extends AbstractGeneralCommand {

		override protected function set Result(value:*):void {			
			value.data = data;			
			super.Result = value;			
		}

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!data.uid)
				data.uid = applicationModel.viewer.id;			

			delegate.getVariable(data.key, data.uid, data.session);
		}		

	}
}