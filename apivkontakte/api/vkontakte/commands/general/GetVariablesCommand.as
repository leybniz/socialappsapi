package api.vkontakte.commands.general {

	import org.vyana.control.VyanaEvent;

	public class GetVariablesCommand extends AbstractGeneralCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getVariables(data.key, data.count, data.uid);
		}
		
	}
}