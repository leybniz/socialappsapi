package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.*;
	import api.vkontakte.events.APIConnectionEvent;
	
	import org.vyana.control.VyanaEvent;
	
	public class AskUserPermissionsSetCommand extends VkontakteCommand {
		
		protected var permissions:MightyArray;
		protected var flags:Number = 0;
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			permissions = new MightyArray(data);
			
			for each (var p:Number in permissions)
				flags |= p;
			
			if (VkontakteGlobals.wrapper) {
				VkontakteGlobals.wrapper.addEventListener(WrapperEventType.SETTINGS_CHANGED, onSettingsChanged);
				VkontakteGlobals.wrapper.external.callMethod('showSettingsBox', flags);
			} else
				if (localConnectionDelegate) {
					localConnectionDelegate.addEventListener(APIConnectionEvent.SETTINGS_CHANGED, onSettingsChanged);
					localConnectionDelegate.invokeMethod('showSettingsBox', flags);
				} else {
					Result = StringBoolean.FALSE;
					return;
				}			
			
		}
		
		protected function onSettingsChanged(e:Object = null):void {
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.removeEventListener(WrapperEventType.SETTINGS_CHANGED, onSettingsChanged);
			else
				if (localConnectionDelegate)
					localConnectionDelegate.removeEventListener(APIConnectionEvent.SETTINGS_CHANGED, onSettingsChanged);
		
			var settings:UserSettings;
			if (e is APIConnectionEvent) {
				settings = new UserSettings(parseInt(e.params[0]));
			} else {
				settings = new UserSettings(parseInt(e.settings));
			}			
			
			var appliedCount:uint = 0;
			
			for each (var p:Number in permissions)
				if (settings.isSet(p)) {
					applicationModel.viewer.settings.setFlag(p);
					appliedCount++;
				} else {
					// Non mandatory settings, nice to have ones
					if (p == UserSettings.MENU_NAVIGATION_GRANTED || p == UserSettings.WALL_POST_GRANTED)
						appliedCount++;
				}
			
			Result = appliedCount ==  permissions.length ? StringBoolean.TRUE : StringBoolean.FALSE;	
		}
		
	}
}