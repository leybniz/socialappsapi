package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.APIRestriction;
	
	import org.vyana.control.VyanaEvent;

	public class SetApplicationViewSizeCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (VkontakteGlobals.wrapper) {
				var width:uint = VkontakteGlobals.wrapper.application.stageWidth;
				var height:uint = VkontakteGlobals.wrapper.application.stageHeight;
				
				if (data.hasOwnProperty('width'))
					width = data.width > APIRestriction.MAX_WRAPPED_APPLICATION_WIDTH ? APIRestriction.MAX_WRAPPED_APPLICATION_WIDTH : data.width;
				
				if (data.hasOwnProperty('height'))
					height = data.height > APIRestriction.MAX_WRAPPED_APPLICATION_HEIGHT ? APIRestriction.MAX_WRAPPED_APPLICATION_HEIGHT : data.height;
				
				VkontakteGlobals.wrapper.external.callMethod('resizeWindow', width, height);
			} else
				if (localConnectionDelegate)
					localConnectionDelegate.invokeMethod('resizeWindow', data.width, data.height);
			
		}	
		
	}
}