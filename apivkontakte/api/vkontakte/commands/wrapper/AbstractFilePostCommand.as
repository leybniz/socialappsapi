package api.vkontakte.commands.wrapper {

	import api.vkontakte.commands.VkontakteCommand;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.utils.ByteArray;
	
	import mx.rpc.events.ResultEvent;
	
	import org.vyana.control.VyanaEvent;

	public class AbstractFilePostCommand extends VkontakteCommand {
		
		public const CR:String = '\r\n';
		public const CR2:String = CR + CR;
		public const D:String = '--';
		public const BOUNDARY:String = 'FLEX-API-VKONTAKTE';
		public const CONTENT_TYPE:String = 'multipart/form-data; boundary=' + BOUNDARY;
		
		public var HEADER1:String;
		public var HEADER2:String;		
		
		protected var loader:URLLoader;
		protected var sendBytes:ByteArray = new ByteArray();
		protected var fileBytes:ByteArray;
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			// Setup loader
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onUploadComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			
			// Prepare headers
			var headerBytes1:* = new ByteArray();
			headerBytes1.writeMultiByte(HEADER1, 'utf-8');
			var headerBytes2:* = new ByteArray();
			headerBytes2.writeMultiByte(HEADER2, 'utf-8');			
			
			// Compose sendBytes
			with (sendBytes) {
				writeBytes(headerBytes1, 0, headerBytes1.length);
				writeBytes(fileBytes, 0, fileBytes.length);
				writeBytes(headerBytes2, 0, headerBytes2.length);
			}
			
			// Setup request
			var request:URLRequest = data.request;
			request.data = sendBytes;
			request.method = URLRequestMethod.POST;
			request.contentType = CONTENT_TYPE;			
			
			// Pass loader up to be invoked in proper callStack to avoid
			// FP Security violation
			result(ResultEvent.createEvent(loader));
		}
		
		protected function onUploadComplete(e:Event):void {
			loader.removeEventListener(Event.COMPLETE, onUploadComplete);
			loader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
		}
		
		protected function onError(e:IOErrorEvent):void {
			loader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
		}		

	}
}