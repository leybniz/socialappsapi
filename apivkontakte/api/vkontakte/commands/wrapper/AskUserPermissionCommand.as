package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.*;
	import api.vkontakte.events.APIConnectionEvent;
	
	import org.vyana.control.VyanaEvent;
	
	public class AskUserPermissionCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			if (!applicationModel.viewer.settings.isSet(data)) {
				if (VkontakteGlobals.wrapper) {
					VkontakteGlobals.wrapper.addEventListener(WrapperEventType.SETTINGS_CHANGED, onSettingsChanged);
					VkontakteGlobals.wrapper.external.callMethod('showSettingsBox', data);
				} else
					if (localConnectionDelegate) {
						localConnectionDelegate.invokeMethod('showSettingsBox', data);
						localConnectionDelegate.addEventListener(APIConnectionEvent.SETTINGS_CHANGED, onSettingsChanged);
					} else					
						Result = StringBoolean.FALSE;
			} else {
				Result = StringBoolean.TRUE;
			}
			
		}
		
		protected function onSettingsChanged(e:Object = null):void {
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.removeEventListener(WrapperEventType.SETTINGS_CHANGED, onSettingsChanged);
			if (localConnectionDelegate)
				localConnectionDelegate.removeEventListener(APIConnectionEvent.SETTINGS_CHANGED, onSettingsChanged);			
			
			var settings:UserSettings;
			
			if (VkontakteGlobals.wrapper)
				settings = new UserSettings(parseInt(e.settings));
			
			if (localConnectionDelegate)
				if (e is APIConnectionEvent)
					settings = new UserSettings(parseInt(e.params[0]));
			
			if (settings.isSet(data)) {
				applicationModel.viewer.settings.setFlag(data);
				Result = StringBoolean.TRUE;			
			} else {
				Result = StringBoolean.FALSE;
			}
		}
		
	}
}