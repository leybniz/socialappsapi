package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	
	import flash.net.URLRequest;
	
	import org.vyana.control.VyanaEvent;
	
	public class NavigateUrlCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.navigateToURL(new URLRequest(data));
		}	
		
	}
}