package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.*;
	import api.vkontakte.model.vo.audio.AudioItemKey;
	
	import org.vyana.control.VyanaEvent;

	public class AddToMyAudiosCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			// Check for permission to modify user Audio list
			new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.AUDIOS_GRANTED).dispatch(
				function (r:String):void {
					if (r != StringBoolean.TRUE)
						return;
					
					addToMyAudios();
				}
			);			
			
		}
		
		protected function addToMyAudios():void {
			var audioKey:AudioItemKey = new AudioItemKey(data);			 
			
			new VyanaEvent(APIVkontakte.ADD_AUDIO, 
				{
					oid : audioKey.ownerId,
					aid : audioKey.audioId
				}
			).dispatch(
				function (r:*):void {
					
					Result = r;
				}
			);			
		}
		
	}
}