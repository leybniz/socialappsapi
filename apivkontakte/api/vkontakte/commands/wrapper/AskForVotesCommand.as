package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.*;
	import api.vkontakte.events.APIConnectionEvent;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.vyana.control.VyanaEvent;
	
	public class AskForVotesCommand extends VkontakteCommand {
		
		protected var balancePingTimer:Timer = new Timer(2000, 10);
		protected var balanceChanged:Boolean;
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (VkontakteGlobals.wrapper) {
				VkontakteGlobals.wrapper.addEventListener(WrapperEventType.BALANCE_CHANGED, onBalanceChanged);
				VkontakteGlobals.wrapper.addEventListener(WrapperEventType.WINDOW_FOCUS, onWindowFocus);
				VkontakteGlobals.wrapper.external.callMethod('showPaymentBox', data);
			} else
				if (localConnectionDelegate) {
					localConnectionDelegate.invokeMethod('showPaymentBox', data);
					localConnectionDelegate.addEventListener(APIConnectionEvent.BALANCE_CHANGED, onBalanceChanged);
					localConnectionDelegate.addEventListener(APIConnectionEvent.WINDOW_FOCUS, onWindowFocus);
				}
				
		}
		
		protected function onWindowFocus(e:Object = null):void {
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.removeEventListener(WrapperEventType.WINDOW_FOCUS, onWindowFocus);
			else
				if (localConnectionDelegate)
					localConnectionDelegate.removeEventListener(APIConnectionEvent.WINDOW_FOCUS, onWindowFocus);
			
			if (balancePingTimer) {
				balancePingTimer.addEventListener(TimerEvent.TIMER, onBalancePing);
				balancePingTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onBalancePingComplete);
				balancePingTimer.start();
			}
		}			
		
		protected function onBalanceChanged(e:Object = null):void {
			var balance:uint;
			
			balanceChanged = true;
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.removeEventListener(WrapperEventType.BALANCE_CHANGED, onBalanceChanged);
			else
				if (localConnectionDelegate)
					localConnectionDelegate.removeEventListener(APIConnectionEvent.BALANCE_CHANGED, onBalanceChanged);
			dismissBalancePingTimer();
			
			if (e is APIConnectionEvent) {
				balance = e.params[0];
			} else {
				balance = e.balance;
			}
			
			if (balance >= parseInt(data)) {
				Result = StringBoolean.TRUE;			
			} else {
				Result = StringBoolean.FALSE;
			}
			
		}
		
		protected function dismissBalancePingTimer():void {
			balancePingTimer.stop();
			balancePingTimer.removeEventListener(TimerEvent.TIMER, onBalancePing);
			balancePingTimer = null;
		}
		
		protected function onBalancePing(e:Event):void {
			if (balanceChanged)
				return;
			
			new VyanaEvent(APIVkontakte.GET_USER_BALANCE).dispatch(
				function (r:*):void {
					if (parseInt(r.response) >= parseInt(data)) {
						dismissBalancePingTimer();
						Result = StringBoolean.TRUE;
					}
				}
			);
		}
		
		protected function onBalancePingComplete(e:Event):void {
			balancePingTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onBalancePingComplete);
			dismissBalancePingTimer();
		}		
		
	}
}