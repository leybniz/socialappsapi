package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	
	import org.vyana.control.VyanaEvent;
	import org.vyana.control.commands.Command;

	public class ScrollWindowCommand extends Command {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!VkontakteGlobals.wrapper)
				return;
			
			VkontakteGlobals.wrapper.external.callMethod('scrollWindow', data.top, data.speed);
		}	
		
	}
}