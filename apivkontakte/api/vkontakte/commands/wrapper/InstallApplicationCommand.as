package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.*;
	import api.vkontakte.events.APIConnectionEvent;
	
	import org.vyana.control.VyanaEvent;

	public class InstallApplicationCommand extends VkontakteCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!applicationModel.viewer.isAppUser) {
				
				if (VkontakteGlobals.wrapper) {
					VkontakteGlobals.wrapper.addEventListener(WrapperEventType.APPLICATION_ADDED, onApplicationInstalled);
					VkontakteGlobals.wrapper.external.callMethod('showInstallBox');
				} else
					if (localConnectionDelegate) {
						localConnectionDelegate.addEventListener(APIConnectionEvent.APP_ADDED, onApplicationInstalled);
						localConnectionDelegate.invokeMethod('showInstallBox');
					} 
					else {
						Result = StringBoolean.FALSE;
						return;
					}					
				
			} else {
				Result = StringBoolean.TRUE;
			}			
			
		}
		
		protected function onApplicationInstalled(e:Object):void {
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.removeEventListener(WrapperEventType.APPLICATION_ADDED, onApplicationInstalled);
			else
				if (localConnectionDelegate)
					localConnectionDelegate.removeEventListener(APIConnectionEvent.APP_ADDED, onApplicationInstalled);
			
			applicationModel.viewer.isAppUser = true;
			Result = StringBoolean.TRUE;
		}
		
	}
}