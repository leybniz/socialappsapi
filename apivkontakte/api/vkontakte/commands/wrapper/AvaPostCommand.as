package api.vkontakte.commands.wrapper {

	import api.vkontakte.VkontakteGlobals;
	
	import com.adobe.serialization.json.JSON;
	
	import flash.display.BitmapData;
	import flash.display.IBitmapDrawable;
	import flash.events.Event;
	
	import mx.graphics.codec.JPEGEncoder;
	
	import org.vyana.control.VyanaEvent;

	/**
	 * Usage:
	 * 
	 * 		var request:URLRequest = new URLRequest(model.avaUploadURL);			
	 *		new VyanaEvent(APIVkontakte.AVA_POST, 
	 *			{
	 *				target : BitmapData or IBitmapDrawable,
	 *				request : request 
	 *			}				
	 *		).dispatch(
	 *			function (r:URLLoader):void {					
	 *				r.load(request);
	 *			}
	 *		);
	 * 
	 * 
	 */
	public class AvaPostCommand extends AbstractFilePostCommand {		
		
		override public function execute(e:VyanaEvent):void {
			HEADER1 = CR + D + BOUNDARY + CR + "Content-Disposition: form-data; name=\"photo\"; filename=\"ava.jpg\"\r\n" + "Content-Type: image/jpeg" + CR2;
			HEADER2 = D + BOUNDARY + CR + "Content-Disposition: form-data; name=\"Upload\"\r\n\r\n" + "Submit Query\r\n" + D + BOUNDARY + D;		
			
			// Make up fileBytes
			if (e.data.target is IBitmapDrawable) {			
				var bitmapData:BitmapData;
				bitmapData = new BitmapData(e.data.target.width, e.data.target.height);
				bitmapData.draw(e.data.target);
				fileBytes = new JPEGEncoder(100).encode(bitmapData);
			}
			
			if (e.data.target is BitmapData) {
				fileBytes = new JPEGEncoder(100).encode(e.data.target);
			}			
			
			super.execute(e);			
		}
		
		override protected function onUploadComplete(e:Event):void {
			super.onUploadComplete(e);
			
			var rObj:Object = com.adobe.serialization.json.JSON.decode(e.target.data);
			new VyanaEvent(APIVkontakte.SAVE_PROFILE_PHOTO,
				{
					server: rObj.server,
					photo: rObj.photo,
					hash: rObj.hash
				}
			).dispatch(
				function (r:*):void {
					if (VkontakteGlobals.wrapper)
						VkontakteGlobals.wrapper.external.callMethod('showProfilePhotoBox', r.response.photo_hash);
					else
						if (localConnectionDelegate)
							localConnectionDelegate.invokeMethod('showProfilePhotoBox', r.response.photo_hash);
				}
			);
		}

	}
}