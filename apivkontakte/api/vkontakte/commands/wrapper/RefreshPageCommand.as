package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	import api.vkontakte.constant.VKontakteURL;
	
	import flash.net.URLRequest;
	
	import org.vyana.control.VyanaEvent;
	
	public class RefreshPageCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.navigateToURL(new URLRequest(VKontakteURL.appPage + applicationModel.appDescriptor.id));
		}	
		
	}
}