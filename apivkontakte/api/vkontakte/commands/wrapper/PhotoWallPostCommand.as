package api.vkontakte.commands.wrapper {

	import com.adobe.serialization.json.JSON;
	
	import flash.display.BitmapData;
	import flash.display.IBitmapDrawable;
	import flash.events.Event;
	
	import mx.graphics.codec.PNGEncoder;
	
	import org.vyana.control.VyanaEvent;

	/**
	 * Usage:
	 * 
	 * 		var request:URLRequest = new URLRequest(model.wallUploadURL);			
	 *		new VyanaEvent(APIVkontakte.WALL_POST, 
	 *			{
	 *				target : BitmapData or IBitmapDrawable,
	 *				request : request 
	 *			}				
	 *		).dispatch(
	 *			function (r:*):void {					
	 *				r.load(request);
	 *			}
	 *		);
	 * 
	 * 
	 */
	public class PhotoWallPostCommand extends AbstractFilePostCommand {		
		
		override public function execute(e:VyanaEvent):void {
			HEADER1 = CR + D + BOUNDARY + CR + "Content-Disposition: form-data; name=\"photo\"; filename=\"photo.png\"\r\n" + "Content-Type: image/png" + CR2;
			HEADER2 = D + BOUNDARY + CR + "Content-Disposition: form-data; name=\"Upload\"\r\n\r\n" + "Submit Query\r\n" + D + BOUNDARY + D;		
			
			// Make up fileBytes
			if (e.data.target is IBitmapDrawable) {			
				var bitmapData:BitmapData;
				bitmapData = new BitmapData(e.data.target.width, e.data.target.height);
				bitmapData.draw(e.data.target);
				fileBytes = new PNGEncoder().encode(bitmapData);
			}
			
			if (e.data.target is BitmapData) {
				fileBytes = new PNGEncoder().encode(e.data.target);
			}			
			
			super.execute(e);			
		}
		
		override protected function onUploadComplete(e:Event):void {
			super.onUploadComplete(e);
						
			var rObj:Object = com.adobe.serialization.json.JSON.decode(e.target.data);
			
			if (!rObj.hasOwnProperty('photo'))
				return;
			
			new VyanaEvent(APIVkontakte.SAVE_WALL_POST,
				{
					wallId : data.wallId ? data.wallId : model.viewer.userId,					
					hash : rObj.hash,
					photo : rObj.photo,
					server : rObj.server,
					postId : data.postId,
					message : data.message
				}
			).dispatch();				
			
		}

	}
}