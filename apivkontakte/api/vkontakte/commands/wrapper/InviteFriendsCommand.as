package api.vkontakte.commands.wrapper {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.commands.VkontakteCommand;
	
	import org.vyana.control.VyanaEvent;

	public class InviteFriendsCommand extends VkontakteCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.external.callMethod('showInviteBox');
			else
				if (localConnectionDelegate)
					localConnectionDelegate.invokeMethod('showInviteBox');
		}	
		
	}
}