package api.vkontakte.commands.audio {
	
	import api.vkontakte.constant.APIRestriction;
	
	import org.vyana.control.VyanaEvent;

	public class SearchAudiosCommand extends FetchAudioInfoBaseCommand {
		
		override protected function set Result(value:*):void {			
			
			if (isJSON) {
				
				var items:MightyArray = new MightyArray();
				var r:* = {};				
				
				// First string property should be overall records number found
				// The rest is items descriptors
				for each (var o:* in value.response) {
					
					if (o is String || o is Number) {
						r.overall = o;
						continue;
					}					
					
					items.addItem(o);	
				}
				
				r.count = data.count;
				r.offset = data.offset;
				r.items = cacheAudioObjects(items);					
				value = r;				
			}	
			
			super.Result = value;			
		}		
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);			
			
			if (!data.hasOwnProperty('pattern') || data.pattern == null) {
				if (isJSON)
					Result = { response: [] }; 
				return;
			}	
			
			if (!data.hasOwnProperty('offset'))
				data.offset = 0;
			
			if (!data.hasOwnProperty('count'))
				data.count = APIRestriction.MAX_AUDIO_SEARCH_RESULTS;			
			
			delegate.search(data.pattern, data.count, data.offset);
		}
		
	}
}