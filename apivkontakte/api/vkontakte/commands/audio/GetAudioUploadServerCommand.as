package api.vkontakte.commands.audio {
	
	import api.vkontakte.constant.StringBoolean;
	import api.vkontakte.constant.UserSettings;
	
	import org.vyana.control.VyanaEvent;

	public class GetAudioUploadServerCommand extends AbstractAudioCommand {
		
		override protected function set Result(value:*):void {			
			
			if (value && value.response.hasOwnProperty('upload_url'))
				value = value.response.upload_url;
			
			super.Result = value;			
		}		

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			if (applicationModel.appDescriptor.testMode)
				delegate.getAudioUploadServer();
			else
				// Check for permission to modify user Audio
				new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.AUDIOS_GRANTED).dispatch(
					function (r:String):void {
						if (r != StringBoolean.TRUE) {
							Result = null;
							return;
						}	
						
						delegate.getAudioUploadServer();
					}
				);
				
		}

	}
}