package api.vkontakte.commands.audio {
	
	import org.vyana.control.VyanaEvent;

	public class SaveAudioCommand extends AbstractAudioCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.saveAudio(data.server, data.audio, data.hash, data.artist, data.title);
		}

	}
}