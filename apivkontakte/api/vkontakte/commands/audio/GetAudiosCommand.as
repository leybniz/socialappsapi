package api.vkontakte.commands.audio {

	import api.vkontakte.constant.*;
	
	import org.vyana.control.VyanaEvent;

	public class GetAudiosCommand extends FetchAudioInfoBaseCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			var params:* = data;
			
			if (!params)
				params = {};
			
			if (!params.uid)
				params.uid = applicationModel.viewer.id;
			
			if (!params.aids)
				params.aids = [];			
			
			// Check for permission to access user Audio list
			new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.AUDIOS_GRANTED).dispatch(
				function (r:String):void {
					if (r != StringBoolean.TRUE && !applicationModel.appDescriptor.testMode)
						return;
					
					delegate.getAudios(params.uid, params.aids);
				}
			);
			
		}
		
	}
}