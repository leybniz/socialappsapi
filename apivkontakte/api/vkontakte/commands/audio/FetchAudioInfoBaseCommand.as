package api.vkontakte.commands.audio {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.model.vo.audio.AudioInfo;

	public class FetchAudioInfoBaseCommand extends AbstractAudioCommand {
		
		override protected function set Result(value:*):void {			
			
			if (isJSON && value.response is Array) {
				super.Result = cacheAudioObjects(value.response);				
				return;
			}			
			
			super.Result = value;
		}
		
		protected function cacheAudioObjects(objects:Array):MightyArray {			
			var result:MightyArray = new MightyArray();
			
			for each (var o:* in objects) {
				var ai:AudioInfo = new AudioInfo(o);
				VkontakteGlobals.audioInfoCache[ai.key.toString()] = ai;
				result.addItem(ai);
			}
			
			return result;
		}
		
	}
}