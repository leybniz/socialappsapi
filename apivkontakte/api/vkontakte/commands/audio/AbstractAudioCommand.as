package api.vkontakte.commands.audio {

	import api.vkontakte.business.delegates.AudioVkontakteServiceDelegate;
	
	import mx.rpc.IResponder;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractAudioCommand extends VkontakteCommand {
		
		protected var delegate:AudioVkontakteServiceDelegate = new AudioVkontakteServiceDelegate(this as IResponder);
		
	}
}