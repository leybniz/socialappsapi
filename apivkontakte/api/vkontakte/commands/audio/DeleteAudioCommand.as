package api.vkontakte.commands.audio {
	
	import api.vkontakte.model.vo.audio.AudioInfo;
	import api.vkontakte.model.vo.audio.AudioItemKey;
	
	import org.vyana.control.VyanaEvent;

	public class DeleteAudioCommand extends AbstractAudioCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (data is AudioInfo) {
				delegate.remove(AudioInfo(data).key.audioId, AudioInfo(data).key.ownerId);
				return;
			}
			
			if (data is AudioItemKey) {
				delegate.remove(AudioItemKey(data).audioId, AudioItemKey(data).ownerId);
				return;
			}
			
			if (!data.oid)
				data.oid = applicationModel.viewer.id;			
			
			delegate.remove(data.aid, data.oid);
		}
		
	}
}