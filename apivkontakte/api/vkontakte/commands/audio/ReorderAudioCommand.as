package api.vkontakte.commands.audio {
	
	import org.vyana.control.VyanaEvent;

	public class ReorderAudioCommand extends AbstractAudioCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.reorder(data.aid, data.before, data.after);
		}
		
	}
}