package api.vkontakte.commands.audio {
	
	import org.vyana.control.VyanaEvent;

	public class AddAudioCommand extends AbstractAudioCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!data.oid)
				data.oid = applicationModel.viewer.id;			
			
			delegate.add(data.aid, data.oid);
		}
		
	}
}