package api.vkontakte.commands.audio {
	
	import org.vyana.control.VyanaEvent;

	public class GetAudiosInfoCommand extends FetchAudioInfoBaseCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (!data)
				return;
			
			// skip incorrectly formatted audio ID's
			// Id's must be formatted as 'owner_id'_'audio_id'
			var items:Array = [];
			for each (var key:String in data) {
				if (key.indexOf('_') > 0)
					items.push(key);
			}
			
			if (items.length == 0) {
				if (isJSON)
					Result = { response: [] }; 
				return;
			}		
			
			delegate.getAudiosInfo(items);
		}		
		
	}
}