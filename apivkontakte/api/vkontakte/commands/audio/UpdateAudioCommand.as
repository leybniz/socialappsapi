package api.vkontakte.commands.audio {
	
	import org.vyana.control.VyanaEvent;

	public class UpdateAudioCommand extends AbstractAudioCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.updateAudio(
				data.aid, 
				data.oid, 
				data.artist, 
				data.title,
				data.text,
				data.noSearch
			);
		}
		
	}
}