package api.vkontakte.commands.photo {

	import org.vyana.control.VyanaEvent;

	public class GetPhotosUploadServerCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getPhotosUploadServer(data.aid);
		}

	}
}