package api.vkontakte.commands.photo {

	import org.vyana.control.VyanaEvent;

	public class CreatePhotoCommentCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.createComment(data.pid, data.message, data.ownerId);
		}

	}
}