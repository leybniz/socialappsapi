package api.vkontakte.commands.photo {
	
	import org.vyana.control.VyanaEvent;

	public class SaveProfilePhotoCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.saveProfilePhoto(data.server, data.photo, data.hash);
		}

	}
}