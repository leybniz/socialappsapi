package api.vkontakte.commands.photo {
	
	import org.vyana.control.VyanaEvent;

	public class GetPhotosCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getPhotos(data.uid, data.aid, data.pids);
		}

	}
}