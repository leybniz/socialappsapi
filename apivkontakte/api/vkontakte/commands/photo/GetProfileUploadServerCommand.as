package api.vkontakte.commands.photo {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.InitialAPIResultField;
	
	import org.vyana.control.VyanaEvent;

	public class GetProfileUploadServerCommand extends AbstractPhotoCommand {
		
		override protected function set Result(value:*):void {			
			
			if (value && value.hasOwnProperty('response')) {				
				super.Result = value.response.upload_url;				
			} else			
				super.Result = value;			
		}		

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			if (VkontakteGlobals.initialResult && VkontakteGlobals.initialResult.hasOwnProperty(InitialAPIResultField.AVA_POST_URL)) {
				Result = {response:VkontakteGlobals.initialResult[InitialAPIResultField.AVA_POST_URL]};
			} else
				delegate.getProfileUploadServer();
		}

	}
}