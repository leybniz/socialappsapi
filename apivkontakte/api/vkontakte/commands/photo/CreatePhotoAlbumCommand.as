package api.vkontakte.commands.photo {

	import org.vyana.control.VyanaEvent;

	public class CreatePhotoAlbumCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.createPhotoAlbum(data.title, data.description, data.privacy);
		}

	}
}