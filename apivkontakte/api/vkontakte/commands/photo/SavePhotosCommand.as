package api.vkontakte.commands.photo {
	
	import org.vyana.control.VyanaEvent;

	public class SavePhotosCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.savePhotos(data.aid, data.server, data.photos_list, data.hash);
		}

	}
}