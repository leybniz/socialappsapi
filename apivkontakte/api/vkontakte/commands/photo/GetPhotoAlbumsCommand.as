package api.vkontakte.commands.photo {
	
	import org.vyana.control.VyanaEvent;

	public class GetPhotoAlbumsCommand extends AbstractPhotoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getPhotoAlbums(data.uid, data.aids);
		}

	}
}