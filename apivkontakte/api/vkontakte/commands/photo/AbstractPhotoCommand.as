package api.vkontakte.commands.photo {
	
	import api.vkontakte.business.delegates.PhotoVkontakteServiceDelegate;

	import mx.rpc.IResponder;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractPhotoCommand extends VkontakteCommand {

		protected var delegate:PhotoVkontakteServiceDelegate = new PhotoVkontakteServiceDelegate(this as IResponder);

	}
}