package api.vkontakte.commands.friends {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.*;
	import api.vkontakte.model.vo.UserInfo;
	
	import org.vyana.control.VyanaEvent;

	public class GetFriendsCommand extends AbstractFriendsCommand {
		
		override protected function set Result(value:*):void {
			
			if (isJSON) {
				
				if (data.hasOwnProperty('fields') && value.response is Array) {
					var items:MightyArray = new MightyArray();
					
					for each (var o:* in value.response)
						items.addItem(new UserInfo(o));
					
					super.Result = items;
					return;
				}
				
			}
			
			super.Result = value;
		}

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			if (!VkontakteGlobals.wrapper) {
				delegate.getFriends(data.fields, data.nameCase);
				return;
			}

			// Check for permission to access users list				
			new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.FRIENDS_GRANTED).dispatch(
				function (r:String):void {
					if (r != StringBoolean.TRUE) {
						Result = {response: []};
						return;
					}	
					
					delegate.getFriends(data.fields, data.nameCase);
				}
			);			
			
		}

	}
}