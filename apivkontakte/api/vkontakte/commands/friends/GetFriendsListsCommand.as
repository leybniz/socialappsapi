package api.vkontakte.commands.friends {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.*;
	
	import flash.utils.Dictionary;
	
	import org.vyana.control.VyanaEvent;

	public class GetFriendsListsCommand extends AbstractFriendsCommand {
		
		override protected function set Result(value:*):void {
			
			if (isJSON) {				
				
				if (value.response) {
					var tags:Dictionary = new Dictionary();
					
					for (var tag:String in value.response)
						tags[tag] = value.response[tag];
					
					super.Result = tags;
				}
				
				return;
			}
			
			super.Result = value;
		}
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			if (!VkontakteGlobals.wrapper) {
				delegate.getLists();
				return;
			}

			// Check for permission to access users lists				
			new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.FRIENDS_GRANTED).dispatch(
				function (r:String):void {
					if (r != StringBoolean.TRUE) {
						Result = {response: []};
						return;
					}	
					
					delegate.getLists();
				}
			);			
			
		}

	}
}