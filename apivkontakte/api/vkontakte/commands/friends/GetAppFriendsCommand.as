package api.vkontakte.commands.friends {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.InitialAPIResultField;
	import api.vkontakte.constant.StringBoolean;
	import api.vkontakte.constant.UserSettings;
	
	import org.vyana.control.VyanaEvent;

	public class GetAppFriendsCommand extends AbstractFriendsCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			if (VkontakteGlobals.initialResult && VkontakteGlobals.initialResult.hasOwnProperty(InitialAPIResultField.APP_FRIENDS)) {
				Result = 
					{ 
						response: VkontakteGlobals.initialResult[InitialAPIResultField.APP_FRIENDS] 
					};
				return;
			}
			
			// Check for permission to access users list
			if (VkontakteGlobals.wrapper)
				new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.FRIENDS_GRANTED).dispatch(
					function (r:String):void {
						if (r != StringBoolean.TRUE) {
							Result = {response: []};
							return;
						}	
						
						delegate.getAppFriends();
					}
				)
			else
				delegate.getAppFriends();				
		}

	}
}