package api.vkontakte.commands.friends {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.StringBoolean;
	import api.vkontakte.constant.UserSettings;
	
	import org.vyana.control.VyanaEvent;

	public class GetMutualFriendsCommand extends AbstractFriendsCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			if (!VkontakteGlobals.wrapper) {
				delegate.getMutual(data.target, data.source);
				return;
			}

			// Check for permission to access				
			new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.FRIENDS_GRANTED).dispatch(
				function (r:String):void {
					if (r != StringBoolean.TRUE) {
						Result = {response: []};
						return;
					}	
					
					delegate.getMutual(data.target, data.source);
				}
			);			
			
		}

	}
}