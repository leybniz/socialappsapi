package api.vkontakte.commands.friends {

	import api.vkontakte.business.delegates.FriendsVkontakteServiceDelegate;
	import api.vkontakte.commands.VkontakteCommand;
	
	import mx.rpc.IResponder;

	public class AbstractFriendsCommand extends VkontakteCommand {
		
		protected var delegate:FriendsVkontakteServiceDelegate = new FriendsVkontakteServiceDelegate(this as IResponder);

	}
}