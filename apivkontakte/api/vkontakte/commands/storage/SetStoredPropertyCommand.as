package api.vkontakte.commands.storage {
	
	import org.vyana.control.VyanaEvent;

	public class SetStoredPropertyCommand extends AbstractStorageCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.setValue(data.key, data.value, data.global);
		}

	}
}