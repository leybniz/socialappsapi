package api.vkontakte.commands.storage {
	
	import org.vyana.control.VyanaEvent;

	public class GetStoredPropertyCommand extends AbstractStorageCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getValue(data.key, data.global, data.keys);
		}

	}
}