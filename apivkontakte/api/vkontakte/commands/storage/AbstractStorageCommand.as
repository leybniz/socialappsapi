package api.vkontakte.commands.storage {

	import api.vkontakte.business.delegates.StorageServiceDelegate;
	import api.vkontakte.commands.VkontakteCommand;
	
	import mx.rpc.IResponder;

	public class AbstractStorageCommand extends VkontakteCommand {
	
		protected var delegate:StorageServiceDelegate = new StorageServiceDelegate(this as IResponder);
	
	}
}