package api.vkontakte.commands.wall {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.methods.WallAPIName;
	
	import org.vyana.control.VyanaEvent;

	public class WallPostCommand extends AbstractWallCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);	
			
			if (VkontakteGlobals.wrapper) {
				VkontakteGlobals.wrapper.external.api(WallAPIName.WALL_POST, data);
			} else			
				if (localConnectionDelegate)
					localConnectionDelegate.api(WallAPIName.WALL_POST, data);
			
			
			// TODO: Implement case for the Desktop application
			
			// Check for permission to access user Wall
//			if (VkontakteGlobals.wrapper)
//				new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.WALL_GRANTED).dispatch(
//					function (r:String):void {
//						if (r != StringBoolean.TRUE && !applicationModel.appDescriptor.testMode)
//							return;
//						
//						delegate.wallPost(
//							data.owner_id,
//							data.message, 
//							data.attachment
//						);
//					}
//				);
		}
		
	}
}