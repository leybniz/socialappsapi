package api.vkontakte.commands.wall {
	
	import api.vkontakte.business.delegates.WallVkontakteServiceDelegate;
	
	import mx.rpc.IResponder;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractWallCommand extends VkontakteCommand {
		
		protected var delegate:WallVkontakteServiceDelegate = new WallVkontakteServiceDelegate(this as IResponder);
		
	}
	
}