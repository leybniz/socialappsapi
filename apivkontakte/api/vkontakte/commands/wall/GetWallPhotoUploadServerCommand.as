package api.vkontakte.commands.wall {
	
	import org.vyana.control.VyanaEvent;

	public class GetWallPhotoUploadServerCommand extends AbstractWallCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getUploadServer();
		}		
		
	}
}