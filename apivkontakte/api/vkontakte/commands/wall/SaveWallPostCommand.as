package api.vkontakte.commands.wall {
	
	import api.vkontakte.VkontakteGlobals;
	
	import org.vyana.control.VyanaEvent;

	public class SaveWallPostCommand extends AbstractWallCommand {
		
		override protected function set Result(value:*):void {			
			
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.external.callMethod('saveWallPost', value.response.post_hash);
			
			super.Result = value;
		} 
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);			
			
			delegate.savePost(
				data.wallId,				
				data.server, 
				data.photo, 
				data.hash,
				data.postId,
				data.message
			);
		}
		
	}
}