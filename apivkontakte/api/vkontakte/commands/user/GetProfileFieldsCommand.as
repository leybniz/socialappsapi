package api.vkontakte.commands.user {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.UserProfileField;
	import api.vkontakte.model.vo.UserInfo;
	
	import mx.rpc.events.ResultEvent;
	
	import org.vyana.control.VyanaEvent;

	public class GetProfileFieldsCommand extends AbstractUserCommand {
		
		protected var potentialResult:MightyArray = new MightyArray();

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			var res:* = {};
			var uids:MightyArray = new MightyArray(data.uids);
			
			if (uids.length == 0) {
				res.response = [];
				Result = res;
				return;
			}
			
			// Use cache first to satisfy request, do we need a query?
			for each (var uid:String in uids) {
				if (isUserCached(uid)) {
					potentialResult.addItem(VkontakteGlobals.userInfoCache[uid]);
					uids.removeItem(uid);
				}
			}
			
			// Full satisfaction
			if (uids.length == 0) {
				res.response = potentialResult;
				Result = res;
				return;
			}

			delegate.getProfiles(uids, data.fields, data.nameCase);
		}
		
		override protected function onResult(e:ResultEvent):void {			
			if (isJSON && e.result.response is Array) {
				e.result.response = 
					new MightyArray(
						cacheUserInfoObjects(e.result.response)
					).append(potentialResult);
			}
			
			super.onResult(e);
		}
		
		protected function cacheUserInfoObjects(objects:Array):MightyArray {			
			var result:MightyArray = new MightyArray();
			
			for each (var o:* in objects) {
				var ui:UserInfo = new UserInfo(o);
				
				if (VkontakteGlobals.userInfoCache[ui.id])
					mergeUserInfoObjects(VkontakteGlobals.userInfoCache[ui.id], ui)
				else	
					VkontakteGlobals.userInfoCache[ui.id] = ui;
				
				result.addItem(ui);
			}
			
			return result;
		}
		
		protected function mergeUserInfoObjects(oldInfo:UserInfo, newInfo:UserInfo):void {
			var exceptionFields:MightyArray = new MightyArray(
				[
					UserProfileField.FIRST_NAME, 
					UserProfileField.LAST_NAME
				]
			);
			
			// Merge properties
			for (var p:String in newInfo)
				if (!exceptionFields.contains(p))
					oldInfo[p] = newInfo[p];
			
			// Detect new nameCase
			if (data.nameCase && data.nameCase != oldInfo.nameCase)
				oldInfo.registerNameCase(newInfo, data.nameCase);
		}
		
		protected function isUserCached(uid:String):Boolean {
			var result:Boolean;
			var ui:UserInfo = VkontakteGlobals.userInfoCache[uid];
			
			if (ui)
				result = true;
			
			if (ui && data.nameCase && ui.nameCase != data.nameCase)
				result = false;
			
			return result;
		}

	}
}