package api.vkontakte.commands.user {

	import org.vyana.control.VyanaEvent;

	public class GetUserGroupsCommand extends AbstractUserCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getGroups();
		}
		
	}
}