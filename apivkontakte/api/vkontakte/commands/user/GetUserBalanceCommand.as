package api.vkontakte.commands.user {

	import org.vyana.control.VyanaEvent;

	public class GetUserBalanceCommand extends AbstractUserCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getUserBalance();
		}

	}
}