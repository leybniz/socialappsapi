package api.vkontakte.commands.user {
	
	import org.vyana.control.VyanaEvent;

	public class IsAppUserCommand extends AbstractUserCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.isAppUser(data);
		}

	}
}