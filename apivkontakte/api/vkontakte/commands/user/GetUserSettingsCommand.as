package api.vkontakte.commands.user {
	
	import api.vkontakte.constant.UserSettings;
	
	import org.vyana.control.VyanaEvent;

	public class GetUserSettingsCommand extends AbstractUserCommand {
		
		override protected function set Result(value:*):void {			
			var result:UserSettings;
			
			if (isJSON)
				result = new UserSettings(parseInt(value.response));			
			
			super.Result = result;
		}		
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getUserSettings();
		}		
		
	}
}