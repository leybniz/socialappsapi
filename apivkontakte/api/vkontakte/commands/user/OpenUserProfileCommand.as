package api.vkontakte.commands.user {
	
	import api.vkontakte.constant.VKontakteURL;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import org.vyana.control.VyanaEvent;
	import org.vyana.control.commands.Command;

	public class OpenUserProfileCommand extends Command {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			navigateToURL(new URLRequest(VKontakteURL.profilePage + data), '_blank');
		}		
		
	}
}