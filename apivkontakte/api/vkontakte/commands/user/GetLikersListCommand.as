package api.vkontakte.commands.user {
	
	import org.vyana.control.VyanaEvent;

	public class GetLikersListCommand extends AbstractUserCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getLikersList(
				data.type, 
				data.offset, 
				data.count, 
				data.friendsOnly, 
				data.ownerId, 
				data.itemId, 
				data.pageUrl
			);
		}

	}
}