package api.vkontakte.commands.user {

	import api.vkontakte.business.delegates.UserVkontakteServiceDelegate;
	
	import mx.rpc.IResponder;
	
	import org.vyana.control.VyanaEvent;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractUserCommand extends VkontakteCommand {
		
		protected var delegate:UserVkontakteServiceDelegate = new UserVkontakteServiceDelegate(this as IResponder);

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			if (!data.uid)
				data.uid = applicationModel.viewer.id;

		}		

	}
}