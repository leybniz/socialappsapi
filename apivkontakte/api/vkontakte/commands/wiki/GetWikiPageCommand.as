package api.vkontakte.commands.wiki {
	
	import org.vyana.control.VyanaEvent;

	public class GetWikiPageCommand extends AbstractWikiCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getPage(data.pid, data.gid, data.asHTML);
		}
		
	}
}