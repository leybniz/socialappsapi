package api.vkontakte.commands.wiki {
	
	import org.vyana.control.VyanaEvent;

	public class SaveWikiPageAccessCommand extends AbstractWikiCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.savePageAccess(data.pid, data.gid, data.view, data.edit);
		}
		
	}
}