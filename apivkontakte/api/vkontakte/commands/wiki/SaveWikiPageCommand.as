package api.vkontakte.commands.wiki {
	
	import org.vyana.control.VyanaEvent;

	public class SaveWikiPageCommand extends AbstractWikiCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.savePage(data.pid, data.gid, data.text);
		}
		
	}
}