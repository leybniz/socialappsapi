package api.vkontakte.commands.wiki {
	
	import org.vyana.control.VyanaEvent;

	public class GetWikiPageTitlesCommand extends AbstractWikiCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getTitles(data);
		}
		
	}
}