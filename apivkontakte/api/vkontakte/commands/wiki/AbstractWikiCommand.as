package api.vkontakte.commands.wiki {

	import api.vkontakte.business.delegates.WikiVkontakteServiceDelegate;
	
	import mx.rpc.IResponder;	
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractWikiCommand extends VkontakteCommand {
		
		protected var delegate:WikiVkontakteServiceDelegate = new WikiVkontakteServiceDelegate(this as IResponder);
		
	}
}