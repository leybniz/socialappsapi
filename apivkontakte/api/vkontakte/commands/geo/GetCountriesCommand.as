package api.vkontakte.commands.geo {

	import org.vyana.control.VyanaEvent;

	public class GetCountriesCommand extends AbstractGeoCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.getCountries(data.cids);
		}

	}
}