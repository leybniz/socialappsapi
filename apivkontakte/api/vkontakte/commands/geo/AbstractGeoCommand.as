package api.vkontakte.commands.geo {

	import api.vkontakte.business.delegates.GeoVkontakteServiceDelegate;
	
	import mx.rpc.IResponder;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractGeoCommand extends VkontakteCommand {
		
		protected var delegate:GeoVkontakteServiceDelegate = new GeoVkontakteServiceDelegate(this as IResponder);

	}
}