package api.vkontakte.commands.questions {
	
	import org.vyana.control.VyanaEvent;

	public class AddAnswerCommand extends AbstractQuestionsCommand {

		override public function execute(e:VyanaEvent):void {
			super.execute(e);

			delegate.addAnswer(data.uid, data.qid, data.text);
		}

	}
}