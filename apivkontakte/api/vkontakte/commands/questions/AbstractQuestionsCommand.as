package api.vkontakte.commands.questions {

	import api.vkontakte.business.delegates.QuestionsVkontakteServiceDelegate;
	
	import mx.rpc.IResponder;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractQuestionsCommand extends VkontakteCommand {
	
		protected var delegate:QuestionsVkontakteServiceDelegate = new QuestionsVkontakteServiceDelegate(this as IResponder);
	
	}
}