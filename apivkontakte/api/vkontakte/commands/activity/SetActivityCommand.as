package api.vkontakte.commands.activity {
	
	import api.vkontakte.constant.StringBoolean;
	import api.vkontakte.constant.UserSettings;
	
	import org.vyana.control.VyanaEvent;

	public class SetActivityCommand extends AbstractActivityCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			// Check for permission to modify user Activity
			new VyanaEvent(APIVkontakte.ASK_PERMISSION, UserSettings.ACTIVITY_GRANTED).dispatch(
				function (r:String):void {
					if (r != StringBoolean.TRUE)
						return;
					
				}
			);			
			
		}
		
	}
}