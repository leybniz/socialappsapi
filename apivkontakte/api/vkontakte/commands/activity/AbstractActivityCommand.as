package api.vkontakte.commands.activity {

	import api.vkontakte.business.delegates.ActivityVkontakteServiceDelegate;
	
	import mx.rpc.IResponder;
	import api.vkontakte.commands.VkontakteCommand;

	public class AbstractActivityCommand extends VkontakteCommand {
		
		protected var delegate:ActivityVkontakteServiceDelegate = new ActivityVkontakteServiceDelegate(this as IResponder);
		
	}
}