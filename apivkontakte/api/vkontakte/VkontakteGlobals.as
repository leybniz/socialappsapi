package api.vkontakte {

	import flash.utils.Dictionary;

	[Bindable]
	public class VkontakteGlobals {
		
		/**
		 * First API call result object reference
		 * For more information see flashvars.api_result 
		 */
		public static var initialResult:Object;
		
		/**
		 * Reference to APIWrapper class provided by vkontakte.ru for seamless integration to the network 
		 */
		public static var wrapper:* = null;
		
		/**
		 * Cached AudioInfo's instances from all audio info fetching apis 
		 */
		public static var audioInfoCache:Dictionary = new Dictionary();
		
		/**
		 * Cached UserInfo's instances from all audio info fetching apis 
		 */
		public static var userInfoCache:Dictionary = new Dictionary();
		
	}
}