package api.vkontakte.business.delegates {

	import api.vkontakte.constant.methods.FriendsAPIName;
	
	import mx.rpc.IResponder;

	public class FriendsVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function FriendsVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}

		public function getAppFriends():void {
			invokeMethod(FriendsAPIName.GET_APP_USERS);
		}
		
		public function getFriends(fields:Array = null, nameCase:String = null):void {
			var params:* = {};
			
			if (fields)
				params.fields = fields.join(',');
			
			if (nameCase)
				params.name_case = nameCase;			
			
			invokeMethod(FriendsAPIName.GET, params);
		}
		
		public function getLists():void {
			invokeMethod(FriendsAPIName.GET_LISTS);
		}		
		
		public function getOnline(uid:String = null):void {
			var params:* = {};
			
			if (uid)
				params.uid = uid;

			invokeMethod(FriendsAPIName.GET_ONLINE, params);
		}		

		public function getMutual(target_uid:String, source_uid:String = null):void {
			var params:* = {};
			
			if (target_uid)
				params.target_uid = target_uid;
			
			if (source_uid)
				params.source_uid = source_uid;

			invokeMethod(FriendsAPIName.GET_MUTUAL, params);
		}
		
	}
}