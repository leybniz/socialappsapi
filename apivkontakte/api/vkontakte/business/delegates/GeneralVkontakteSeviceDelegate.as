package api.vkontakte.business.delegates {
	
	import api.vkontakte.constant.APIRestriction;
	import api.vkontakte.constant.AdvertisementType;
	import api.vkontakte.constant.methods.GeneralAPIName;
	
	import mx.rpc.IResponder;

	public class GeneralVkontakteSeviceDelegate extends AbstractVkontakteServiceDelegate {
		
		function GeneralVkontakteSeviceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getLocalizationValues(language:String = null, keys:Array = null, all:Boolean = true):void {
			var params:Object =	{};
			
			if (language)
				params.language = language;

			if (keys)
				params.keys = keys.join(',');

			if (all)
				params.all = '1';

			invokeMethod(GeneralAPIName.GET_LOCALIZATION_VALUES, params);
		}

		public function getServerTime():void {
			invokeMethod(GeneralAPIName.GET_SERVER_TIME);
		}
		
		public function getVariable(key:int, uid:String = '', session:String = ''):void {
			var params:Object = 
				{
					key : key,
					user_id : uid					
				}; 
			
			if (session && session.length > 0)
				params.session = session;
			
			invokeMethod(GeneralAPIName.GET_VARIABLE, params);
		}		
		
		public function getVariables(key:int, count:int = 1, uid:String = ''):void {
			var params:Object = 
				{
					key : key,
					count : count,
					user_id : uid					
				}; 
			
			invokeMethod(GeneralAPIName.GET_VARIABLES, params);
		}		
		
		public function putVariable(key:int, value:String, uid:String = ''):void {
			var params:Object = 
				{
					key : key,
					value : value,
					user_id : uid					
				}; 
			
			invokeMethod(GeneralAPIName.PUT_VARIABLE, params);
		}		
		
		public function getAdvertisements(count:uint = 1, type:String = null, directApps:Array = null, minPrice:uint = 0):void {
			var params:Object =	{}; 
			
			if (count > 1) {
				params.count = count;
				if (count > APIRestriction.MAX_ADVERTISEMENTS_PER_CALL)
					params.count = APIRestriction.MAX_ADVERTISEMENTS_PER_CALL;
			}	
			
			if (type)
				params.type = type;
			
			if (type && type != AdvertisementType.TARGETTED && directApps)
				params.apps_ids = directApps.join(',');
			
			if (type && type == AdvertisementType.DIRECT && minPrice > 0)
				params.min_price = minPrice;			
			
			invokeMethod(GeneralAPIName.GET_ADS, params);
		}
		
		public function setMenuCounter(counter:int):void {
			invokeMethod(GeneralAPIName.SET_MENU_NAVIGATION_COUNTER,
				{
					counter : counter
				} 
			);
		}
		
		public function setMenuLabel(label:String):void {
			invokeMethod(GeneralAPIName.SET_MENU_NAVIGATION_LABEL,
				{
					name : label.slice(0, APIRestriction.MAX_MENU_LABEL_LENGTH)
				} 
			);
		}
		
		public function executeVKScript(code:String):void {
			invokeMethod(GeneralAPIName.EXECUTE,
				{
					code : code
				} 
			);
		}		
		
	}
}