package api.vkontakte.business.delegates {

	import api.vkontakte.constant.APIRestriction;
	import api.vkontakte.constant.StringBoolean;
	import api.vkontakte.constant.methods.AudioAPIName;
	
	import mx.rpc.IResponder;

	public class AudioVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function AudioVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function search(pattern:String, count:int, offset:int = 0, sort:int = 0, lyrics:Boolean = false):void {
			invokeMethod(AudioAPIName.SEARCH,
				{
					q  : pattern,
					sort : sort,
					lyrics : StringBoolean.toStringBoolean(lyrics),
					count : Math.min(count, APIRestriction.MAX_AUDIO_SEARCH_RESULTS),
					offset : offset
				} 
			);
		}
		
		public function getAudioUploadServer():void {
			invokeMethod(AudioAPIName.GET_AUDIO_UPLOAD_SERVER);
		}
		
		public function getAudios(uid:String, aids:Array):void {
			invokeMethod(AudioAPIName.GET_AUDIOS,
				{
					uid  : uid,
					aids : aids.join(',')
				} 
			);
		}
		
		public function getAudiosInfo(audios:Array):void {
			invokeMethod(AudioAPIName.GET_AUDIOS_INFO,
				{
					audios : audios.join(',')
				} 
			);
		}
		
		public function updateAudio(aid:String, oid:String, artist:String, title:String, text:String = '', noSearch:Boolean = false):void {
			invokeMethod(AudioAPIName.EDIT,
				{
					aid : aid,
					oid : oid,
					artist : artist,
					title : title,
					text : text,
					no_search : StringBoolean.toStringBoolean(noSearch) 
				} 
			);			
		}
		
		public function saveAudio(server:String, audio:String, hash:String, artist:String = null, title:String = null):void {
			var params:* = 
				{
					server : server,
					audio : audio,
					hash : hash					
				};
			
			if (artist)
				params.artist = artist;
			
			if (title)
				params.title = title;			
			
			invokeMethod(AudioAPIName.SAVE_AUDIO, params);
		}
		
		public function add(aid:String, oid:String):void {
			invokeMethod(AudioAPIName.ADD,
				{
					aid : aid,
					oid : oid
				} 
			);			
		}
		
		public function remove(aid:String, oid:String):void {
			invokeMethod(AudioAPIName.DELETE,
				{
					aid : aid,
					oid : oid
				} 
			);			
		}
		
		public function reorder(aid:String, before:String, after:String, oid:String = null):void {
			var params:* = 
				{
					aid : aid,
					before : before,
					after : after
				};
			
			if (oid)
				params.oid = oid;			
			
			invokeMethod(AudioAPIName.REORDER, params);			
		}		
		
	}
}