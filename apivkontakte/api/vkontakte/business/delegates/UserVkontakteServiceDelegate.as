package api.vkontakte.business.delegates {

	import api.vkontakte.constant.StringBoolean;
	import api.vkontakte.constant.methods.UserAPIName;
	
	import mx.rpc.IResponder;

	public class UserVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function UserVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getLikersList(type:String, 
									  offset:Number = NaN,
									  count:Number = NaN,
									  friendsOnly:Boolean = false,
									  ownerId:String = null, 
									  itemId:String = null,
									  pageUrl:String = null):void {
			var params:Object = 
				{
					type : type
				};			
			
			if (friendsOnly)
				params.friends_only = StringBoolean.toStringBoolean(friendsOnly);
			
			if (pageUrl)
				params.page_url = pageUrl;
			
			if (ownerId)
				params.owner_id = ownerId;
			
			if (itemId)
				params.item_id = itemId;
			
			if (!isNaN(offset))
				params.offset = offset;
			
			if (!isNaN(count))
				params.count = count;			
			
			invokeMethod(UserAPIName.GET_LIKES, params);
		}
		
		public function getCounters():void {
			invokeMethod(UserAPIName.GET_COUNTERS);
		}		
		
		public function isAppUser(uid:String):void {
			invokeMethod(UserAPIName.IS_APP_USER, 
				{ uid : uid }			
			);
		}
		
		public function getProfiles(uids:Array, fields:Array, nameCase:String = null):void {
			var params:Object = 
				{
					uids : uids.join(','),
						fields : fields ? fields.join(',') : '' 
				}; 
			
			if (nameCase)
				params.name_case = nameCase;			
			
			invokeMethod(UserAPIName.GET_PROFILES, params);
		}		
		
		public function getUserBalance():void {
			invokeMethod(UserAPIName.GET_USER_BALANCE);
		}
		
		public function getUserSettings():void {
			invokeMethod(UserAPIName.GET_USER_SETTINGS);
		}
		
		public function getGroups():void {
			invokeMethod(UserAPIName.GET_GROUPS);
		}
		
		public function getGroupsFull():void {
			invokeMethod(UserAPIName.GET_GROUPS_FULL);
		}
		
	}
}