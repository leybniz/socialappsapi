package api.vkontakte.business.delegates {

	import api.vkontakte.constant.*;
	import api.vkontakte.model.VkontakteApplicationModel;
	import api.vkontakte.model.vo.VkontakteApplicationDescriptor;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	import org.vyana.model.business.HTTPServiceDelegate;
	import org.vyana.model.business.VyanaHTTPService;

	public class AbstractVkontakteServiceDelegate extends HTTPServiceDelegate {
		
		public static var requestsQueue:QueueManager;		
		
		function AbstractVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder, ServiceName.VKONTAKTE);

			if (applicationModel.appDescriptor.responseFormat == ResponseFormat.XML)
				vkService.resultFormat = 'e4x';
			
			if (applicationModel.appDescriptor.responseFormat == ResponseFormat.JSON)
				vkService.resultFormat = 'text';

			if (!requestsQueue) {	
				requestsQueue = new QueueManager(1000 / APIRestriction.MAX_REQUESTS_PER_SECOND);
				requestsQueue.processingFunction = 
					function (request:*):void {
						var call:AsyncToken;
						call = vkService.send(request.params);
						call.addResponder(request.responder);
						requestsQueue.stopProcessing();
					};
				requestsQueue.startProcessing();
			}	
		}

		protected function get applicationModel():VkontakteApplicationModel {
			return context.model as VkontakteApplicationModel;
		}

		protected function get vkService():VyanaHTTPService {
			return service as VyanaHTTPService;
		}

		protected function getParamsSignature(params:Object):String {
			var keys:Array = [];
			for (var k:String in params)
				keys.push(k);
			keys.sort();

			var sig:String = applicationModel.viewer.id;
			for (var i:int = 0; i < keys.length; i++)
				sig += keys[i] + "=" + params[keys[i]];
			sig += applicationModel.appDescriptor.secret;

			return MD5.encrypt(sig);
		}

		protected function invokeMethod(methodName:String, params:* = null):void {
			if (!params)
				params = {};

			if (applicationModel.appDescriptor.testMode)
				params.test_mode = 1;

			if (applicationModel.appDescriptor.responseFormat != ResponseFormat.XML)
				params.format = applicationModel.appDescriptor.responseFormat;

			params.v = APIVersion.VERSION;
			params.api_id = applicationModel.appDescriptor.id;
			params.method = methodName;

			params.sig = getParamsSignature(params);
			
			if (APIVersion.VERSION == APIVersion.V30)
				params.sid = applicationModel.appDescriptor.testMode ? VkontakteApplicationDescriptor.TEST_SESSION_ID : applicationModel.viewer.sessionId;

			requestsQueue.queueObject(
				{
					params : params,
					responder : responder
				}
			);
			
			if (requestsQueue.length > 0)
				requestsQueue.startProcessing();						
		}

	}
}