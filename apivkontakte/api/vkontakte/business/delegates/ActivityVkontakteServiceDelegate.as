package api.vkontakte.business.delegates {

	import api.vkontakte.constant.methods.ActivityAPIName;
	
	import mx.rpc.IResponder;

	public class ActivityVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function ActivityVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
	}
}