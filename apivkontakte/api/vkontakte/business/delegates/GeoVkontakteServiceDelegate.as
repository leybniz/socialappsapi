package api.vkontakte.business.delegates {

	import api.vkontakte.constant.methods.GeoAPIName;
	
	import mx.rpc.IResponder;

	public class GeoVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function GeoVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getCities(cids:Array):void {
			invokeMethod(GeoAPIName.GET_CITIES,
				{
					cids : cids.join(',')
				} 
			);
		}		
		
		public function getCountries(cids:Array):void {
			invokeMethod(GeoAPIName.GET_COUNTRIES,
				{
					cids : cids.join(',')
				} 
			);
		}		
		
	}
}