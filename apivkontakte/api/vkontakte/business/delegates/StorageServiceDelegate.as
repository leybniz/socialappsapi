package api.vkontakte.business.delegates {

	import api.vkontakte.constant.APIRestriction;
	import api.vkontakte.constant.StringBoolean;
	import api.vkontakte.constant.methods.StorageAPIName;
	
	import mx.rpc.IResponder;

	public class StorageServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function StorageServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getValue(key:String, global:Boolean = false, keys:Array = null):void {
			var params:* = 
				{
					key : key,
					global : StringBoolean.toStringBoolean(global)
				};

			if (keys)
				params.keys = keys.join(',');
			
			invokeMethod(StorageAPIName.GET_VARIABLE, params);
		}		
		
		public function setValue(key:String, value:String, global:Boolean = false):void {
			value  = value.slice(0, APIRestriction.MAX_STORAGE_VALUE_LENGTH);
			// TODO: add check for key constraint  [a-zA-Z_\-0-9]
			key  = key.slice(0, APIRestriction.MAX_STORAGE_KEY_LENGTH);
			
			invokeMethod(StorageAPIName.SET_VARIABLE,
				{
					key    : key,
					value  : value,
					global : StringBoolean.toStringBoolean(global)
				} 
			);
		}		
		
	}
}