package api.vkontakte.business.delegates {
	
	import api.vkontakte.constant.methods.WikiAPIName;
	
	import mx.rpc.IResponder;

	public class WikiVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function WikiVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getTitles(gid:String):void {
			invokeMethod(WikiAPIName.GET_TITLES,
				{
					gid: gid

				}
			);
		}

		public function getPage(pid:String, gid:String, asHTML:Boolean = false):void {
			invokeMethod(WikiAPIName.GET,
				{
					pid: pid,
					gid: gid,
					need_html : asHTML ? '1' : ''
				}
			);
		}
		
		public function savePage(pid:String, gid:String, text:String):void {
			invokeMethod(WikiAPIName.SAVE,
				{
					pid: pid,
					gid: gid,
					Text: text
				}
			);
		}
		
		public function savePageAccess(pid:String, gid:String, view:int, edit:int):void {
			invokeMethod(WikiAPIName.SAVE_ACCESS,
				{
					pid: pid,
					gid: gid,
					view: view,
					edit: edit
				}
			);
		}
		
	}
}