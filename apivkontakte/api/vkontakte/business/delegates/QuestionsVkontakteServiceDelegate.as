package api.vkontakte.business.delegates {

	import api.vkontakte.constant.methods.QuestionsName;
	
	import mx.rpc.IResponder;

	public class QuestionsVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function QuestionsVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function addAnswer(uid:String, qid:String, text:String):void {
			invokeMethod(QuestionsName.ADD_ANSWER, 
				{ 
					uid : uid,
					qid : qid,
					text : text
				}			
			);
		}		 
		
	}
}