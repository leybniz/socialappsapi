package api.vkontakte.business.delegates {

	import api.vkontakte.constant.PrivacyLevel;
	import api.vkontakte.constant.methods.PhotoAPIName;
	
	import mx.rpc.IResponder;

	public class PhotoVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function PhotoVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getPhotoAlbums(uid:String, aids:Array):void {
			invokeMethod(PhotoAPIName.GET_PHOTO_ALBUMS,
				{
					uid  : uid,
					aids : aids.join(',')
				} 
			);
		}		
		
		public function getPhotos(uid:String, aid:String, pids:Array):void {
			invokeMethod(PhotoAPIName.GET_PHOTOS,
				{
					uid  : uid,
					aid  : aid,
					pids : pids.join(',')
				} 
			);
		}		
		
		public function createComment(pid:String, message:String, ownerId:String = null):void {
			var params:Object = 
				{
					pid  : pid,
					message  : message
				}; 
			
			if (ownerId)
				params.owner_id = ownerId;
			
			invokeMethod(PhotoAPIName.CREATE_COMMENT, params);
		}		

		public function createPhotoAlbum(title:String, description:String, privacy:int = PrivacyLevel.ALL):void {
			invokeMethod(PhotoAPIName.CREATE_PHOTO_ALBUM,
				{
					title  : title,
					privacy  : privacy,
					description : description
				} 
			);
		}		
		
		public function getPhotosUploadServer(aid:String):void {
			invokeMethod(PhotoAPIName.GET_PHOTOS_UPLOAD_SERVER,
				{
					aid  : aid					
				} 
			);
		}
		
		public function getProfileUploadServer():void {
			invokeMethod(PhotoAPIName.GET_PROFILE_UPLOAD_SERVER);
		}
		
		public function saveProfilePhoto(server:String, photo:String, hash:String):void {
			invokeMethod(PhotoAPIName.SAVE_PROFILE_PHOTO,
				{
					server : server,
					photo : photo,
					hash : hash
				} 
			);
		}		
		
		public function savePhotos(aid:String, server:String, photos_list:String, hash:String):void {
			invokeMethod(PhotoAPIName.SAVE_PHOTOS,
				{
					aid  : aid,
					server : server,
					photos_list : photos_list,
					hash : hash
				} 
			);
		}		
		
	}
}