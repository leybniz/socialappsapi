package api.vkontakte.business.delegates {
	
	import api.vkontakte.constant.methods.WallAPIName;
	
	import mx.rpc.IResponder;
	
	public class WallVkontakteServiceDelegate extends AbstractVkontakteServiceDelegate {
		
		function WallVkontakteServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getUploadServer():void {
			invokeMethod(WallAPIName.GET_PHOTO_UPLOAD_SERVER);
		}
		
		public function wallPost(ownerId:String = null, message:String = null, attachment:String = null):void {
			var params : Object = {};
			
			if (ownerId)
				params.owner_id = ownerId;
			
			if (message)
				params.message = message;
			
			if (attachment)
				params.attachment = attachment;
			
			invokeMethod(WallAPIName.WALL_POST, params);			
		}
		
		public function savePost(wallId:String,								  
								 server:String, 
								 photo:String, 
								 hash:String, 
								 postId:String = null,
								 message:String = null,
								 photoId:String = null):void {
			
			var params : Object = 
				{ 
					wall_id : wallId,
					server : server,
					photo : photo,
					hash : hash
				};			
				
			if (message)
				params.message = message;
			
			if (photoId)
				params.photo_id = photoId;			
			
			if (postId)
				params.post_id = postId;
			
			invokeMethod(WallAPIName.SAVE_POST, params);
		}		 
		
	}
}