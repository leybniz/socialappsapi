package api.vkontakte.business {

	import api.vkontakte.constant.ServiceName;
	import api.vkontakte.events.APIConnectionEvent;
	
	import mx.rpc.IResponder;
	
	import org.vyana.model.business.LocalConnectionServiceDelegate;

	public class AbstractLocalConnectionServiceDelegate extends LocalConnectionServiceDelegate {
		
		public static const CONNECTION_PREFIX:String = '_in_';
		public static var apiCallId:Number = 0;
		public static var apiCalls:Object = {}; 		

		function AbstractLocalConnectionServiceDelegate(responder:IResponder) {
			super(responder, ServiceName.LOCAL_CONNECTION);			
			
			if (localConnection) {
				localConnection.client = 
					{
						customEvent: onEventCallback,
						apiCallback: onApiCallback,
						onBalanceChanged: onBalanceChanged,
						onSettingsChanged: onSettingsChanged
					};
				localConnection.send(connectionName, 'initConnection');
			}
		}
		
		protected function get connectionName():String {
			return CONNECTION_PREFIX + ServiceName.LOCAL_CONNECTION;
		}
		
		public function api(method:String, params:Object, onComplete:Function = null, onError:Function = null):void {
			if (!localConnection)
				return;
			
			var callId:Number = apiCallId++;
			apiCalls[callId] = function(data: Object):void {
				if (data.error) {
					responder.fault(data.error);
				} else {
					responder.result(data.response);
				}
			}

			localConnection.send(connectionName, 'api', callId, method, params);
		}
		
		public function invokeMethod(method:String, ... rest):void {
			if (!localConnection)
				return;
			
			if (rest)
				localConnection.send.apply(localConnection, [connectionName, 'callMethod', method].concat(rest));
			else
				localConnection.send(connectionName, 'callMethod', [method]);
		}
		
		protected function onApiCallback(callId:Number, data:Object):void {
			apiCalls[callId](data);
			delete apiCalls[callId];
		}
		
		protected function onBalanceChanged(... params):void {
			var paramsArr:Array = params as Array;			
			var e:APIConnectionEvent = new APIConnectionEvent(APIConnectionEvent.BALANCE_CHANGED);
			e.params = paramsArr;
			dispatchEvent(e);			
		}
		
		protected function onSettingsChanged(... params):void {
			var paramsArr:Array = params as Array;			
			var e:APIConnectionEvent = new APIConnectionEvent(APIConnectionEvent.SETTINGS_CHANGED);
			e.params = paramsArr;
			dispatchEvent(e);			
		}		
		
		protected function onEventCallback(... params):void {
			var paramsArr:Array = params as Array;
			var eventName:String = paramsArr.shift();

//			trace(eventName);			
//			Alert.show(eventName);			
//			debug(eventName);
			var e:APIConnectionEvent = new APIConnectionEvent(eventName);
			e.params = paramsArr;
			dispatchEvent(e);
		}
		
	}
}