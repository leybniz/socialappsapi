package api.vkontakte.business {
	
	import flash.events.Event;
	
	import org.vyana.model.business.VyanaLocalConnectionService;

	public class LocalConnectionService extends VyanaLocalConnectionService {
		
		public static const CONNECTION_PREFIX:String = '_out_';
		
		function LocalConnectionService(name:String = null) {
			super(name);
		}
		
		override public function connect(connectionName:String):void {
			super.connect(CONNECTION_PREFIX + connectionName);
		}
		
	}
}