package api.vkontakte.business {
	
	import api.vkontakte.model.VkontakteApplicationModel;
	
	import flash.net.URLRequestMethod;
	
	import mx.rpc.AsyncToken;
	
	import org.vyana.model.business.VyanaHTTPService;

	/**
	 * Used to operate with own application servlet   
	 */
	public class HTTPServletService extends VyanaHTTPService {
		
		function HTTPServletService(URL:String = null, rootURL:String = null, destination:String = null) {
			super(URL, rootURL, destination);
			
			method = URLRequestMethod.POST;
		}
		
		protected function get model():VkontakteApplicationModel {
			return VyanaContext.getInstance().model as VkontakteApplicationModel;  
		}
		
		override public function send(parameters:Object = null):AsyncToken {
			// TODO: Warn user if parameters already has properties named as "key" and/or "viewer"
			
			// Append user authentication parameters			
			parameters.key = model.viewer.authKey;
			if (parameters.hasOwnProperty('authKey')) {
				parameters.key = parameters.authKey;
				delete parameters['authKey']; 
			}
			parameters.viewer = model.viewer.id;
			
			return super.send(parameters);
		}
	}
}