package api.vkontakte.business {
	
	public class UserNotificationService extends HTTPServletService {
		
		/**
		 * @param URL - url to api.vkontakte.servlet.NotificationsManagementServlet
		 */
		function UserNotificationService(URL:String = null, rootURL:String = null, destination:String = null) {
			super(URL, rootURL, destination);
			
		}
		
		public function publishNotification(message:String):String {
			var params:* = {};								
			params.act = 'enqueue';
			params.id = GUID.createNew();
			params.msg = message;
			send(params);			
			return params.id; 
		}
	}
}