package api.vkontakte.business {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.ServiceName;
	import api.vkontakte.constant.VKontakteURL;
	import api.vkontakte.model.VkontakteApplicationModel;
	
	import mx.utils.URLUtil;
	
	import org.vyana.model.VyanaModel;
	import org.vyana.model.business.VyanaHTTPService;
	import org.vyana.model.business.VyanaServices;

	public class VkontakteServices extends VyanaServices {

		function VkontakteServices() {
		}
		
		override public function init(model:VyanaModel):void {
			
			var m:VkontakteApplicationModel = model as VkontakteApplicationModel;
			var flashvars:* = m.flashvars;
			
			// Register LocalConnection service
			if (flashvars && flashvars.hasOwnProperty('lc_name')) {
				ServiceName.LOCAL_CONNECTION = flashvars.lc_name;
				registerLocalConnectionService(new LocalConnectionService(ServiceName.LOCAL_CONNECTION));
			}

			// Override service url if provided by flashvars
			if (flashvars && flashvars.hasOwnProperty('api_url')) {
				ServiceName.VKONTAKTE = flashvars.api_url;
				
				// Figure out Domain out of "api_url"
				var host:String = URLUtil.getServerNameWithPort(ServiceName.VKONTAKTE);
				// get TLD1 domain
				var domainParts:Array = host.split('.');
				while (domainParts.length > 2)
					domainParts.shift();
				VKontakteURL.DOMAIN = domainParts.join('.');
			}
			
			// Override domain if provided by flashvars
			if (flashvars && flashvars.hasOwnProperty('domain'))
				VKontakteURL.DOMAIN = flashvars.domain;
			
			registerHTTPService(new VyanaHTTPService(ServiceName.VKONTAKTE));
		}

	}
}