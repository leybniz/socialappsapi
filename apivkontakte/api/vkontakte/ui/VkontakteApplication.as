package api.vkontakte.ui {

	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.APIRestriction;
	import api.vkontakte.model.VkontakteApplicationModel;
	
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import spark.components.Application;
	import spark.layouts.VerticalLayout;


	/**
	 * 
	 * [SWF (width="#" height="#" widthPercent="#" heightPercent="#" scriptRecursionLimit="#"
	 * scriptTimeLimit="#" frameRate="#" backgroundColor="#" pageTitle="<String>")]
	 * 
	 */
	public class VkontakteApplication extends Application {

		function VkontakteApplication() {
			frameRate = 30;
			layout = new VerticalLayout();

			maxWidth = APIRestriction.MAX_APPLICATION_WIDTH;
			maxHeight = APIRestriction.MAX_APPLICATION_HEIGHT;

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}		

		public function get isWrapperAccessible():Boolean {
			return isWrapperExists && VkontakteGlobals.wrapper; 
		}
		
		public function get isWrapperAccessBroken():Boolean {
			return isWrapperExists && !isWrapperAccessible; 
		}

		public function get isWrapperExists():Boolean {
			var result:Boolean;

			if (parent.parent is Loader)
				result = true;

			return result;
		}
		
		protected function get isInstalled():Boolean {
			var result:Boolean;
			
			if (appModel && appModel.viewer.isAppUser)
				return true;
			
			return result;
		}	
		
		private function get appModel():VkontakteApplicationModel {
			return VyanaContext.getInstance().model as VkontakteApplicationModel;
		}
		
		override public function get parameters():Object {
			return VkontakteGlobals.wrapper ? VkontakteGlobals.wrapper.application.parameters : super.parameters;
		}

		override protected function initializationComplete():void {
			super.initializationComplete();			
				
			VyanaContext.getInstance();
		}
		
		protected function onAddedToStage(e:Event):void {			
			VkontakteGlobals.wrapper = parent.parent.parent;
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.application.frameRate = frameRate;
			
			if (appModel)
				appModel.init();
		}		

	}
}