package api.vkontakte.ui {
	
	import mx.managers.ISystemManager;
	import mx.managers.systemClasses.ActiveWindowManager;

	public class ActiveWindowManager extends mx.managers.systemClasses.ActiveWindowManager {
		
		function ActiveWindowManager(systemManager:ISystemManager = null) {
			super(systemManager);			
			
		}
		
		override public function activate(f:Object) : void {
			
			try {
				super.activate(f);
			} catch (e:Error) {
				// Note:  Due to Vkontakte wrapper implementation, Stage object is null
				// so systemManager.stage == null and this flow leads to RTE
				// in order to calm down focusManager we have this catch inhere
			}
		}
	}
}