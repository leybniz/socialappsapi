package api.vkontakte.ui {

	import flash.display.DisplayObject;
	import flash.events.ProgressEvent;

	public class EdensAppleDownloadProgressBar extends VkontakteDownloadProgressBar {

		[Embed(source="EdensAppleLogo.swf")]
		protected var edensAppleLogoClass:Class;

		function EdensAppleDownloadProgressBar() {
		}

		override protected function createChildren():void {
			initBackgroundImage(new edensAppleLogoClass() as DisplayObject);
		}

		override protected function showDisplayForInit(elapsedTime:int, count:int):Boolean {
			return true;
		}

		override protected function showDisplayForDownloading(elapsedTime:int, event:ProgressEvent):Boolean {
			return true;
		}

		override protected function setDownloadProgress(completed:Number, total:Number):void {

		}

		override protected function setInitProgress(completed:Number, total:Number):void {

		}
		
		protected function initBackgroundImage(image:DisplayObject):void {
			addChildAt(image, 0);

			var backgroundImageWidth:Number = image.width;
			var backgroundImageHeight:Number = image.height;

			// Scale according to backgroundSize
			var percentage:Number = calcBackgroundSize();
			if (isNaN(percentage)) {
				var sX:Number = 1.0;
				var sY:Number = 1.0;
			}
			else {
				var scale:Number = percentage * 0.01;
				sX = scale * stageWidth / backgroundImageWidth;
				sY = scale * stageHeight / backgroundImageHeight;
			}

			image.scaleX = sX;
			image.scaleY = sY;

			// Center everything.
			// Use a scrollRect to position and clip the image.
			var offsetX:Number = Math.round(0.5 * (stageWidth - backgroundImageWidth * sX));
			var offsetY:Number = Math.round(0.5 * (stageHeight - backgroundImageHeight * sY));

			image.x = offsetX;
			image.y = offsetY;

			// Adjust alpha to match backgroundAlpha
			if (!isNaN(backgroundAlpha))
				image.alpha = backgroundAlpha;
		}

		protected function calcBackgroundSize():Number {
			var percentage:Number = NaN;

			if (backgroundSize) {
				var index:int = backgroundSize.indexOf("%");
				if (index != -1)
					percentage = Number(backgroundSize.substr(0, index));
			}

			return percentage;
		}

	}
}