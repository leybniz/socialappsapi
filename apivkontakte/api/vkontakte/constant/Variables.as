package api.vkontakte.constant {

	public class Variables {
		
		public static const UNIX_TIME:uint = 0;
		public static const USER_ID:uint = 1280;
		public static const USER_NAME:uint = 1281;
		public static const SESSION_ID:uint = 2048;
		public static const SESSION_NAME:uint = 2049;		
		
	}
}