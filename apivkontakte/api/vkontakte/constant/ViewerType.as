package api.vkontakte.constant {
	
	public final class ViewerType {
		
		public static const GUEST:String = '0';
		public static const PAGE_OWNER_FRIEND:String  = '1';
		public static const PAGE_OWNER:String  = '2';
		
	}
}