package api.vkontakte.constant {
	
	/**
	 * First API request examples: 
	 * 		method=execute&code=return {
	 * 			"lng": API.language.getValues({all:1})
	 * 		};&format=json 
	 * 		
	 * 		method=execute&code=return {
	 * 				"lng": API.language.getValues({all:1}),
	 * 				"appFriends": API.getAppFriends(),  
	 * 				"wpurl": API.wall.getPhotoUploadServer(),
	 * 				"apurl": API.photos.getProfileUploadServer(),
	 * 				"viewer": API.getProfiles({"fields" : "sex, bdate", "uids" : API.getViewerId()}),
	 * 				"user": API.getProfiles({"uids":{user_id}})
	 * 		};&format=json 
	 */
	public final class InitialAPIResultField {
		
		public static var APP_FRIENDS:String  = 'appFriends';
		public static var USER_INFO:String  = 'user';
		public static var VIEWER_INFO:String  = 'viewer';
		public static var LANGUAGE:String  = 'lng';
		public static var WALL_POST_URL:String  = 'wpurl';
		public static var AVA_POST_URL:String  = 'apurl';
		
	}
}