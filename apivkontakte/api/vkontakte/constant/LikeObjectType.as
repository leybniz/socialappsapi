package api.vkontakte.constant {
	
	public final class LikeObjectType	{
		
		public static const POST:String = 'post';
		public static const PHOTO:String = 'photo';
		public static const AUDIO:String = 'audio';
		public static const VIDEO:String = 'video';
		public static const SITE_PAGE:String = 'sitepage';
		
	}
}