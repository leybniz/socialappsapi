package api.vkontakte.constant {

	public final class UserSettings {
		
		public static const NOTIFICATIONS_GRANTED:Number = 1;
		public static const FRIENDS_GRANTED:Number = 2;
		public static const PHOTOS_GRANTED:Number = 4;
		public static const AUDIOS_GRANTED:Number = 8;
		public static const VIDEOS_GRANTED:Number = 16;
		public static const OFFERS_GRANTED:Number = 32;
		public static const QUESTIONS_GRANTED:Number = 64;
		public static const WIKI_PAGES_GRANTED:Number = 128;
		public static const MENU_NAVIGATION_GRANTED:Number = 256;
		public static const WALL_POST_GRANTED:Number = 512;
		public static const ACTIVITY_GRANTED:Number = 1024;
		public static const NOTES_GRANTED:Number = 2048;		
		public static const MESSAGES_GRANTED:Number = 4096;
		public static const WALL_GRANTED:Number = 8192;
		
		protected var settings:Number;
		
		function UserSettings(settings:Number) {
			this.settings = settings; 
		}
		
		public function isSet(setting:Number):Boolean {
			return (settings & setting) > 0; 
		}
		
		public function setFlag(setting:Number):void {
			settings |= setting; 
		}		
		
	}
}