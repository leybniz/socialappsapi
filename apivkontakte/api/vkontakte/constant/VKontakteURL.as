package api.vkontakte.constant {

	public class VKontakteURL {
		
		public static var DOMAIN:String = 'vkontakte.ru';
		
		public static function get base():String {
			return 'http://' + DOMAIN + '/';
		}

		public static function get profilePage():String {
			return base + 'id';
		}
		
		public static function get appPage():String {
			return base + 'app';
		}
		
		public static function get blogPage():String {
			return base + 'blog.php?nid=';
		}
		
		public static function get clubPage():String {
			return base + 'club';
		}		
		
		public static function get publicPage():String {
			return base + 'public';
		}		

		public static function get payments():String {
			return base + 'payments.php?act=addfunds';
		}	
		
		public static function toUID(url:String):String {
			var result:String = url;
			
			if (url.length == 0)
				return result;
			
			if (url.toLowerCase().indexOf(profilePage) >= 0) {
				result = url.slice(profilePage.length);
			}
			
			return result;
		}
		
	}
}