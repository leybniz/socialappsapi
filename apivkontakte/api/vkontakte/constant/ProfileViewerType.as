package api.vkontakte.constant {
	
	public final class ProfileViewerType {
		
		public static const GUEST:int = 0;
		public static const FRIEND:int = 1;
		public static const OWNER:int = 2;
		
	}
}