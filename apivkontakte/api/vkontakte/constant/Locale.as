package api.vkontakte.constant {

	public class Locale {
		
		public static const en_US:String = 'en_US';
		public static const ru_RU:String = 'ru_RU';
		public static const ua_UA:String = 'ua_UA';
		public static const be_BE:String = 'be_BE';
		
	}
}