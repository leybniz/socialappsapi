package api.vkontakte.constant {
	
	public final class APIVersion {
		
		public static var VERSION:String  = V20;
		
		public static const V20:String  = '2.0';
		public static const V30:String  = '3.0';
		
	}
}