package api.vkontakte.constant {

	public final class WikiPageEditAccess {

		public static const EVERYBODY:int = 2;
		public static const MEMBERS:int   = 1;
		public static const OWNERS:int    = 0;
		
	}
}