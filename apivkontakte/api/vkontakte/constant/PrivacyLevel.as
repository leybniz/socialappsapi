package api.vkontakte.constant {
	
	public final class PrivacyLevel {
		
		public static const FRIENDS_FRIENDS:int = 0;
		public static const ALL:int  = 1;
		public static const FRIENDS:int  = 2;
		
	}
}