package api.vkontakte.constant {

	public class Referrer {
		
		public static const MENU:String = 'menu';
		public static const WALL_POST:String = 'wall_post';
		public static const WALL_POST_INLINE:String = 'wall_post_inline';
		public static const WALL_VIEW:String = 'wall_view';
		public static const WALL_VIEW_INLINE:String = 'wall_view_inline';
		
	}
}