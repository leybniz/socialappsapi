package api.vkontakte.constant {

	public class Gender {
		
		public static const UNKNOWN:String = '0';
		public static const FEMALE:String = '1';
		public static const MALE:String = '2';
		
	}
}