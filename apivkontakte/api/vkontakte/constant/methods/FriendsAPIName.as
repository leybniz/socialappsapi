package api.vkontakte.constant.methods {
	
	public final class FriendsAPIName {
		
		public static const GET:String   = 'friends.get';
		public static const GET_APP_USERS:String   = 'friends.getAppUsers';
		public static const GET_LISTS:String   = 'friends.getLists';		
		public static const GET_ONLINE:String   = 'friends.getOnline';
		public static const GET_MUTUAL:String   = 'friends.getMutual';
		
	}
}