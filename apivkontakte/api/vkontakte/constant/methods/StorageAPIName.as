package api.vkontakte.constant.methods {
	
	public final class StorageAPIName {
		
		public static const GET_VARIABLE:String  = 'storage.get';
		public static const SET_VARIABLE:String  = 'storage.set';
		
	}
}