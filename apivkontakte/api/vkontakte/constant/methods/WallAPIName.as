package api.vkontakte.constant.methods {

	public final class WallAPIName {
		
		public static const SAVE_POST:String   = 'wall.savePost';
		public static const WALL_POST:String   = 'wall.post';
		public static const GET_PHOTO_UPLOAD_SERVER:String   = 'wall.getPhotoUploadServer';
		
	}
}