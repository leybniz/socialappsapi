package api.vkontakte.constant.methods {
	
	public final class ActivityAPIName {
		
		public static const GET_NEWS:String  = 'activity.getNews';
		public static const GET_HISTORY:String   = 'activity.getHistory';
		public static const DELETE_HISTORY_ITEM:String  = 'activity.deleteHistoryItem';
		
	}
}