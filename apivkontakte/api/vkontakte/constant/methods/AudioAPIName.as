package api.vkontakte.constant.methods {
	
	public final class AudioAPIName {
		
		public static const GET_AUDIOS:String   = 'audio.get';
		public static const GET_AUDIOS_INFO:String   = 'audio.getById';
		public static const GET_AUDIO_UPLOAD_SERVER:String   = 'getAudioUploadServer';
		public static const SAVE_AUDIO:String  = 'audio.save';
		public static const SEARCH:String  = 'audio.search';
		public static const EDIT:String  = 'audio.edit';		
		public static const ADD:String  = 'audio.add';
		public static const DELETE:String  = 'audio.delete';
		public static const REORDER:String  = 'audio.reorder';
		
	}
}