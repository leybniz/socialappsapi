package api.vkontakte.constant.methods {
	
	public final class GeneralAPIName {
		
		public static const EXECUTE:String           = 'execute';
		
		public static const GET_ADS:String           = 'getAds';
		public static const GET_SERVER_TIME:String   = 'getServerTime';
		public static const GET_LOCALIZATION_VALUES:String   = 'language.getValues';
		
		public static const GET_VARIABLE:String   = 'getVariable';
		public static const GET_VARIABLES:String  = 'getVariables';
		public static const PUT_VARIABLE:String   = 'putVariable';
		
		public static const GET_HIGH_SCORES:String  = 'getHighScores';
		public static const SET_USER_SCORE:String   = 'setUserScore';				 
		
		public static const GET_MESSAGES:String   = 'getMessages';
		public static const SEND_MESSAGE:String   = 'sendMessage';
		
		public static const SET_MENU_NAVIGATION_LABEL:String   = 'setNameInMenu';
		public static const SET_MENU_NAVIGATION_COUNTER:String = 'setCounter';
		
	}
}