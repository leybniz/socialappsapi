package api.vkontakte.constant.methods {
	
	public final class PhotoAPIName {
		
		public static const CREATE_PHOTO_ALBUM:String   = 'photos.createAlbum';
		public static const CREATE_COMMENT:String   = 'photos.createComment';		
		public static const GET_PHOTO_ALBUMS:String   = 'photos.getAlbums';
		public static const GET_PHOTOS:String  = 'photos.get';
		public static const GET_PHOTOS_UPLOAD_SERVER:String   = 'photos.getUploadServer';
		public static const GET_PROFILE_UPLOAD_SERVER:String   = 'photos.getProfileUploadServer';
		public static const SAVE_PHOTOS:String   = 'photos.save';		 
		public static const SAVE_PROFILE_PHOTO:String   = 'photos.saveProfilePhoto';
		
	}
}