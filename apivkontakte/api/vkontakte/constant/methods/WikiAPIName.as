package api.vkontakte.constant.methods {

	public final class WikiAPIName {
		
		public static const GET:String = 'pages.get';
		public static const SAVE:String = 'pages.save';
		public static const SAVE_ACCESS:String = 'pages.saveAccess';
		public static const GET_VERSION:String = 'pages.getVersion';
		public static const GET_HISTORY:String = 'pages.getHistory';
		public static const GET_TITLES:String = 'pages.getTitles';
		
	}
}