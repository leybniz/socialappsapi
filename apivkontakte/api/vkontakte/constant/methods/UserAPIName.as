package api.vkontakte.constant.methods {
	
	public final class UserAPIName {
		
		public static const IS_APP_USER:String   = 'isAppUser';
		public static const GET_PROFILES:String  = 'getProfiles';
		public static const GET_USER_BALANCE:String   = 'getUserBalance';		 
		public static const GET_USER_SETTINGS:String   = 'getUserSettings';
		public static const GET_GROUPS:String   = 'getGroups';
		public static const GET_GROUPS_FULL:String   = 'getGroupsFull';
		public static const GET_COUNTERS:String   = 'getCounters';
		public static const GET_LIKES:String   = 'likes.getList';
		
	}
}