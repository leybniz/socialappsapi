package api.vkontakte.constant.methods {
	
	public final class GeoAPIName {
		
		public static const GET_CITIES:String   = 'getCities';
		public static const GET_COUNTRIES:String  = 'getCountries';
		
	}
}