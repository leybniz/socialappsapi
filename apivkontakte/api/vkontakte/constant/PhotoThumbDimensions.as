package api.vkontakte.constant {

	public class PhotoThumbDimensions {
		
		public static const TINY:uint = 50;
		public static const MEDIUM:uint = 100;
		public static const BIG:uint = 200;
		
	}
}