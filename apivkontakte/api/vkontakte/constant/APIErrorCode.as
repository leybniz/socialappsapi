package api.vkontakte.constant {

	public class APIErrorCode {

		public static const UNKNOWN:String = '1';
		public static const APPLICATION_DISABLED:String = '2';
		
		public static const INCORRECT_SIGNATURE:String = '4';
		public static const USER_AUTHORIZATION_FAILED:String = '5';
		public static const TOO_MANY_REQUIESTS_PER_SECOND:String = '6';
		public static const USER_PERMISSION_DENIED:String = '7';		
		public static const TEST_MODE_FOR_DISABLED_APP_ONLY:String = '11';
		
		public static const WIKI_PAGE_NOT_FOUND:String = '140';
		public static const WIKI_PAGE_ACCESS_DENIED:String = '141';
		
	}
}