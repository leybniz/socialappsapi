package api.vkontakte.constant {

	public class WrapperEventType {
	
		public static const APPLICATION_ADDED:String = 'onApplicationAdded';
		public static const SETTINGS_CHANGED:String = 'onSettingsChanged';
		public static const LOCATION_CHANGED:String = 'onLocationChanged';
		public static const BALANCE_CHANGED:String = 'onBalanceChanged';		
		public static const WINDOW_FOCUS:String = 'onWindowFocus';
		public static const WALL_POST_SAVED:String = 'onWallPostSave';
		public static const WALL_POST_CANCELLED:String = 'onWallPostCancel';		
		
	}
}