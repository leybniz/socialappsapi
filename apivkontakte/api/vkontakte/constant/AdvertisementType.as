package api.vkontakte.constant {
	
	public final class AdvertisementType {
		
		public static const TARGETTED:String  = '1';
		public static const DIRECT:String  = '2';
		public static const ALL:String  = '3';
		
	}
}