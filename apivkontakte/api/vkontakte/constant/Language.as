package api.vkontakte.constant {

	public class Language {

		public static const RUSSIAN:int = 0;
		public static const UKRAINIAN:int = 1;
		public static const BELARUSIAN:int = 2;
		public static const ENGLISH:int = 3;

		public static function toLocale(lngId:int):String {
			switch (lngId) {

				case ENGLISH:
					return Locale.en_US;
					break;

				case RUSSIAN:
					return Locale.ru_RU;
					break;

				case UKRAINIAN:
					return Locale.ua_UA;
					break;

				case BELARUSIAN:
					return Locale.be_BE;
					break;

				default:
					return Locale.ru_RU;
			}
		}
	}
}