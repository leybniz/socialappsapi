package api.vkontakte.constant {
	
	public final class GroupViewerType {
		
		public static const GUEST:String = '0';
		public static const MEMBER:String  = '1';
		public static const BOARD_MEMBER:String  = '2';
		public static const ADMIN:String  = '3';
		
	}
}