package api.vkontakte.constant {
	
	public final class UserProfileField {
		
		public static const UID:String  = 'uid';
		public static const FIRST_NAME:String  = 'first_name';
		public static const LAST_NAME:String  = 'last_name';
		public static const NICKNAME:String  = 'nickname';
		public static const SCREEN_NAME:String  = 'screen_name';		
		public static const ONLINE:String  = 'online';
		public static const LISTS:String  = 'lists';		
		public static const RATE:String  = 'rate';
		public static const HAS_MOBILE:String  = 'has_mobile';		
		public static const SEX:String  = 'sex';
		public static const BIRTH_DATE:String  = 'bdate';
		public static const CITY:String  = 'city';
		public static const COUNTRY:String  = 'country';
		public static const TIME_ZONE:String  = 'timezone';
		public static const PHOTO:String  = 'photo';
		public static const PHOTO_MEDIUM:String  = 'photo_medium';
		public static const PHOTO_BIG:String  = 'photo_big';
		public static const PHOTO_REC:String  = 'photo_rec';		
		public static const CONTACTS:String  = 'contacts';
		public static const EDUCATION:String  = 'education';
		public static const CAN_POST:String  = 'can_post';
		public static const CAN_WRITE_PRIVATE_MESSAGE:String  = 'can_write_private_message';
		public static const COUNTERS:String  = 'counters';		
		
	}
}