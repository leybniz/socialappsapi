package api.vkontakte.constant {

	public class NameCase {
		
		public static const NOM:String = 'nom';
		public static const GEN:String = 'gen';
		public static const DAT:String = 'dat';
		public static const ACC:String = 'acc';
		public static const INS:String = 'ins';
		public static const ABL:String = 'abl';
		
	}
}