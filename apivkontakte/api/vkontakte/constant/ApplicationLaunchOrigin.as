package api.vkontakte.constant {
	
	public final class ApplicationLaunchOrigin {
		
		public static const USER_PROFILE:String = 'profile';
		public static const GROUP:String  = 'group';
		
	}
}