package {

	import api.vkontakte.model.VkontakteApplicationModel;
	
	import mx.utils.StringUtil;

	/**
	 * ResourceManager shorthand, is a static wrapper class, provides
	 * localization engine isolation from the project codebase, no matter which
	 * localization engine you are going to use:
	 * - Standard Flex framework provided via ResourceManager class
	 * - Vkontakte.ru provided localization solution
	 * - Any other home brewed localization solution, etc.
	 * Utilizing this class, your project codebase will be ready to change 
	 * localization engine in no time!
	 */
	public class RM {		
		
		public static var stringKeyWrapperFunction:Function;
		public static var getStringWrapperFunction:Function;
		
		public static function get model():VkontakteApplicationModel {
			return VyanaContext.getInstance().model as VkontakteApplicationModel; 
		}
		
		public static function getString(key:String, ... args):String {
			var result:String;

			// Use Vkontakte.ru provided localization solution
			if (model && model.appDescriptor.useVkontakteLocalization) {
				
				if (stringKeyWrapperFunction != null)
					key = stringKeyWrapperFunction(key);
				
				result = model.appDescriptor.localizationValues[key];

				if (args)
					result = StringUtil.substitute(result, args);
			}
			
			// Use Flex framework provided via ResourceManager
			//result = ResourceManager.getInstance().getString(RESOURCE_BUNDLE_NAME, key, args);
			
			// Use your own localization solution here
			// ...
			if (getStringWrapperFunction != null)
				result = getStringWrapperFunction(key, args);

			if (!result)
				result = key;

			return result;
		}

	}
}