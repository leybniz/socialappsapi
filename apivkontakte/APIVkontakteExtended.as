package {

	import api.vkontakte.commands.friends.*;
	
	import org.vyana.control.commands.*;

	/**
	 * Set of API, dedicated for Desktop/Mobile applications only
	 */
	public final class APIVkontakteExtended extends VyanaCommandRegistry {

		// Friends
		public static const GET_FRIENDS_LISTS:* = GetFriendsListsCommand;		

	}

	new APIVkontakteExtended();

}