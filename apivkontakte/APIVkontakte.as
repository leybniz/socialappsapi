package {

	import api.vkontakte.commands.activity.*;
	import api.vkontakte.commands.audio.*;
	import api.vkontakte.commands.friends.*;
	import api.vkontakte.commands.general.*;
	import api.vkontakte.commands.geo.*;
	import api.vkontakte.commands.photo.*;
	import api.vkontakte.commands.questions.*;
	import api.vkontakte.commands.storage.*;
	import api.vkontakte.commands.user.*;
	import api.vkontakte.commands.wall.*;
	import api.vkontakte.commands.wiki.*;
	import api.vkontakte.commands.wrapper.*;
	
	import org.vyana.control.commands.*;

	/**
	 * Commands registry class, each command represented as separate constant
	 */
	public final class APIVkontakte extends VyanaCommandRegistry {

		// Wrapper dependent commands
		public static const INSTALL_APPLICATION:* = InstallApplicationCommand;
		public static const REFRESH_PAGE:* = RefreshPageCommand;
		public static const NAVIGATE_URL:* = NavigateUrlCommand;
		public static const SET_APPLICATION_VIEW_SIZE:* = SetApplicationViewSizeCommand;
		public static const SCROLL_WINDOW:* = ScrollWindowCommand;
		public static const ASK_PERMISSION:* = AskUserPermissionCommand;
		public static const ASK_PERMISSIONS_SET:* = AskUserPermissionsSetCommand;
		public static const ASK_VOTES:* = AskForVotesCommand;
		public static const INVITE_FRIENDS:* = InviteFriendsCommand;
		public static const ADD_TOMY_AUDIOS:* = AddToMyAudiosCommand;
		public static const PHOTO_WALL_POST:* = PhotoWallPostCommand;
		public static const AVA_POST:* = AvaPostCommand;
		
		// Storage
		public static const GET_STORED_PROPERTY:* = GetStoredPropertyCommand;		
		public static const SET_STORED_PROPERTY:* = SetStoredPropertyCommand;
		
		// Wall
		public static const GET_WALL_POST_SERVER:* = GetWallPhotoUploadServerCommand;		
		public static const SAVE_WALL_POST:* = SaveWallPostCommand;
		public static const WALL_POST:* = WallPostCommand;
		
		// Friends
		public static const GET_FRIENDS:* = GetFriendsCommand;
		public static const GET_APP_FRIENDS:* = GetAppFriendsCommand;
		public static const GET_ONLINE_FRIENDS:* = GetOnlineFriendsCommand;
		public static const GET_MUTUAL_FRIENDS:* = GetMutualFriendsCommand;
		
		// Users		
		public static const IS_APP_USER:* = IsAppUserCommand;
		public static const OPEN_USER_PROFILE:* = OpenUserProfileCommand;
		public static const GET_PROFILE_FIELDS:* = GetProfileFieldsCommand;
		public static const GET_COUNTERS:* = GetCountersCommand;
		public static const GET_USER_BALANCE:* = GetUserBalanceCommand;
		public static const GET_USER_SETTINGS:* = GetUserSettingsCommand;
		public static const GET_USER_GROUPS:* = GetUserGroupsCommand;
		public static const GET_USER_GROUPS_FULL:* = GetUserGroupsFullCommand;
		public static const GET_LIKERS_LIST:* = GetLikersListCommand;		
		
		// GEO
		public static const GET_CITIES:* = GetCitiesCommand;
		public static const GET_COUNTRIES:* = GetCountriesCommand;
		
		// Photos
		public static const GET_PHOTO_ALBUMS:* = GetPhotoAlbumsCommand;
		public static const GET_PHOTOS:* = GetPhotosCommand;
		public static const GET_PHOTOS_UPLOAD_SERVER:* = GetPhotosUploadServerCommand;
		public static const GET_PROFILE_UPLOAD_SERVER:* = GetProfileUploadServerCommand;
		public static const CREATE_PHOTO_ALBUM:* = CreatePhotoAlbumCommand;
		public static const CREATE_PHOTO_COMMENT:* = CreatePhotoCommentCommand;
		public static const SAVE_PHOTOS:* = SavePhotosCommand;
		public static const SAVE_PROFILE_PHOTO:* = SaveProfilePhotoCommand;
		
		
		// Audio
		public static const GET_AUDIO_UPLOAD_SERVER:* = GetAudioUploadServerCommand;
		public static const GET_AUDIOS:* = GetAudiosCommand;
		public static const GET_AUDIOS_INFO:* = GetAudiosInfoCommand;
		public static const SAVE_AUDIO:* = SaveAudioCommand;
		public static const SEARCH_AUDIO:* = SearchAudiosCommand;
		public static const UPDATE_AUDIO:* = UpdateAudioCommand;
		public static const ADD_AUDIO:* = AddAudioCommand;
		public static const DELETE_AUDIO:* = DeleteAudioCommand;
		public static const REORDER_AUDIO:* = ReorderAudioCommand;		
		
		// Questions
		public static const ADD_ANSWER:* = AddAnswerCommand;
		
		// Wiki pages
		public static const GET_WIKI_PAGE:* = GetWikiPageCommand;
		public static const GET_WIKI_PAGE_TITLES:* = GetWikiPageTitlesCommand;
		public static const SAVE_WIKI_PAGE:* = SaveWikiPageCommand;
		public static const SAVE_WIKI_PAGE_ACCESS:* = SaveWikiPageAccessCommand;
		
		// General
		public static const EXECUTE_VKSCRIPT:* = ExecuteVKScriptCommand;
		public static const GET_LOCALIZATION_VALUES:* = GetLocalizationValuesCommand;		
		public static const GET_ADVERTISEMENTS:* = GetAdvertisementsCommand;
		public static const GET_SERVER_TIME:* = GetServerTimeCommand;
		public static const GET_VARIABLE:* = GetVariableCommand;
		public static const GET_VARIABLES:* = GetVariablesCommand;
		public static const PUT_VARIABLE:* = PutVariableCommand;
		public static const SET_MENU_LABEL:* = SetMenuLabelCommand;
		public static const SET_MENU_COUNTER:* = SetMenuCounterCommand;
		public static const OPEN_APPLICATION_PAGE:* = OpenApplicationPageCommand;
		public static const OPEN_CLUB_PAGE:* = OpenClubPageCommand;
		public static const OPEN_PUBLIC_PAGE:* = OpenPublicPageCommand;
		public static const OPEN_BLOG_ENTRY:* = OpenBlogEntryCommand;		
		public static const OPEN_GET_VOTES_PAGE:* = OpenGetVotesPageCommand;
		public static const OPEN_AUTHOR_PROFILE:* = OpenAuthorProfileCommand;
		
		// Extra
		public static const PUT_LONG_VARIABLE:* = PutLongVariableCommand;
		public static const GET_LONG_VARIABLE:* = GetLongVariableCommand;
		public static const CLEAR_LONG_VARIABLE:* = ClearLongVariableCommand;

	}

	new APIVkontakte();

}