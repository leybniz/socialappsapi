package api.vkontakte.constant;

	public class NameCase {
		
		public static final String NOM = "nom";
		public static final String GEN = "gen";
		public static final String DAT = "dat";
		public static final String ACC = "acc";
		public static final String INS = "ins";
		public static final String ABL = "abl";
		
	}
