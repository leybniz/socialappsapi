package api.vkontakte.constant;

public class SecureMethods {
	
	public static final String ADD_RATING  = "secure.addRating";
	
	public static final String IS_APP_USER  = "isAppUser";
	public static final String GET_PROFILES  = "secure.getProfiles";
	public static final String GET_APP_BALANCE  = "secure.getAppBalance";
	public static final String WITHDRAW_VOTES  = "secure.withdrawVotes";
	public static final String SEND_NOTIFICATION = "secure.sendNotification";	
	
	public static final String SET_APP_STATUS  = "secure.saveAppStatus";
	public static final String SET_COUNTER  = "secure.setCounter";
	
	public static final String SET_LANGUAGE_VALUE  = "secure.setLanguageValue";
	public static final String DELETE_LANGUAGE_VALUE  = "secure.deleteLanguageValue";
	
}
