package api.vkontakte.constant;

	public class Gender {
		
		public static final String UNKNOWN = "0";
		public static final String FEMALE = "1";
		public static final String MALE = "2";
		
	}
