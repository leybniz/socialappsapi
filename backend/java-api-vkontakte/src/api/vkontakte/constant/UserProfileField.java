package api.vkontakte.constant;

public class UserProfileField {
	
	public static final String  UID = "uid";
	public static final String  FIRST_NAME = "first_name";
	public static final String  LAST_NAME = "last_name";
	public static final String  NICKNAME = "nickname";
	public static final String  ONLINE = "online";
	public static final String  DOMAIN = "domain";
	public static final String  LISTS = "lists";		
	public static final String  RATE = "rate";
	public static final String  HAS_MOBILE = "has_mobile";		
	public static final String  SEX = "sex";
	public static final String  BIRTH_DATE = "bdate";
	public static final String  CITY = "city";
	public static final String  COUNTRY = "country";
	public static final String  TIME_ZONE = "timezone";
	public static final String  PHOTO = "photo";
	public static final String  PHOTO_MEDIUM = "photo_medium";
	public static final String  PHOTO_BIG = "photo_big";
	public static final String  PHOTO_REC = "photo_rec";		
	public static final String  CONTACTS = "contacts";
	public static final String  EDUCATION = "education";

}
