package api.vkontakte.servlet;

import java.util.Date;

import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class SessionsCleanerServlet extends HttpServlet {

	public static String SESSION_ENTITY_NAME = "_ah_SESSION";
	public static String SESSION_PROPERTY_EXPIRES = "_expires";
	
	
	private DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
	
	public SessionsCleanerServlet() {
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) {		
		
		// select expired sessions
		Query query = new Query(SESSION_ENTITY_NAME);
		query.addFilter(SESSION_PROPERTY_EXPIRES, Query.FilterOperator.LESS_THAN, new Date().getTime());
		
		for (Entity e : ds.prepare(query).asIterable()) {
			ds.delete(e.getKey());
		}
		
	}
	
}