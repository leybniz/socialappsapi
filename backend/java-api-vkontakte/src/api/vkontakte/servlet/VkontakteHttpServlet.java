package api.vkontakte.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import api.vkontakte.exceptions.VkontakteException;
import api.vkontakte.model.VkontakteService;

@SuppressWarnings("serial")
public class VkontakteHttpServlet extends HttpServlet {

	public static final String P_AUTH_KEY  = "key";
	public static final String P_VIEWER_ID  = "viewer";
	
	protected VkontakteService service;

	public VkontakteHttpServlet() {

	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		if (service == null)
			throw new VkontakteException("Service instance not set");		
		
		String authKey = req.getParameter(P_AUTH_KEY);
		String ownAuthKey = service.md5(service.getAppDescriptor().getAppId() + "_" + req.getParameter(P_VIEWER_ID) + "_" + service.getAppDescriptor().getAppSecret());
		
		// If user authentication failed
		if (authKey == null || !authKey.equals(ownAuthKey)) {			
			throw new VkontakteException("User authentication failed: authKey:" + String.valueOf(authKey) + ", ownAuthKey:" + ownAuthKey);
		}	

	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet(req, resp);
	}
	
}
