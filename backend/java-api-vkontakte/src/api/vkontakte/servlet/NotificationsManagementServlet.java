package api.vkontakte.servlet;

import static com.google.appengine.api.datastore.FetchOptions.Builder.withDefaults;
import static com.google.appengine.api.datastore.FetchOptions.Builder.withLimit;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
//import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import api.vkontakte.constant.SecureMethods;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

@SuppressWarnings("serial")
public class NotificationsManagementServlet extends VkontakteHttpServlet {

	public static final int MAX_RECIPIENTS_PER_CALL = 100;
	
	public static final String P_MESSAGE  = "msg";
	public static final String P_ID  = "id";
	public static final String P_ACTION  = "act";
	public static final String ACTION_ENQUEUE  = "enqueue";
	
	
	public static String USER_ENTITY_NAME = "_USER";
	public static String USER_ENTITY_KEY = "__key__";
	
	public static String NOTIFICATION_ENTITY_NAME = "_NOTIFICATION";
	public static String NOTIFICATION_PROPERTY_TEXT = "msg";
	public static String NOTIFICATION_PROPERTY_OFFSET = "offset";
	public static String NOTIFICATION_PROPERTY_CALLS_PER_SECOND = "calls";
	
	private DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
	//private static final Logger log = Logger.getLogger(NotificationsManagementServlet.class.getName());
	
	public NotificationsManagementServlet() {

	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {		
		
		if (req != null && req.getParameter(P_ACTION) != null) {
			super.doGet(req, resp);
			
			if (req.getParameter(P_ACTION).equals(ACTION_ENQUEUE))
				enqueueNotification(req.getParameter(P_ID), req.getParameter(P_MESSAGE));			
		} else {
			doTheJob();
		}				

	}
	
	public void enqueueNotification(String id, String msg) {
		Key key = KeyFactory.createKey(NOTIFICATION_ENTITY_NAME, id);
		Entity e = new Entity(key);
		e.setProperty(NOTIFICATION_PROPERTY_TEXT, new Text(msg));
		e.setProperty(NOTIFICATION_PROPERTY_OFFSET, "0");		
		
		
		Query query = new Query(USER_ENTITY_NAME);
		int usersCouint = ds.prepare(query).countEntities(withDefaults());
		int callsPerSecond = 1;
		
		if (usersCouint <= 10000)
			callsPerSecond = 5;
		
		if (usersCouint > 10000 && usersCouint <= 100000)
			callsPerSecond = 8;
		
		if (usersCouint > 100000 && usersCouint <= 1000000)
			callsPerSecond = 20;
		
		if (usersCouint > 1000000)
			callsPerSecond = 35;		
		
		e.setProperty(NOTIFICATION_PROPERTY_CALLS_PER_SECOND, callsPerSecond);		
		ds.put(e);
	}
	
	protected void doTheJob() {
		Entity notificationEntity = null;
		String offset = "0";
		String text = null;
		int callsPerSecond = 1;
		
		// get the notification Entity
		Query query = new Query(NOTIFICATION_ENTITY_NAME);
		for (Entity e : ds.prepare(query).asIterable()) {
			notificationEntity = e;
			offset = e.getProperty(NOTIFICATION_PROPERTY_OFFSET).toString();
			callsPerSecond = Integer.valueOf(e.getProperty(NOTIFICATION_PROPERTY_CALLS_PER_SECOND).toString());
			text = ((Text) e.getProperty(NOTIFICATION_PROPERTY_TEXT)).getValue();
		}
		
		if (notificationEntity == null)
			return;
		
		for (int i = 0; i < callsPerSecond; i++) {
					
			// TODO: Make 1 query, take it out of the loop
			query = new Query(USER_ENTITY_NAME);
			query.setKeysOnly();
			query.addFilter(USER_ENTITY_KEY, Query.FilterOperator.GREATER_THAN,	KeyFactory.createKey(USER_ENTITY_NAME, offset));
			
			List<Entity> lst = ds.prepare(query).asList(withLimit(MAX_RECIPIENTS_PER_CALL));
			
			// Broadcast is over
			if (lst.size() == 0) {
				ds.delete(notificationEntity.getKey());
				return;
			}
			
			String uids = "";
			Entity lastUser = null;
			for (Entity ue : lst) {
				if (uids.equals("")) {
					uids = uids.concat(ue.getKey().getName());
				} else {
					uids = uids.concat(",").concat(ue.getKey().getName());
				}
				
				lastUser = ue;
			}
			
			// Send batch
			sendNotification(uids, text);
			offset = lastUser.getKey().getName();
			notificationEntity.setProperty(NOTIFICATION_PROPERTY_OFFSET, offset);
			ds.put(notificationEntity);			
		}
		
	}
	
	protected void sendNotification(String uid, String message) {
		//log.info(uid);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("uids", uid);
		params.put("message", message);		
		try {
			service.invokeMethod(SecureMethods.SEND_NOTIFICATION, params);
		} catch (IOException e1) {
			e1.printStackTrace();
		}		
	}	
	
}
