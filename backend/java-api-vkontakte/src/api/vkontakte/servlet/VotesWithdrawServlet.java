package api.vkontakte.servlet;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import api.vkontakte.constant.SecureMethods;

import com.google.appengine.repackaged.org.json.JSONException;
import com.google.appengine.repackaged.org.json.JSONObject;

@SuppressWarnings("serial")
public class VotesWithdrawServlet extends VkontakteHttpServlet {

	public static final String P_UID  = "uid";
	public static final String P_VOTES  = "votes";
	
	public VotesWithdrawServlet() {
		

	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		super.doGet(req, resp);
		
		if (req != null && req.getParameter(P_UID) != null && req.getParameter(P_VOTES) != null) {
			resp.getWriter().write(withdrawVotes(req.getParameter(P_UID), req.getParameter(P_VOTES)));
		}
	}
	
	protected String withdrawVotes(String uid, String votes) {
		String result = "";
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("uid", uid);
		params.put("votes", votes);		
		try {
			try {
				JSONObject r = new JSONObject(service.invokeMethod(SecureMethods.WITHDRAW_VOTES, params));
				if (r != null)
					result = r.getString("response");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return result;
	}
	
	protected Boolean addRating(String uid, int rate, String message) {
		Boolean result = false;
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("uid", uid);
		params.put("message", message + "__________");
		params.put("rate", String.valueOf(rate));
		try {
			try {
				JSONObject r = new JSONObject(service.invokeMethod(SecureMethods.ADD_RATING, params));
				if (r != null && r.getInt("response") == rate)
					result = true;				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return result;
	}
	
}
