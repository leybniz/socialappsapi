package api.vkontakte.exceptions;

@SuppressWarnings("serial")
public class VkontakteException extends RuntimeException {
	
	protected String message = "";
	
	public VkontakteException(String message) {
		this.message = message;
		
	}
	
	public String getMessage() {
		return message;
	}	

}
