package api.vkontakte.model;

import api.vkontakte.constant.ResponseFormat;

public class VkontakteApplicationDescriptor {
	
	private String appId;
	private String authorUID;
	private String appSecret;
	private String responseFormat = ResponseFormat.JSON;
	
	public VkontakteApplicationDescriptor(String appId, String appSecret, String appAuthorUID) {
		this.setAppId(appId);
		this.setAppSecret(appSecret);
		this.setAuthorUID(appAuthorUID);
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setResponseFormat(String responseFormat) {
		this.responseFormat = responseFormat;
	}

	public String getResponseFormat() {
		return responseFormat;
	}

	public void setAuthorUID(String authorUID) {
		this.authorUID = authorUID;
	}

	public String getAuthorUID() {
		return authorUID;
	}

}
