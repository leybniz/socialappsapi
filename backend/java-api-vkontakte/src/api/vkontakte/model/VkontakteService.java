package api.vkontakte.model;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.*;
import java.net.URL;
import java.net.URLEncoder;
import java.security.*;

import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPMethod;

import api.vkontakte.constant.APIVersion;
import api.vkontakte.constant.ServiceName;

public class VkontakteService {
	
	//private static final Logger log = Logger.getLogger(VkontakteService.class.getName());
	
	public static final String PARAM_METHOD = "method";
	public static final String PARAM_FORMAT = "format";
	public static final String PARAM_APP_ID = "api_id";
	public static final String PARAM_TIMESTAMP = "timestamp";
	public static final String PARAM_VERSION = "v";
	public static final String PARAM_RANDOM = "random";
	public static final String PARAM_SIGNATURE = "sig";

	protected VkontakteApplicationDescriptor appDescriptor;
	private URLFetchService service = URLFetchServiceFactory
			.getURLFetchService();

	public VkontakteService() {

	}

	public String invokeMethod(String method, Map<String, String> params)
			throws IOException {

		if (params == null)
			params = new HashMap<String, String>();

		params.put(PARAM_APP_ID, getAppDescriptor().getAppId());
		params.put(PARAM_FORMAT, getAppDescriptor().getResponseFormat());
		params.put(PARAM_METHOD, method);
		params.put(PARAM_VERSION, APIVersion.VERSION);
		params.put(PARAM_TIMESTAMP, Long.toString(Calendar.getInstance()
				.getTime().getTime()));
		params.put(PARAM_RANDOM, Long.toString(Math.round(new Random()
				.nextDouble() * 10000000)));
		params.put(PARAM_SIGNATURE, getParamsSignature(params));

		return call(params);
	}

	protected String call(Map<String, String> params) throws IOException {		
		String query = "";
		HTTPRequest request;
		HTTPResponse result;
		URL url;

		for (String param : params.keySet()) {
			if (!query.equals("")) {
				query += "&";
			}

			if (param.equals("message") || param.equals("status") || param.equals("value")) {
				query += param + "=" + URLEncoder.encode(params.get(param), "UTF-8");
			} else {
				query += param + "=" + params.get(param);
			}
		}
		
// GET
//		url = new URL(ServiceName.VKONTAKTE + "?" + query);
//		request = new HTTPRequest(url, HTTPMethod.GET);
		
// POST
		url = new URL(ServiceName.VKONTAKTE);
		request = new HTTPRequest(url, HTTPMethod.POST);		
		request.addHeader(new HTTPHeader("Content-Type",
				"application/x-www-form-urlencoded"));
		request.setPayload(query.getBytes("UTF-8"));		
		
		result = service.fetch(request);
		return getConentString(result, "UTF-8");
	}

	protected String getParamsSignature(Map<String, String> params) throws UnsupportedEncodingException {
		String sig = "";

		String[] arrayToSort = new String[params.size()];
		params.keySet().toArray(arrayToSort);
		Arrays.sort(arrayToSort);

		for (String key : arrayToSort) {
			sig += key + "=" + params.get(key);
		}
		sig += getAppDescriptor().getAppSecret();

		return md5(sig);
	}

	public String md5(String s) throws UnsupportedEncodingException {
		MessageDigest mdEnc = null;
		try {
			mdEnc = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		// Correct:
		byte [] b = s.getBytes("UTF-8");
		mdEnc.update(b, 0, b.length);
		
		// Wrong:
		//mdEnc.update(encData, 0, s.length());
		
		return new BigInteger(1, mdEnc.digest()).toString(16);
	}

	protected String getConentString(HTTPResponse response, String encoding) {
		try {
			if (response.getResponseCode() == 200) {
				return new String(response.getContent(), encoding);
			} else {
				return "";
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
	}

	public void setAppDescriptor(VkontakteApplicationDescriptor appDescriptor) {
		this.appDescriptor = appDescriptor;
	}

	public VkontakteApplicationDescriptor getAppDescriptor() {
		return appDescriptor;
	}

}
