package {
	
	import api.odnoklassniki.commands.events.*;
	import api.odnoklassniki.commands.forticom.*;
	import api.odnoklassniki.commands.friends.*;
	import api.odnoklassniki.commands.photos.*;
	import api.odnoklassniki.commands.stream.*;
	import api.odnoklassniki.commands.users.*;
	import api.odnoklassniki.commands.widgets.*;
	
	import org.vyana.control.commands.*;

	/**
	 * Commands registry class, each command represented as separate constant
	 */
	public final class APIOdnoklassniki extends VyanaCommandRegistry {

		// Forticom		
		public static const SHOW_INSTALL:* = ShowInstallCommand;
		public static const SHOW_INVITE:* = ShowInviteCommand;
		public static const SHOW_SETTINGS:* = ShowSettingsCommand;
		public static const SHOW_FEED:* = ShowFeedCommand;
		public static const SHOW_PAYMENT:* = ShowPaymentCommand;
		
		// Friends		
		public static const GET_FRIENDS:* = GetFriendsCommand;
		public static const GET_APP_FRIENDS:* = GetAppFriendsCommand;
		public static const GET_ONLINE_FRIENDS:* = GetOnlineFriendsCommand;
		public static const GET_MUTUAL_FRIENDS:* = GetMutualFriendsCommand;
		public static const GET_BIRTHDAYS:* = GetBirthdaysCommand;
		public static const ARE_FRIENDS:* = AreFriendsCommand;
		
		// Users
		public static const GET_LOGGEDIN_USER:* = GetLoggedInUserCommand;
		public static const GET_USERS_INFO:* = GetUsersInfoCommand;
		public static const SET_USER_STATUS:* = SetUserStatusCommand;
		
		// Photos		
		public static const GET_PHOTO_UPLOAD_URL:* = GetPhotoUploadUrlCommand;
		
		// Events		
		public static const GET_USER_EVENTS:* = GetUserEventsCommand;
		public static const GET_USER_EVENTS_INFO:* = GetUserEventsInfoCommand;
		
		// Widgets
		public static const GET_WIDGETS:* = GetWidgetsCommand;
		
		// Stream		
		public static const STREAM_PUBLISH:* = StreamPublishCommand;

	}

	new APIOdnoklassniki();

}