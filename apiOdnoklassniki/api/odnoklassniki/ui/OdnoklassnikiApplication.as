package api.odnoklassniki.ui {

	import api.odnoklassniki.constant.APIRestriction;
	import api.odnoklassniki.model.OdnoklassnikiApplicationModel;
	
	import flash.events.Event;
	
	import spark.components.Application;
	import spark.layouts.VerticalLayout;

	public class OdnoklassnikiApplication extends Application {

		function OdnoklassnikiApplication() {
			frameRate = 30;
			layout = new VerticalLayout();

			maxWidth = APIRestriction.MAX_APPLICATION_WIDTH;
			maxHeight = APIRestriction.MAX_APPLICATION_HEIGHT;

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}		
		
		protected function get appModel():OdnoklassnikiApplicationModel {
			return VyanaContext.getInstance().model as OdnoklassnikiApplicationModel;
		}

		override protected function initializationComplete():void {
			super.initializationComplete();			
				
			VyanaContext.getInstance();
		}
		
		protected function onAddedToStage(e:Event):void {			
			
			if (appModel)
				appModel.init();
			
		}		

	}
}