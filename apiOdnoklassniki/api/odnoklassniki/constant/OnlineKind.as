package api.odnoklassniki.constant {
	
	public final class OnlineKind {
		
		public static const WEB:String  = 'web';
		public static const WAP:String  = 'wap';
		public static const MOBILE:String  = 'mobile';
		
	}
}