package api.odnoklassniki.constant {

	public class APIErrorCode {

		public static const UNKNOWN:String = '1';
		public static const SERVICE_UNAVAILABLE:String = '2';
		public static const UNKNOWN_METHOD:String = '3';
		public static const INVALID_REQUEST:String = '4';
		public static const PERMISSION_DENIED:String = '10';
		public static const PARAM:String = '100';
		public static const PARAM_API_KEY:String = '101';		
		
	}
}
