package api.odnoklassniki.constant {
	
	public final class EventKind {
		
		public static const DISCUSSIONS:String  = 'discussions';
		public static const GUESTS:String  = 'guests';
		public static const MESSAGES:String  = 'messages';
		public static const MARKS:String  = 'marks';
		public static const NOTIFICATIONS:String  = 'notifications';
		public static const ACTIVITIES:String  = 'activities';		
		
	}
}