package api.odnoklassniki.constant {
	
	public final class UserProfileField {
		
		public static const UID:String  = 'uid';
		public static const FIRST_NAME:String  = 'first_name';
		public static const LAST_NAME:String  = 'last_name';
		public static const NAME:String  = 'name';		
		public static const GENDER:String  = 'gender';
		public static const BIRTH_DATE:String  = 'birthdate';
		public static const AGE:String  = 'age';
		public static const LOCALE:String  = 'locale';
		public static const LOCATION:String  = 'location';
		public static const CURRENT_LOCATION:String  = 'current_location';
		public static const CURRENT_STATUS:String  = 'current_status';
		public static const PIC1:String  = 'pic_1';
		public static const PIC2:String  = 'pic_2';
		public static const PIC3:String  = 'pic_3';
		public static const PIC4:String  = 'pic_4';
		public static const URL_PROFILE:String  = 'url_profile';
		public static const URL_PROFILE_MOBILE:String  = 'url_profile_mobile';
		public static const URL_CHAT:String  = 'url_chat';
		public static const URL_CHAT_MOBILE:String  = 'url_chat_mobile';
		
	}
}