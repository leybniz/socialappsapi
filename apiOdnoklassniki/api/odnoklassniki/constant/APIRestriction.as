package api.odnoklassniki.constant {
	
	public final class APIRestriction {
		
		public static const MAX_REQUESTS_PER_SECOND:int  = 20;
		
		public static const MAX_APPLICATION_HEIGHT:int  = 900;
		public static const MAX_APPLICATION_WIDTH:int  = 730;
		
		
	}
}