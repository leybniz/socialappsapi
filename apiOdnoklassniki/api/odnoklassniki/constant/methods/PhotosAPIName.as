package api.odnoklassniki.constant.methods {
	
	public final class PhotosAPIName {
		
		public static const GET_UPLOAD_URL:String   = 'photos.getUploadUrl';
		public static const UPLOAD:String   = 'photos.upload';		
		
	}
}