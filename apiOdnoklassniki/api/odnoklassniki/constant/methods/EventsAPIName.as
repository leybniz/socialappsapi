package api.odnoklassniki.constant.methods {
	
	public final class EventsAPIName {
		
		public static const GET:String   = 'events.get';
		public static const GET_TYPE_INFO:String   = 'events.getTypeInfo';		
		
	}
}