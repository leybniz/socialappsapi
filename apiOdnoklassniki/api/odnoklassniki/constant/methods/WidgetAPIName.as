package api.odnoklassniki.constant.methods {
	
	public final class WidgetAPIName {
		
		public static const GET_WIDGETS:String   = 'widget.getWidgets';
		
	}
}