package api.odnoklassniki.constant.methods {
	
	public final class FriendsAPIName {
		
		public static const GET:String   = 'friends.get';		
		public static const ARE_FRIENDS:String   = 'friends.areFriends';
		public static const GET_MUTUAL_FRIENDS:String   = 'friends.getMutualFriends';
		public static const GET_APP_USERS:String   = 'friends.getAppUsers';
		public static const GET_BIRTHDAYS:String   = 'friends.getBirthdays';		
		public static const GET_ONLINE:String   = 'friends.getOnline';
		
	}
}