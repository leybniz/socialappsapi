package api.odnoklassniki.constant.methods {
	
	public final class UsersAPIName {
		
		public static const GET_INFO:String   = 'users.getInfo';
		public static const SET_STATUS:String   = 'users.setStatus';		
		public static const GET_LOGGEDIN_USER:String   = 'users.getLoggedInUser';
		
	}
}