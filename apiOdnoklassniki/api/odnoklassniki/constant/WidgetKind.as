package api.odnoklassniki.constant {
	
	public final class WidgetKind {
		
		public static const MOBILE_HEADER:String  = 'mobile-header';
		public static const MOBILE_FOOTER:String  = 'mobile-footer';
		
	}
}