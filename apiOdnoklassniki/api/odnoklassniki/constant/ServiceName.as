package api.odnoklassniki.constant {
	
	public final class ServiceName {
		
		public static var REST_BASE:String  = 'http://api.odnoklassniki.ru/';
		public static var FORTICOM:String;
		
		
		public static function get fbURL():String {
			return REST_BASE + 'fb.do';
		}
		
	}
}