package api.odnoklassniki.model {

	import api.odnoklassniki.model.vo.OdnoklassnikiApplicationDescriptor;
	import api.odnoklassniki.model.vo.Viewer;
	
	import mx.core.FlexGlobals;
	
	import org.vyana.model.VyanaModel;

	public class OdnoklassnikiApplicationModel extends VyanaModel {
		
		public var appDescriptor:OdnoklassnikiApplicationDescriptor;		
		public var viewer:Viewer;
		
		function OdnoklassnikiApplicationModel(appKey:String) {			
			
			appDescriptor = new OdnoklassnikiApplicationDescriptor(appKey, flashvars);
			
			// Does this app stolen?
			if (appDescriptor.isStolen) {
				// Neverending loop ;)
				do {
					// Nirvana :)
				} while (true); 
			}
			
		}
		
		protected function get app():* {
			return FlexGlobals.topLevelApplication;
		}

		public function get flashvars():Object {
			return app.parameters;
		}
		
		override public function init():void {
			super.init();
			
			viewer = new Viewer(appDescriptor, flashvars);
			appDescriptor.flashvars = flashvars;
			
		}

	}

}