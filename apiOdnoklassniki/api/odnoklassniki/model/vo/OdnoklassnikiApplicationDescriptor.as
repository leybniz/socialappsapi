package api.odnoklassniki.model.vo {
	
	import api.odnoklassniki.constant.ResponseFormat;
	
	import flash.net.LocalConnection;
	import flash.utils.Dictionary;
	
	import org.vyana.model.vo.VyanaValueObject;

	public class OdnoklassnikiApplicationDescriptor extends VyanaValueObject {
		
		/**
		 * Defines API response format
		 * default: format is JSON, because it's much faster than XML
		 */
		public var responseFormat:String = ResponseFormat.JSON;

		protected var lc:LocalConnection = new LocalConnection();
		protected var _key:String;
		protected var _flashvars:Object;
		
		function OdnoklassnikiApplicationDescriptor(key:String, fVars:Object) {
			
			this.key = key;
			this.flashvars = fVars ? fVars: {};
			
		}
		
		/**
		 * Hash to be checked on server side 
		 */
		public function get authKey():String {
			return flashvars.sig;
		}

		public function get apiConnection():String {
			return flashvars.apiconnection;
		}

		public function get key():String {
			return _key;
		}

		public function set key(value:String):void {
			_key = value;
		}

		public function get flashvars():Object {
			return _flashvars;
		}

		public function set flashvars(value:Object):void {
			_flashvars = value;
		}

		public function get isProduction():Boolean {
			return flashvars.hasOwnProperty('api_server');
		}		
		
		public function get isStolen():Boolean {
			return isProduction && key.toUpperCase() != String(flashvars.application_key).toUpperCase();
		}
		
		public function get isSecondInstance():Boolean {
			try {				
				lc.connect(key);
			}
			catch (e:Error) {
				return true;
			}
			
			return false;
		}
		
		public function get referrer():String {
			return flashvars.referer;
		}
		
		public function get launchOrigin():String {
			return flashvars.refplace;
		}

	}
}