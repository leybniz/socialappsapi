package api.odnoklassniki.model.vo {

	import org.vyana.model.vo.VyanaValueObject;

	public class Viewer extends VyanaValueObject {

		private var _flashvars:*;
		private var _applicationDescriptor:OdnoklassnikiApplicationDescriptor;
		private var _session:Session;

		function Viewer(appDescriptor:OdnoklassnikiApplicationDescriptor, parameters:*) {

			_applicationDescriptor = appDescriptor;
			_flashvars = parameters ? parameters : {};
			_session = new Session(_flashvars.session_key, _flashvars.session_secret_key);
		}

		public function get session():Session {
			return _session;
		}

		public function set session(value:Session):void {
			_session = value;
		}
		
		public function get isAppUser():Boolean {
			return _flashvars.authorized == '1';
		}

		public function get id():String {
			return _flashvars.logged_user_id;
		}
		
		public function get referrer():String {
			return _flashvars.referer;
		}

	}
}