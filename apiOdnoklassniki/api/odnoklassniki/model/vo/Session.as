package api.odnoklassniki.model.vo {

	import org.vyana.model.vo.VyanaValueObject;

	public class Session extends VyanaValueObject {

		private var _key:String;
		private var _secretKey:String;

		function Session(key:String, secretKey:String) {
			this.key = key;
			this.secretKey = secretKey;
		}

		public function get secretKey():String {
			return _secretKey;
		}

		public function set secretKey(value:String):void {
			_secretKey = value;
		}

		public function get key():String {
			return _key;
		}

		public function set key(value:String):void {
			_key = value;
		}

	}
}