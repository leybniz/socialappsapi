package api.odnoklassniki.commands.stream {
	
	import org.vyana.control.VyanaEvent;

	public class StreamPublishCommand extends AbstractStreamCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.publish(data.message, data.attachment, data.actionLinks);
		}
		
	}
}