package api.odnoklassniki.commands.stream {

	import api.odnoklassniki.business.delegates.StreamServiceDelegate;
	import api.odnoklassniki.commands.OdnoklassnikiCommand;
	
	import mx.rpc.IResponder;

	public class AbstractStreamCommand extends OdnoklassnikiCommand {
		
		protected var delegate:StreamServiceDelegate = new StreamServiceDelegate(this as IResponder);
		
	}
}