package api.odnoklassniki.commands.events {
	
	import org.vyana.control.VyanaEvent;

	public class GetUserEventsInfoCommand extends AbstractEventsCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getEventsInfo(data.types, data.locale);
		}
		
	}
}