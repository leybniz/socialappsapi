package api.odnoklassniki.commands.events {

	import api.odnoklassniki.business.delegates.EventsServiceDelegate;
	import api.odnoklassniki.commands.OdnoklassnikiCommand;
	
	import mx.rpc.IResponder;

	public class AbstractEventsCommand extends OdnoklassnikiCommand {
		
		protected var delegate:EventsServiceDelegate = new EventsServiceDelegate(this as IResponder);
		
	}
}