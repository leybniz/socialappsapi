package api.odnoklassniki.commands.events {
	
	import org.vyana.control.VyanaEvent;

	public class GetUserEventsCommand extends AbstractEventsCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getEvents(data.uid, data.types);
		}
		
	}
}