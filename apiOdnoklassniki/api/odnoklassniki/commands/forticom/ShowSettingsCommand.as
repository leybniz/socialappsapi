package api.odnoklassniki.commands.forticom {
	
	import org.vyana.control.VyanaEvent;

	public class ShowSettingsCommand extends AbstractForticomCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.showSettings();
		}
		
	}
}