package api.odnoklassniki.commands.forticom {

	import api.odnoklassniki.business.delegates.ForticomServiceDelegate;
	import api.odnoklassniki.commands.OdnoklassnikiCommand;
	
	import mx.rpc.IResponder;

	public class AbstractForticomCommand extends OdnoklassnikiCommand {
		
		protected var delegate:ForticomServiceDelegate = new ForticomServiceDelegate(this as IResponder);
		
	}
}