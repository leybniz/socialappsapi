package api.odnoklassniki.commands.forticom {
	
	import org.vyana.control.VyanaEvent;

	public class ShowFeedCommand extends AbstractForticomCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.showFeed(data.uid, data.attachments, data.actionLinks);
		}
		
	}
}