package api.odnoklassniki.commands.forticom {
	
	import org.vyana.control.VyanaEvent;

	public class ShowPaymentCommand extends AbstractForticomCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.showPayment(
				data.name, 
				data.description, 
				data.code, 
				data.price, 
				data.options, 
				data.attributes
			);
		}
		
	}
}