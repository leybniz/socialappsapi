package api.odnoklassniki.commands {

	import api.odnoklassniki.business.AbstractOdnoklassnikiServiceDelegate;
	import api.odnoklassniki.constant.ResponseFormat;
	import api.odnoklassniki.model.OdnoklassnikiApplicationModel;
	
	import com.adobe.serialization.json.JSON;
	
	import flash.utils.Dictionary;
	
	import mx.rpc.Fault;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import org.vyana.control.commands.AsynchronousCommand;

	public class OdnoklassnikiCommand extends AsynchronousCommand {

		protected var resultMessages:Dictionary = new Dictionary();

		protected function get applicationModel():OdnoklassnikiApplicationModel {
			return context.model as OdnoklassnikiApplicationModel;
		}

		protected function get isXML():Boolean {
			return applicationModel.appDescriptor.responseFormat == ResponseFormat.XML;
		}

		protected function get isJSON():Boolean {
			return applicationModel.appDescriptor.responseFormat == ResponseFormat.JSON;
		}

		protected function detectResultFault(e:ResultEvent):Fault {
			var f:Fault;
			var r:* = e.result;

			if (!r)
				return f;

			// JSON
			if (r.hasOwnProperty('error'))
				f = new Fault(r.error.error_code, r.error.error_msg, ObjectUtil.toString(r));

			// XML
			if (r is XML && XML(r).localName() == 'error')
				f = new Fault(r.error_code, r.error_msg, ObjectUtil.toString(r));

			return f;
		}

		override public function result(e:Object):void {
			var re:ResultEvent = e as ResultEvent;
			
			// Duplicated result event passed, ignore
			if (re && resultMessages[re.messageId])
				return;
			
			resultMessages[re.messageId] = re.message;				

			if (isJSON) {								
				var resObject:* = e.result;
				
				try {
					if (resObject is String) {
						var strResult:String = e.result as String;
						resObject = (strResult.length > 0) ? JSON.decode(strResult) : {};
					}					
					
					re = ResultEvent.createEvent(resObject, e.token, e.message);
				} catch (err:Error) {
					
				}
			}	

			// Is this an Error?
			var f:Fault = detectResultFault(re);
			if (f) {
				// If so, redirect it to proper destination ;)
				fault(new FaultEvent(FaultEvent.FAULT, false, true, f));

				return;
			}

			super.result(re);
		}

		override protected function onResult(e:ResultEvent):void {			
			super.onResult(e);

			AbstractOdnoklassnikiServiceDelegate.requestsQueue.startProcessing();
		}

		override protected function onFault(e:FaultEvent):void {
			super.onFault(e);

			context.dispatchEvent(e);

			AbstractOdnoklassnikiServiceDelegate.requestsQueue.startProcessing();
		}

	}
}