package api.odnoklassniki.commands.widgets {
	
	import org.vyana.control.VyanaEvent;

	public class GetWidgetsCommand extends AbstractWidgetsCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getWidgets(data);
		}
		
	}
}