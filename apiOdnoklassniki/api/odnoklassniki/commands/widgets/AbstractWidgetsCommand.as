package api.odnoklassniki.commands.widgets {

	import api.odnoklassniki.business.delegates.WidgetsServiceDelegate;
	import api.odnoklassniki.commands.OdnoklassnikiCommand;
	
	import mx.rpc.IResponder;

	public class AbstractWidgetsCommand extends OdnoklassnikiCommand {
		
		protected var delegate:WidgetsServiceDelegate = new WidgetsServiceDelegate(this as IResponder);
		
	}
}