package api.odnoklassniki.commands.users {
	
	import org.vyana.control.VyanaEvent;

	public class GetUsersInfoCommand extends AbstractUsersCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getUsersInfo(data.uids, data.fields);
		}
		
	}
}