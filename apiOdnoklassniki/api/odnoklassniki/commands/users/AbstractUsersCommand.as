package api.odnoklassniki.commands.users {

	import api.odnoklassniki.business.delegates.UsersServiceDelegate;
	import api.odnoklassniki.commands.OdnoklassnikiCommand;
	
	import mx.rpc.IResponder;

	public class AbstractUsersCommand extends OdnoklassnikiCommand {
		
		protected var delegate:UsersServiceDelegate = new UsersServiceDelegate(this as IResponder);
		
	}
}