package api.odnoklassniki.commands.users {
	
	import org.vyana.control.VyanaEvent;

	public class SetUserStatusCommand extends AbstractUsersCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.setUserStatus(data.text, data.location);
		}
		
	}
}