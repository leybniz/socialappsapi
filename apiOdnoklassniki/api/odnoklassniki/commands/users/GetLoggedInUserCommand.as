package api.odnoklassniki.commands.users {
	
	import org.vyana.control.VyanaEvent;

	public class GetLoggedInUserCommand extends AbstractUsersCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getLoggedInUser();
		}
		
	}
}