package api.odnoklassniki.commands.friends {
	
	import org.vyana.control.VyanaEvent;

	public class GetMutualFriendsCommand extends AbstractFriendsCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getMutualFriends(data);
		}
		
	}
}