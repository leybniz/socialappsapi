package api.odnoklassniki.commands.friends {
	
	import org.vyana.control.VyanaEvent;

	public class AreFriendsCommand extends AbstractFriendsCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.areFriends(data.uids1, data.uids2);
		}
		
	}
}