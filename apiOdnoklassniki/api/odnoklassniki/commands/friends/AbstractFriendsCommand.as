package api.odnoklassniki.commands.friends {

	import api.odnoklassniki.business.delegates.FriendsServiceDelegate;
	import api.odnoklassniki.commands.OdnoklassnikiCommand;
	
	import mx.rpc.IResponder;

	public class AbstractFriendsCommand extends OdnoklassnikiCommand {
		
		protected var delegate:FriendsServiceDelegate = new FriendsServiceDelegate(this as IResponder);
		
	}
}