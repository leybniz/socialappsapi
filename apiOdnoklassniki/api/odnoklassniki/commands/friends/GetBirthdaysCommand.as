package api.odnoklassniki.commands.friends {
	
	import org.vyana.control.VyanaEvent;

	public class GetBirthdaysCommand extends AbstractFriendsCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getBirthdays(data);
		}
		
	}
}