package api.odnoklassniki.commands.photos {

	import api.odnoklassniki.business.delegates.PhotosServiceDelegate;
	import api.odnoklassniki.commands.OdnoklassnikiCommand;
	
	import mx.rpc.IResponder;

	public class AbstractPhotosCommand extends OdnoklassnikiCommand {
		
		protected var delegate:PhotosServiceDelegate = new PhotosServiceDelegate(this as IResponder);
		
	}
}