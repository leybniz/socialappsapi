package api.odnoklassniki.commands.photos {
	
	import org.vyana.control.VyanaEvent;

	public class GetPhotoUploadUrlCommand extends AbstractPhotosCommand {
		
		override public function execute(e:VyanaEvent):void {
			super.execute(e);
			
			delegate.getUploadUrl(data);
		}
		
	}
}