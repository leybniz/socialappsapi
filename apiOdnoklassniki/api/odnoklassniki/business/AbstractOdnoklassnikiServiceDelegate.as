package api.odnoklassniki.business {

	import api.odnoklassniki.constant.*;
	import api.odnoklassniki.model.OdnoklassnikiApplicationModel;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	import org.vyana.model.business.HTTPServiceDelegate;
	import org.vyana.model.business.VyanaHTTPService;

	public class AbstractOdnoklassnikiServiceDelegate extends HTTPServiceDelegate {
		
		public static var requestsQueue:QueueManager;		
		
		function AbstractOdnoklassnikiServiceDelegate(responder:IResponder = null) {
			super(responder, ServiceName.fbURL);

			if (applicationModel.appDescriptor.responseFormat == ResponseFormat.XML)
				oService.resultFormat = 'e4x';
			
			if (applicationModel.appDescriptor.responseFormat == ResponseFormat.JSON)
				oService.resultFormat = 'text';

			if (!requestsQueue) {	
				requestsQueue = new QueueManager(1000 / APIRestriction.MAX_REQUESTS_PER_SECOND);
				requestsQueue.processingFunction = 
					function (request:*):void {
						var call:AsyncToken;
						call = oService.send(request.params);
						call.addResponder(request.responder);
						requestsQueue.stopProcessing();
					};
				requestsQueue.startProcessing();
			}	
		}

		protected function get applicationModel():OdnoklassnikiApplicationModel {
			return context.model as OdnoklassnikiApplicationModel;
		}

		protected function get oService():VyanaHTTPService {
			return service as VyanaHTTPService;
		}

		protected function getParamsSignature(params:Object):String {
			var keys:Array = [];
			for (var k:String in params)
				keys.push(k);
			keys.sort();

			var sig:String = '';
			for (var i:int = 0; i < keys.length; i++)
				sig += keys[i] + "=" + params[keys[i]];
			sig += applicationModel.viewer.session.secretKey;

			return MD5.encrypt(sig).toLowerCase();
		}

		protected function invokeMethod(methodName:String, params:* = null):void {
			if (!params)
				params = {};

			if (applicationModel.appDescriptor.responseFormat != ResponseFormat.XML)
				params.format = applicationModel.appDescriptor.responseFormat;

			params.application_key = applicationModel.appDescriptor.key;
			params.session_key = applicationModel.viewer.session.key;
			params.method = methodName;

			params.sig = getParamsSignature(params);

			requestsQueue.queueObject(
				{
					params : params,
					responder : responder
				}
			);
			
			if (requestsQueue.length > 0)
				requestsQueue.startProcessing();						
		}

	}
}