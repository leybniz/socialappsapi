package api.odnoklassniki.business {
	
	import org.vyana.model.business.VyanaLocalConnectionService;

	public class ForticomLocalConnectionService extends VyanaLocalConnectionService {
		
		public static const CONNECTION_PREFIX:String = '_api_';
		
		function ForticomLocalConnectionService(name:String = null) {
			super(name);
		}
		
		override public function connect(connectionName:String):void {
			super.connect(CONNECTION_PREFIX + connectionName);
		}		
		
	}
}