package api.odnoklassniki.business.delegates {

	import api.odnoklassniki.constant.ServiceName;
	
	import mx.rpc.IResponder;
	
	import org.vyana.model.business.LocalConnectionServiceDelegate;

	public class ForticomServiceDelegate extends LocalConnectionServiceDelegate {
		
		public static const CONNECTION_PREFIX:String = '_proxy_';

		function ForticomServiceDelegate(responder:IResponder) {
			super(responder, ServiceName.FORTICOM);			
			
		}

		public function showInstall():void {
			invokeMethod('showInstall');
		}

		public function showSettings():void {
			invokeMethod('showSettings');
		}

		public function showFeed(uid:String = null, attachment:String = null, actionLinks:String = null):void {
			invokeMethod('showFeed', uid, attachment, actionLinks);
		}

		public function showInvite():void {
			invokeMethod('showInvite');
		}

		public function showPayment(name:String, description:String, code:String, price:int = -1, options:String = null, attributes:String = null):void {
			invokeMethod('showPayment', name, description, code, price, options, attributes);
		}
		
		protected function get connectionName():String {
			return CONNECTION_PREFIX + ServiceName.FORTICOM;
		}
		
		protected function invokeMethod(method:String, ... rest):void {
			if (!localConnection)
				return;
			
			if (rest)
				localConnection.send.apply(localConnection, [connectionName, method].concat(rest));
			else
				localConnection.send(connectionName, method);
		}
		
	}
}