package api.odnoklassniki.business.delegates {

	import api.odnoklassniki.business.AbstractOdnoklassnikiServiceDelegate;
	import api.odnoklassniki.constant.methods.StreamAPIName;
	
	import mx.rpc.IResponder;

	public class StreamServiceDelegate extends AbstractOdnoklassnikiServiceDelegate {
		
		function StreamServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function publish(message:String, attachment:String = null, actionLinks:String = null):void {
			var params:Object = {};
			
			params.message = message;
			
			if (attachment)
				params.attachment = attachment;

			if (actionLinks)
				params.action_links = actionLinks;
			
			invokeMethod(StreamAPIName.PUBLISH, params);
		}
		
	}
}