package api.odnoklassniki.business.delegates {

	import api.odnoklassniki.business.AbstractOdnoklassnikiServiceDelegate;
	import api.odnoklassniki.constant.methods.PhotosAPIName;
	
	import mx.rpc.IResponder;

	public class PhotosServiceDelegate extends AbstractOdnoklassnikiServiceDelegate {
		
		function PhotosServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getUploadUrl(album:String = null):void {
			var params:Object = {};
			
			if (album)
				params.aid = album;
			
			invokeMethod(PhotosAPIName.GET_UPLOAD_URL, params);
		}
		
	}
}