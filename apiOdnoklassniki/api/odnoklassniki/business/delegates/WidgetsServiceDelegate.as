package api.odnoklassniki.business.delegates {

	import api.odnoklassniki.business.AbstractOdnoklassnikiServiceDelegate;
	import api.odnoklassniki.constant.methods.WidgetAPIName;
	
	import mx.rpc.IResponder;

	public class WidgetsServiceDelegate extends AbstractOdnoklassnikiServiceDelegate {
		
		function WidgetsServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getWidgets(wids:Array):void {
			invokeMethod(WidgetAPIName.GET_WIDGETS,
				{
					wids : wids.join(',')
				}
			);
		}
		
	}
}