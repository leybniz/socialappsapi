package api.odnoklassniki.business.delegates {

	import api.odnoklassniki.business.AbstractOdnoklassnikiServiceDelegate;
	import api.odnoklassniki.constant.methods.FriendsAPIName;
	
	import mx.rpc.IResponder;

	public class FriendsServiceDelegate extends AbstractOdnoklassnikiServiceDelegate {
		
		function FriendsServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getFriends():void {
			invokeMethod(FriendsAPIName.GET);
		}

		public function getAppFriends():void {
			invokeMethod(FriendsAPIName.GET_APP_USERS);
		}		
		
		public function getMutualFriends(targetUid:String):void {
			invokeMethod(FriendsAPIName.GET_MUTUAL_FRIENDS, 
				{
					target_id : targetUid
				}
			);
		}
		
		public function getOnlineFriends(kind:String = null):void {
			var params:Object = {};
			
			if (kind)
				params.online = kind;			
			
			invokeMethod(FriendsAPIName.GET_ONLINE, params);
		}		
		
		public function areFriends(uids1:Array, uids2:Array):void {
			invokeMethod(FriendsAPIName.ARE_FRIENDS, 
				{
					uids1 : uids1.join(','),
					uids2 : uids2.join(',')
				}
			);
		}
		
		public function getBirthdays(soon:Boolean = false):void {
			invokeMethod(FriendsAPIName.GET_BIRTHDAYS, 
				{
					future : soon
				}
			);
		}		
		
	}
}