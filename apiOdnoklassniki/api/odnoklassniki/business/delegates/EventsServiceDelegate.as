package api.odnoklassniki.business.delegates {

	import api.odnoklassniki.business.AbstractOdnoklassnikiServiceDelegate;
	import api.odnoklassniki.constant.methods.EventsAPIName;
	
	import mx.rpc.IResponder;

	public class EventsServiceDelegate extends AbstractOdnoklassnikiServiceDelegate {
		
		function EventsServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getEventsInfo(types:Array, locale:String = null):void {
			var params:Object = {};
			
			params.types = types.join(',');
			
			if (locale)
				params.locale = locale;
			
			invokeMethod(EventsAPIName.GET_TYPE_INFO, params);
		}
		
		public function getEvents(uid:String, types:Array = null):void {
			invokeMethod(EventsAPIName.GET, 
				{
					uid : uid,
					types : types.join(',')
				}				
			);
		}
		
	}
}