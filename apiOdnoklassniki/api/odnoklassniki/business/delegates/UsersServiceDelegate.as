package api.odnoklassniki.business.delegates {

	import api.odnoklassniki.business.AbstractOdnoklassnikiServiceDelegate;
	import api.odnoklassniki.constant.methods.UsersAPIName;
	
	import mx.rpc.IResponder;

	public class UsersServiceDelegate extends AbstractOdnoklassnikiServiceDelegate {
		
		function UsersServiceDelegate(responder:IResponder = null) {
			super(responder);
		}
		
		public function getUsersInfo(uids:Array, fields:Array):void {
			invokeMethod(UsersAPIName.GET_INFO, 
				{
					uids : uids.join(','),
					fields : fields.join(',')
				}
			);
		}
		
		public function setUserStatus(text:String, location:String = null):void {
			var params:Object = {};
			
			params.status = text;
			
			if (location)
				params.location = location;
			
			invokeMethod(UsersAPIName.SET_STATUS, params);
		}		

		public function getLoggedInUser():void {
			invokeMethod(UsersAPIName.GET_LOGGEDIN_USER);
		}
		
	}
}