package api.odnoklassniki.business {

	import api.odnoklassniki.constant.ServiceName;
	import api.odnoklassniki.model.OdnoklassnikiApplicationModel;
	
	import mx.controls.Alert;
	
	import org.vyana.model.VyanaModel;
	import org.vyana.model.business.VyanaHTTPService;
	import org.vyana.model.business.VyanaServices;

	public class OdnoklassnikiServices extends VyanaServices {

		function OdnoklassnikiServices() {
		}
		
		override public function init(model:VyanaModel):void {
			
			var m:OdnoklassnikiApplicationModel = model as OdnoklassnikiApplicationModel;
			var flashvars:* = m.flashvars; 
			
			// Override service url if provided by flashvars
			if (flashvars && flashvars.hasOwnProperty('api_server'))
				ServiceName.REST_BASE = flashvars.api_server;
			
			registerHTTPService(new VyanaHTTPService(ServiceName.fbURL));
			
			if (flashvars && flashvars.hasOwnProperty('apiconnection')) {
				ServiceName.FORTICOM = flashvars.apiconnection;
				registerLocalConnectionService(new ForticomLocalConnectionService(ServiceName.FORTICOM));
			}	
		}

	}
}