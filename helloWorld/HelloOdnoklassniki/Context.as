package  {

	import api.odnoklassniki.business.OdnoklassnikiServices;
	
	public final class Context extends VyanaContext {

		function Context() {
			services = new OdnoklassnikiServices();
		}

		public static function get Model():HelloOdnoklassnikiApplicationModel {
			if (!Context.getInstance().model)
				Context.getInstance().model = new HelloOdnoklassnikiApplicationModel();
			
			return Context.getInstance().model as HelloOdnoklassnikiApplicationModel;
		}
		
		public static function getInstance():Context {
			return VyanaContext.getInstance() as Context;
		}

		public static function addEventListener(type:String, listener:Function):void {
			Context.getInstance().addEventListener(type, listener);
		}

	}
}