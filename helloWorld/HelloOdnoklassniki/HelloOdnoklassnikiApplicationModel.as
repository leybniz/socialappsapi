package  {

	import api.odnoklassniki.model.OdnoklassnikiApplicationModel;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;

	public class HelloOdnoklassnikiApplicationModel extends OdnoklassnikiApplicationModel {

		public static const APPLICATION_KEY:String = 'CBAODGABABABABABA';

		function HelloOdnoklassnikiApplicationModel() {
			super(APPLICATION_KEY);
			
			// Handle Faults
			Context.addEventListener(FaultEvent.FAULT, 
				function(e:FaultEvent):void {
					Alert.show(e.fault.faultString);
				}
			);

		}
		
	}
}