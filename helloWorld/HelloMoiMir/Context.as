package {

	import api.mailru.business.MoyMirServices;
	
	public final class Context extends VyanaContext {

		function Context() {
			services = new MoyMirServices();
		}

		public static function get Model():HelloMoyMirApplicationModel {
			if (!Context.getInstance().model)
				Context.getInstance().model = new HelloMoyMirApplicationModel();
			
			return Context.getInstance().model as HelloMoyMirApplicationModel;
		}
		
		public static function getInstance():Context {
			return VyanaContext.getInstance() as Context;
		}

		public static function addEventListener(type:String, listener:Function):void {
			Context.getInstance().addEventListener(type, listener);
		}

	}
}