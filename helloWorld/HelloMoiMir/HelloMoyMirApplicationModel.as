package   {

	import api.mailru.model.MoyMirApplicationModel;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	
	import org.vyana.control.VyanaEvent;

	public class HelloMoyMirApplicationModel extends MoyMirApplicationModel {

		public static const APPLICATION_ID:String = '480211';
		public static const APPLICATION_PRIVATE_KEY:String = '39117cd198be66312de317551bf51a9e';
		public static const APPLICATION_AUTHOR_ID:String = '12252171248967220022';

		function HelloMoyMirApplicationModel() {
			super(APPLICATION_ID, APPLICATION_PRIVATE_KEY, APPLICATION_AUTHOR_ID);
			
			// Handle Faults
			Context.addEventListener(FaultEvent.FAULT, 
				function(e:FaultEvent):void {
					Alert.show(e.fault.faultString);
				}
			);

		}
		
	}
}