package {

	import api.vkontakte.model.VkontakteApplicationModel;

	public class ClipboardApplicationModel extends VkontakteApplicationModel {

		// TODO: Put your application constant values here
		public static const APPLICATION_ID:String = '1xxxxxx';
		public static const APPLICATION_SECRET:String = 'sdfgsdfgsdfgsd';
		public static const APPLICATION_AUTHOR_ID:String = '16598809';

		
		// Variable key to store text in
		public static const STORAGE_VAR:int = 1040;
		public static const MAX_VARS_OCCUPIED:int = 40;
		
		public static const MAX_CHARS_GUEST:int = 50 * 1000; // 49 Kb
		public static const MAX_CHARS_USER:int  = 100 * 1000; // 99 Kb

		[Bindable]
		public static var MAX_CHARS_STORAGE:int = MAX_CHARS_USER;

		function ClipboardApplicationModel() {
			super(APPLICATION_ID, APPLICATION_SECRET, APPLICATION_AUTHOR_ID);
		}
		
		override public function init():void {
			super.init();
			
			if (!viewer.isAppUser)			
				MAX_CHARS_STORAGE = MAX_CHARS_GUEST;			
			
		}
		
	}
}