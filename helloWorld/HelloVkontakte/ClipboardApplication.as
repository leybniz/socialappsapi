package {
	
	import api.vkontakte.VkontakteGlobals;
	import api.vkontakte.constant.UserSettings;
	import api.vkontakte.constant.WrapperEventType;
	import api.vkontakte.ui.VkontakteApplication;
	
	import flash.events.Event;
	import flash.events.ProgressEvent;
	
	import mx.controls.Alert;
	import mx.controls.ProgressBar;
	import mx.controls.ProgressBarMode;
	import mx.controls.Text;
	import mx.controls.TextArea;
	import mx.events.CloseEvent;
	import mx.formatters.NumberFormatter;
	import mx.rpc.events.FaultEvent;
	
	import org.vyana.control.VyanaEvent;
	
	import spark.components.Button;
	import spark.layouts.VerticalLayout;

	public class ClipboardApplication extends VkontakteApplication {
		
		public var btnInstall:Button;
		public var btnSave:Button;
		public var btnClear:Button;
		public var txtCopyright:Text;
		public var pbOccupied:ProgressBar;
		public var taText:TextArea;
		
		protected var nf:NumberFormatter = new NumberFormatter();		
		
		function ClipboardApplication() {
		}
		
		protected function get Model():ClipboardApplicationModel {
			return Context.Model;
		}		
		
		override protected function childrenCreated():void {
		
			if (layout is VerticalLayout) {
				VerticalLayout(layout).gap = 1;
			}
			
			// Handle Faults
			Context.addEventListener(FaultEvent.FAULT,
				function(e:FaultEvent):void {
					enabled = true;
					onTextAreaChange();
				}				 
			);
			
			// Update progress bar on Text Save/Load operations
			Context.addEventListener(ProgressEvent.PROGRESS,
				function(e:ProgressEvent):void {
					pbOccupied.setProgress(e.bytesLoaded, e.bytesTotal);
					
					if (e.bytesLoaded == e.bytesTotal)
						onTextAreaChange();
				}				 
			);
			
			with (nf) {
				thousandsSeparatorTo = thousandsSeparatorFrom = ',';
				useThousandsSeparator = true;				
			}
			
			with (pbOccupied) {
				maximum = ClipboardApplicationModel.MAX_CHARS_STORAGE;
				minimum = 0;
				mode = ProgressBarMode.MANUAL;				
			}
			
			with (taText) {
				maxChars = ClipboardApplicationModel.MAX_CHARS_STORAGE;
			}
			
		}
		
		override protected function partAdded(partName:String, instance:Object):void {
			super.partAdded(partName, instance);
			
		}
		
		override protected function onAddedToStage(e:Event):void {
			super.onAddedToStage(e);
			
			Model.init();
			
			// Load text
			enabled = false;
			new VyanaEvent(APIVkontakte.GET_LONG_VARIABLE, 
				{ key: ClipboardApplicationModel.STORAGE_VAR }
			).dispatch( 
				function(result:*):void {
					taText.text = result.response;
					taText.setFocus();
					onTextAreaChange();
					enabled = true;
				}
			);
			
			btnInstall.visible = btnInstall.includeInLayout = !isInstalled && isWrapperAccessible;				
		}
		
		protected function onInstallClick(e:Event):void {
			
			VkontakteGlobals.wrapper.addEventListener(
				WrapperEventType.APPLICATION_ADDED,
				function (a:Object):void {
					if (!Context.Model.viewer.settings.isSet(UserSettings.MENU_NAVIGATION_GRANTED))
						VkontakteGlobals.wrapper.external.showSettingsBox(UserSettings.MENU_NAVIGATION_GRANTED);
				}
			);
			
			VkontakteGlobals.wrapper.external.showInstallBox();				
			
			btnInstall.visible = btnInstall.includeInLayout = false;
			ClipboardApplicationModel.MAX_CHARS_STORAGE = ClipboardApplicationModel.MAX_CHARS_USER;
			onTextAreaChange();
		}
		
		protected function onSaveButtonClick(e:Event = null):void {				
			
			btnSave.enabled = enabled = false;
			pbOccupied.label = RM.getString('msgTextIsBeingSaved');
			
			// Save text
			new VyanaEvent(APIVkontakte.PUT_LONG_VARIABLE, 
				{ 
					key: ClipboardApplicationModel.STORAGE_VAR, 
					value: taText.text 
				}
			).dispatch(
				function(result:*):void {
					btnSave.enabled = enabled = true;
					onTextAreaChange();
				}
			);
			
		}			
		
		protected function onClearButtonClick(e:Event = null):void {
			
			// Long clean				
			//				btnClear.enabled = btnSave.enabled = enabled = false;
			//				pbOccupied.label = resourceManager.getString(RESOURCE_BUNDLE_NAME, 'msgTextIsBeingCleared');
			//				
			//				// Clear text
			//				new VyanaEvent(APIVkontakte.CLEAR_LONG_VARIABLE, 
			//					{ 
			//						key: ClipboardApplicationModel.STORAGE_VAR, 
			//						numVars : ClipboardApplicationModel.MAX_VARS_OCCUPIED
			//					}
			//				).dispatch(
			//					function(result:*):void {
			//						btnClear.enabled = btnSave.enabled = enabled = true;
			//
			//						taText.text = '';
			//						taText.setFocus();
			//						onTextAreaChange();						
			//					}					
			//				); 
			
			Alert.show(RM.getString('msgAreYouSureToClean'), 
				RM.getString('warning'), Alert.YES | Alert.NO, this,
				function (ce:CloseEvent):void {
					if (ce.detail == Alert.YES) {
						// Fast clean
						new VyanaEvent(APIVkontakte.PUT_VARIABLE, 
							{ 
								key: ClipboardApplicationModel.STORAGE_VAR, 
								value: '' 
							}
						).dispatch();
						taText.text = '';
						taText.setFocus();
						onTextAreaChange();				
					}
				},
				null,
				Alert.NO);
			
		}			
		
		protected function onTextAreaChange(e:Event = null):void {
			btnClear.enabled = taText.text.length > 0;
			pbOccupied.setProgress(taText.text.length, ClipboardApplicationModel.MAX_CHARS_STORAGE);
			pbOccupied.label = RM.getString('msgUnusedCharsLeft', [nf.format(ClipboardApplicationModel.MAX_CHARS_STORAGE - taText.text.length)]);
			if (ClipboardApplicationModel.MAX_CHARS_STORAGE - taText.text.length == 0)
				pbOccupied.label = RM.getString('msgCharsLimitOverflow');
			
			if (VkontakteGlobals.wrapper)
				VkontakteGlobals.wrapper.external.setNavigation(
					[
						RM.getString('msgUsedChars', 
							[nf.format(taText.text.length)]
						)
					]
				);
		}
		
		override protected function resourcesChanged():void {
			super.resourcesChanged();
			
			if (pbOccupied)
				pbOccupied.label = RM.getString('msgTextIsBeingLoaded');
			
			if (btnClear)
				btnClear.label = RM.getString('btnClear.label');
			
			if (btnSave)
				btnSave.label = RM.getString('btnSave.label');
			
			if (btnInstall)
				btnInstall.label = RM.getString('btnInstall.label');
			
			if (txtCopyright)
				txtCopyright.text = RM.getString('copyright');
			
			Alert.yesLabel = RM.getString('yes');
			Alert.noLabel = RM.getString('no');
		}		
		
	}
}