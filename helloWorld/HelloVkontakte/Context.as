package {

	import api.vkontakte.business.VkontakteServices;

	import org.vyana.control.*;

	public final class Context extends VyanaContext {

		function Context() {
			services = new VkontakteServices();
		}

		public static function getInstance():Context {
			return VyanaContext.getInstance() as Context;
		}

		public static function get Model():ClipboardApplicationModel {
			if (!Context.getInstance().model) {
				Context.getInstance().model = new ClipboardApplicationModel();
			}
			
			return Context.getInstance().model as ClipboardApplicationModel;
		}		

		public static function addEventListener(type:String, listener:Function):void {
			Context.getInstance().addEventListener(type, listener);
		}

	}
}