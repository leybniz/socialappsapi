package {

	import flash.events.EventDispatcher;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	import mx.core.Singleton;
	import mx.managers.ISystemManager;
	
	import org.vyana.control.VyanaController;
	import org.vyana.model.VyanaModel;
	import org.vyana.model.business.VyanaServices;
	import org.vyana.model.constant.VyanaMessageCodes;
	

	[Mixin]
	public class VyanaContext extends EventDispatcher {

		public static const CONTEXT_SINGLETON_NAME:String = 'Context';
		public static var CONTEXT_CLASS:Class;
		
		public static function init(sm:ISystemManager):void {
			
			CONTEXT_CLASS = Class(getDefinitionByName(CONTEXT_SINGLETON_NAME));
			// Register descendant Singleton class
			Singleton.registerClass(
				CONTEXT_SINGLETON_NAME,
				CONTEXT_CLASS
			);
			
		}
		
		public var controller:VyanaController = new VyanaController();
		public var services:VyanaServices;
		public var model:VyanaModel;
		
		public static var instance:*;
		
		function VyanaContext() {
			if (instance)
				throw new Error(
					VyanaMessageCodes.SINGLETON_EXCEPTION, 
					getQualifiedClassName(Singleton.getInstance(CONTEXT_SINGLETON_NAME))
				);
		}		
		
		public static function getInstance():VyanaContext {
			if (!instance)
				instance = new CONTEXT_CLASS();
		   
			return instance;
		}		

	}
}