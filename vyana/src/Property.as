package {
	
	/**
	 * Property class describes single property entity 
	 * in terms of it's name and value, used as a parameter 
	 * to MightyArray methods.
	 * 
	 * @see MightyArray
	 */
	public class Property {
		
		/**
		 * Property name, to be identified 
		 */
		public var name:String;
		
		/**
		 * Property value 
		 */
		public var value:*;
		
		function Property(name:String = "", value:* = null) {
			this.name = name;
			this.value = value;
		}

	}
}