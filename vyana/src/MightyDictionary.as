package {
	
	import flash.utils.Dictionary;

	public class MightyDictionary extends Dictionary {
		
		function MightyDictionary(weakKeys:Boolean = false) {
			super(weakKeys);
		}
		
		public function get isEmpty():Boolean {
			return new MightyArray(this).length == 0;
		}
	}
}