package org.vyana.ui {

	import mx.core.Application;	
	
	public class VyanaApplication extends Application {
		
		function VyanaApplication() {
			
		}
		
		override protected function initializationComplete():void {
			super.initializationComplete();
			
			VyanaContext.getInstance();
		}

	}
}