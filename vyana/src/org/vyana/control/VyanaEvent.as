package org.vyana.control {

	import flash.events.Event;
	import flash.utils.getQualifiedClassName;
	
	import mx.core.Singleton;

	public class VyanaEvent extends Event {

		/**
		 * The data property can be used to hold information to be passed with the event
		 */
		public var data : *;

		public var resultFunction:Function;
		public var resultConsumers:Array;

		function VyanaEvent(type:*, data:* = null, bubbles:Boolean = false, cancelable:Boolean = false) {

			// Execute command event?
			if (type is Class)
				type = getQualifiedClassName(type);

			super(type, bubbles, cancelable);

			this.data = data;
		}

		public function get context():VyanaContext {
			return Singleton.getInstance(VyanaContext.CONTEXT_SINGLETON_NAME) as VyanaContext;
		}

		override public function clone():Event {
			return new VyanaEvent(type, data);
		}		

		public function dispatch(resultConsumer:* = null):Boolean {

			if (resultConsumer is Function)
				resultFunction = resultConsumer;
				
			if (resultConsumer is Array)
				resultConsumers = resultConsumer;								 

			return context.dispatchEvent(this);
		}

	}
}