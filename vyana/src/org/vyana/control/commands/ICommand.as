package org.vyana.control.commands {

	import org.vyana.control.VyanaEvent;	

	public interface ICommand {

		function execute(event:VyanaEvent):void;

	}
}