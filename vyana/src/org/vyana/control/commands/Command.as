package org.vyana.control.commands {
	
	import org.vyana.control.VyanaEvent;

	public class Command implements ICommand {
		
		protected var eventInitiator:VyanaEvent;
		
		function Command() {
		}
		
		protected function get context():VyanaContext {
			return VyanaContext.getInstance();
		}
		
		protected function get data():* {
			return eventInitiator && eventInitiator.data ? eventInitiator.data : {}; 
		}		
		
		/**
		 * Override this getter to point to your Model class 
		 * @return VyanaModel descendant class 
		 * 
		 */
		protected function get model():* {
			return context.model;
		}		

		public function execute(event:VyanaEvent):void {
			eventInitiator = event;
		}
		
	}
}