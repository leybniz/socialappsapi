package org.vyana.control.commands {

	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class AsynchronousCommand extends Command implements IResponder {

		protected function set Result(value:*):void {

			if (eventInitiator.resultFunction != null) {
				eventInitiator.resultFunction(value);
			}	
			
			if (eventInitiator.resultConsumers != null)
				for each (var resultConsumerFunction:Function in eventInitiator.resultConsumers)
					resultConsumerFunction(value);	

		}		

		public function result(e:Object):void {
			if (e is ResultEvent)				
				onResult(ResultEvent(e));
		}

		public function fault(e:Object):void {
			if (e is FaultEvent)
				onFault(FaultEvent(e));
		}

		protected function onResult(e:ResultEvent):void {
			Result = ResultEvent(e).result;
		}

		protected function onFault(e:FaultEvent):void {

		}

	}
}