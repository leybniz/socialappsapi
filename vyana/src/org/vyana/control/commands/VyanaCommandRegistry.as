package org.vyana.control.commands {

	/**
	 * Commands registry base class, used as commands container 
	 * pending to be registered with the controller once 
	 * VyanaCommandRegistry descendant instantiated.  
	 */
	public class VyanaCommandRegistry {
		
		function VyanaCommandRegistry() {

			context.controller.addCommandRegistry(
						Object(this).constructor
			);
			
		}
		
		public function get context():VyanaContext {
			return VyanaContext.getInstance();
		}

	}
	
}