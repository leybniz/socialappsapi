package org.vyana.model {
	
	import flash.events.EventDispatcher;

	public class VyanaManager extends EventDispatcher {
		
		protected function get model():VyanaModel {
			return VyanaContext.getInstance().model;
		}
		
	}
}