package org.vyana.model.vo {
	
	public class VyanaValueObject {
		
		protected var _xml:XML;
		
		function VyanaValueObject() {
			
		}

		[Transient]
		public function get xml():XML {
			return _xml;
		}

		public function set xml(value:XML):void {
			_xml = value;
		}

	}
}