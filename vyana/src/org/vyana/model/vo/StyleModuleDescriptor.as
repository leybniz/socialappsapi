package org.vyana.model.vo {
	
	import mx.events.ModuleEvent;
	import mx.styles.IStyleManager2;
	import mx.styles.IStyleModule;
	
	import org.vyana.model.modules.VyanaModuleEvent;

	public class StyleModuleDescriptor extends VyanaModuleDescriptor {
		
		import mx.core.Singleton;
		
		public var styleModule:IStyleModule;
		
		function StyleModuleDescriptor(type:String, id:String, url:String, isDefault:Boolean = false) {
			super(type, id, url, isDefault);
		}
		
		protected function get styleManager():IStyleManager2 {
			return mx.core.Singleton.getInstance("mx.styles::IStyleManager2") as IStyleManager2;
		}
		
		override protected function onModuleReady(e:ModuleEvent = null):void {
			super.onModuleReady(e);
			
			styleModule = IStyleModule(e.module.factory.create());			
			// Register the style module to use this style manager.
			e.module.factory.registerImplementation("mx.styles::IStyleManager2", styleManager);
			styleModule.setStyleDeclarations(styleManager);
			
			styleManager.styleDeclarationsChanged();
			new VyanaModuleEvent(VyanaModuleEvent.STYLE_MODULE_APPLIED, this).dispatch();
		}
		
		override public function unload() : void {
			super.unload();			
			
			if (styleModule)
				styleModule.unload();			
			styleManager.styleDeclarationsChanged();
		}
		
	}
}