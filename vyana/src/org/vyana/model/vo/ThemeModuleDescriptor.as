package org.vyana.model.vo {
	
	import mx.events.ModuleEvent;
	
	import org.vyana.model.modules.BaseThemeModule;
	import org.vyana.model.modules.VyanaModuleEvent;

	public class ThemeModuleDescriptor extends VyanaModuleDescriptor {
		
		function ThemeModuleDescriptor(type:String, id:String, url:String, isDefault:Boolean = false) {
			super(type, id, url, isDefault);
		}
		
		public function get themeModule():BaseThemeModule {
			return module as BaseThemeModule;
		}
		
		override protected function onModuleReady(e:ModuleEvent = null):void {
			super.onModuleReady(e);
			
			new VyanaModuleEvent(VyanaModuleEvent.THEME_MODULE_READY, this).dispatch();
		}		
		
	}
}