package org.vyana.model.business {

	import mx.rpc.IResponder;

	/**
	 * This class maintains asynchronous calls that use HTTPService class instances
	 * as accessor, and IResponder objects as contractor
	 */
	public class HTTPServiceDelegate extends VyanaDelegate {

		function HTTPServiceDelegate(responder:IResponder, serviceName:String) {
			super(responder, serviceName);
			
			if (context.services)
				this.service = context.services.getHTTPService(serviceName);
		}		

	}
}