package org.vyana.model.business {
	
	import mx.messaging.Consumer;

	public class VyanaConsumer extends Consumer {
		
		function VyanaConsumer(destination:String = '', messageType:String = 'flex.messaging.messages.AsyncMessage') {
			super(messageType);
			this.destination = destination;
		}
	}
}