package org.vyana.model.business {

	import flash.events.EventDispatcher;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;

	public class VyanaDelegate extends EventDispatcher {

		protected var service:*;
		protected var responder:IResponder;

		function VyanaDelegate(responder:IResponder, serviceName:String) {			
			this.responder = responder;
		}
		
		public function get context():VyanaContext {
			return VyanaContext.getInstance();
		}
		
		protected function invoke(call:AsyncToken):Object {
			call.addResponder(responder);
			return call;
		}

	}
}