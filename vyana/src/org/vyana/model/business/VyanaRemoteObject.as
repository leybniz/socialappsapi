package org.vyana.model.business {

	import mx.rpc.remoting.mxml.RemoteObject;

	public class VyanaRemoteObject extends RemoteObject {

		function VyanaRemoteObject(destination:String, source:String = '', endpoint:String = '') {
			super(destination);

			this.showBusyCursor = true;
			this.source = source;
			this.endpoint = endpoint;
		}

	}
}