package org.vyana.model.business {

	import flash.utils.Dictionary;
	
	import mx.messaging.Consumer;
	import mx.messaging.MessageAgent;
	import mx.messaging.Producer;
	import mx.rpc.remoting.mxml.RemoteObject;
	
	import org.vyana.model.VyanaModel;
	
	public class VyanaServices {
		
		private var remoteObjectsRegistry:Dictionary = new Dictionary();
		private var httpServicesRegistry:Dictionary = new Dictionary();		
		private var localConnectionServicesRegistry:Dictionary = new Dictionary();
		private var messageAgentsRegistry:MightyArray = new MightyArray();
		private var messageSubscribersRegistry:MightyArray = new MightyArray();
		
		function VyanaServices() {
			
		}
		
		public function init(model:VyanaModel):void {
			
		}
		
		public function registerLocalConnectionService(service:VyanaLocalConnectionService):void {
			localConnectionServicesRegistry[service.name] = service;
		}
		
		public function getProducer(destination:String):Producer {
			var result:Producer;
			
			enumMessageAgents(
				function(ma:MessageAgent):void {
					if (ma.destination == destination && ma is Producer)
						result = ma as Producer;
				}
			);
			
			return result;
		}
		
		public function getConsumer(destination:String):Consumer {
			var result:Consumer;
			
			enumMessageAgents(
				function(ma:MessageAgent):void {
					if (ma.destination == destination && ma is Consumer)
						result = ma as Consumer;
				}
			);
			
			return result;
		}		
		
		public function getLocalConnectionService(name:String):VyanaLocalConnectionService {
			return localConnectionServicesRegistry[name];
		}		

		public function registerHTTPService(httpService:VyanaHTTPService):void {
			httpServicesRegistry[httpService.url] = httpService;
		}
		
		public function registerRemoteObject(ro:RemoteObject):void {
			remoteObjectsRegistry[ro.source ? ro.source : ro.destination] = ro;
		}
		
		public function registerMessageAgent(ma:MessageAgent):void {
			messageAgentsRegistry.addItem(ma);
		}
		
		public function registerMessageSubscriber(ms:Object):void {
			messageSubscribersRegistry.addItem(ms);
		}

		public function getHTTPService(name:String):VyanaHTTPService {
			return httpServicesRegistry[name];
		}
		
		public function getRemoteObject(name:String):RemoteObject {
			var r:* = remoteObjectsRegistry[name];
			
			if (!r)
				for each (var ro:RemoteObject in remoteObjectsRegistry)
					if (ro.destination == name)
						r = ro;
			
			return r;
		}

		public function enumRemoteObjects(visitorFunction:Function):void {
			for each (var ro:RemoteObject in remoteObjectsRegistry)
				visitorFunction(ro);
		}
		
		public function enumMessageAgents(visitorFunction:Function):void {
			for each (var ma:MessageAgent in messageAgentsRegistry)
				visitorFunction(ma);
		}
		
		public function enumMessageSubscribers(visitorFunction:Function):void {
			for each (var ms:Object in messageSubscribersRegistry)
				visitorFunction(ms);
		}		

	}
}