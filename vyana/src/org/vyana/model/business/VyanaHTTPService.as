package org.vyana.model.business {

	import mx.rpc.http.HTTPService;	
	
	public class VyanaHTTPService extends HTTPService {
		
		function VyanaHTTPService(URL:String = null, rootURL:String = null, destination:String = null) {
			super(rootURL, destination);
			
			url = URL;
		}

	}
}