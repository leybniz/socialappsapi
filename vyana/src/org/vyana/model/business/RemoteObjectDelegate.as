package org.vyana.model.business {

	import mx.rpc.IResponder;
	import mx.rpc.remoting.mxml.RemoteObject;

	/**
	 * This class maintains asynchronous calls that use RemoteObject class instances
	 * as accessor, and IResponder objects as contractor
	 */
	public class RemoteObjectDelegate extends VyanaDelegate {

		function RemoteObjectDelegate(responder:IResponder, serviceName:String) {
			super(responder, serviceName);
			
			this.service = context.services.getRemoteObject(serviceName);
		}		

	}
}