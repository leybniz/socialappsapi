package org.vyana.model.business {
	
	import flash.events.AsyncErrorEvent;
	import flash.events.StatusEvent;
	import flash.net.LocalConnection;

	public class VyanaLocalConnectionService {

		private var _name:String;
		private var _lc:LocalConnection;

		function VyanaLocalConnectionService(name:String = null) {
			_lc = new LocalConnection();
			_lc.addEventListener(AsyncErrorEvent.ASYNC_ERROR, onAsyncError);
			_lc.addEventListener(StatusEvent.STATUS, onCallStatus);
			_lc.client = this;
			_lc.allowDomain('*');
			
			if (name)
				this.name = name;
		}

		public function get localConnection():LocalConnection {
			return _lc;
		}

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
			connect(value);
		}
		
		public function connect(connectionName:String):void {
			try {
				_lc.connect(connectionName);
			} catch (e:Error) {
				throw new Error('Connection "' + connectionName + '" is already in use!');
			}			
		}
		
		protected function onAsyncError(e:AsyncErrorEvent):void {
			
		}
		
		protected function onCallStatus(e:StatusEvent):void {
			
		}

	}
}