package org.vyana.model.business {
	
	import mx.messaging.Producer;

	public class VyanaProducer extends Producer {
		
		function VyanaProducer(destination:String = '') {
			this.destination = destination;
		}
		
	}
}