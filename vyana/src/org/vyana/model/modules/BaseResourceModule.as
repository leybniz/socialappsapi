package org.vyana.model.modules {

	import flash.display.BitmapData;
	import flash.utils.Dictionary;
	
	import mx.core.BitmapAsset;
	
	import org.vyana.model.*;

	public class BaseResourceModule extends VyanaModule {

		protected var items:Dictionary = new Dictionary();
		
		function BaseResourceModule() {
			init();
		}
		
		public function getBitmapData(id:String):BitmapData {
			var result:BitmapData;
			var cls:Class = getResource(id);
			var ba:BitmapAsset = cls ? new cls() : null;
			
			if (ba)
				result = ba.bitmapData;
			
			return result;
		}
		
		public function getResource(id:String):* {
			return items[id];
		}
		
		public function get resources():MightyArray {
			return new MightyArray(items);
		}
		
		protected function init():void {
			var variables:XMLList = accessors;
			
			for (var i:uint = 0; i < variables.length(); i++)
				items[String(variables[i])] = this[variables[i]];
		}
		
	}
}