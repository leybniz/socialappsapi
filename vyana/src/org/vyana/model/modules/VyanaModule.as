package org.vyana.model.modules {

	import flash.utils.ByteArray;
	
	import mx.modules.ModuleBase;
	import mx.styles.IStyleManager2;
	import mx.styles.StyleManager;
	import mx.utils.DescribeTypeCache;
	
	import org.vyana.model.vo.VyanaModuleDescriptor;

	public class VyanaModule extends ModuleBase {
		
		public var moduleDescriptor:VyanaModuleDescriptor;

		function VyanaModule() {
		}
		
		public function get url():String {
			return moduleDescriptor.url;			
		}
		
		public function unload():void {
			
		}
		
		protected function get styleManager():IStyleManager2 {
			return StyleManager.getStyleManager(moduleDescriptor.moduleInfo.factory);
		}

		protected function getClassAsByteArray(cls:Class):ByteArray {
			return new cls() as ByteArray;
		}

		protected function getClassAsString(cls:Class):String {
			var ba:ByteArray = new cls() as ByteArray;
			return ba.readUTFBytes(ba.length);
		}

		protected function get accessors():XMLList {
			var description:XML = DescribeTypeCache.describeType(this).typeDescription;
			var accessors:XMLList = description.accessor.(@access == "readwrite").@name;

			return accessors;
		}
	}
}