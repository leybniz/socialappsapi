package org.vyana.model.modules {

	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import mx.styles.StyleManager;
	
	import org.vyana.model.vo.StyleModuleDescriptor;

	public class BaseThemeModule extends BaseResourceModule {

		public var colors:Dictionary = new Dictionary();
		
		protected var styleModuleDescriptor:StyleModuleDescriptor;
		
		function BaseThemeModule() {
			
		}		
		
		public function getColor(id:String):uint {
			var c:Object = colors[id];			
			
			if (c is String)
				return styleManager.getColorName(c);
			
			return uint(c);
		}
		
		override public function unload():void {
			super.unload();
			
			if (styleModuleDescriptor) {
				styleModuleDescriptor.unload();
				styleModuleDescriptor = null;
			}	
		}		
		
		protected function loadStyleModuleFromClass(cls:Class):void {
			styleModuleDescriptor = new StyleModuleDescriptor('', '', getQualifiedClassName(this));
			styleModuleDescriptor.load(getClassAsByteArray(cls));			
		}		
		
	}
}