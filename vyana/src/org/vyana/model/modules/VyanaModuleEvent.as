package org.vyana.model.modules {
	
	import flash.events.Event;
	
	import org.vyana.control.VyanaEvent;

	public class VyanaModuleEvent extends VyanaEvent {
		
		public static const DOWNLOAD_STARTED:String = 'evtModuleDownloadStarted';
		public static const STYLE_MODULE_APPLIED:String = 'evtStyleModuleApplied';
		public static const THEME_MODULE_READY:String = 'evtThemeModuleApplied';
		public static const MODULE_READY:String = 'evtModuleReady';
		public static const MODULE_DESCRIPTORS_LIST_SET:String = 'evtModuleDescriptorsSet';
		
		
		function VyanaModuleEvent(type:*, data:* = null, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, data, bubbles, cancelable);
		}
		
		override public function clone():Event {
			return new VyanaModuleEvent(type, data, bubbles, cancelable);
		}
		
	}
}