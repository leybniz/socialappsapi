package org.vyana.model.modules {
	
	import flash.events.ProgressEvent;
	
	import org.vyana.model.vo.VyanaModuleDescriptor;

	public class VyanaModuleProgressEvent extends ProgressEvent {
		
		public var moduleDescriptor:VyanaModuleDescriptor;
		
		function VyanaModuleProgressEvent(type:String, moduleDesc:VyanaModuleDescriptor, bubbles:Boolean = false, cancelable:Boolean = false, bytesLoaded:uint = 0, bytesTotal:uint = 0) {
			super(type, bubbles, cancelable, bytesLoaded, bytesTotal);
			
			this.moduleDescriptor = moduleDesc;
		}
		
	}
}