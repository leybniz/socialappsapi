package org.vyana.model.constant {

	/**
	 * Stores Vyana message codes.
	 * 
	 * <p>All messages/error codes must match the regular expression:
	 *
	 * V\d{4}[EWI]
	 *
	 * 1. The application prefix e.g. 'V'.
	 * 
	 * 2. A four-digit error code that must be unique.
	 * 
	 * 3. A single character severity indicator
	 *    (E: error, W: warning, I: informational).</p>
	 */
	public class VyanaMessageCodes {

		public static const SINGLETON_EXCEPTION : String = "V0001E";
		public static const SERVICE_NOT_FOUND : String = "V0002E";
		public static const COMMAND_ALREADY_REGISTERED : String = "V0003E";
		public static const COMMAND_NOT_FOUND : String = "V0004E";
		public static const VIEW_ALREADY_REGISTERED : String = "V0005E";
		public static const VIEW_NOT_FOUND : String = "V0006E";
		public static const REMOTE_OBJECT_NOT_FOUND : String = "V0007E";	
		public static const HTTP_SERVICE_NOT_FOUND : String = "V0008E";
		public static const WEB_SERVICE_NOT_FOUND : String = "V0009E";
		public static const CONSUMER_NOT_FOUND : String = "V0010E";
		public static const PRODUCER_NOT_FOUND : String = "V0012E";
		public static const DATA_SERVICE_NOT_FOUND : String = "V0013E";
		public static const ABSTRACT_METHOD_CALLED : String = "V0014E";
		public static const COMMAND_NOT_REGISTERED : String = "V0015E";

	}
}