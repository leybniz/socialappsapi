package org.vyana.model {

	public dynamic class VyanaModel {

		public var modulesManager:VyanaModulesManager;
		public var themesManager:VyanaThemesManager;
		
		function VyanaModel() {
			if (VyanaContext.getInstance().services)
				VyanaContext.getInstance().services.init(this);
		}
		
		public function init():void {
			
		}

	}
}